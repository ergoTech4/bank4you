package uk.bank4you.client.services;

import android.content.Context;
import android.content.Intent;
import uk.bank4you.client.App;
import uk.bank4you.client.BuildConfig;
import uk.bank4you.client.constants.ConstBC;
import uk.bank4you.client.constants.ConstProgress;

public class UtilSendBroadcast {
    private Context context;
    private App     app;

    public UtilSendBroadcast(Context context) {
        this.context    = context;
        this.app        = (App) (this.context).getApplicationContext();
    }

    public void sendMyBroadcastToast(String msg, int duration) {
        Intent intentBroadCast = new Intent();
        intentBroadCast.setAction(BuildConfig.BC_ACTION);
        intentBroadCast.putExtra("EVENT", ConstBC.BC_EVENT_TOAST);
        intentBroadCast.putExtra("TEXT", msg);
        intentBroadCast.putExtra("LONGITUDE", duration);
        context.sendBroadcast(intentBroadCast);
    }

    // **************************************************************************************************
    public void sendMyBroadcastProgress(int status, String msg) {
        Intent intentBroadCast = new Intent();
        intentBroadCast.setAction(BuildConfig.BC_ACTION);
        intentBroadCast.putExtra("EVENT", ConstBC.BC_EVENT_PROGRESS);
        intentBroadCast.putExtra("STATUS", status);
        intentBroadCast.putExtra("TEXT", msg);
        context.sendBroadcast(intentBroadCast);

        if (status== ConstProgress.BC_STATUS_PROGRESS_START) app.setMyProgressBar(msg, true);
        else if (status== ConstProgress.BC_STATUS_PROGRESS_STOP) app.setMyProgressBar("", false);
    }

    /* ОТВЕТ ОТ SERVICE INTENT О РЕЗУЛЬТАТЕ */
    public void sendMyBroadcastResult(int event, String status, String text, String log) {
        Intent intentBroadCast = new Intent();
        intentBroadCast.setAction(BuildConfig.BC_ACTION);
        intentBroadCast.putExtra("EVENT", event);
        intentBroadCast.putExtra("STATUS", status);
        intentBroadCast.putExtra("TEXT", text);
        intentBroadCast.putExtra("LOG", log);
        context.sendBroadcast(intentBroadCast);
    }
}