package uk.bank4you.client.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import uk.bank4you.client.App;
import uk.bank4you.client.BuildConfig;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.*;
import uk.bank4you.client.multyfields.AnswerJSONObject;
import uk.bank4you.client.multyfields.AnswerText;
import uk.bank4you.client.response.ReqMain;
import uk.bank4you.client.utils.*;

import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ServiceDownload extends IntentService {

    private static final String TAG = "ServiceDownload";

    private int                 event;
    private UtilLog             utilLog;
    private Util                util;
    private UtilWeb             utilWeb;
    private App                 app;
    private UtilDbGet           utilDbGet;
    private UtilDbEdite         utilDbEdite;
    private Serializable        serializableObj;
    private UtilPrefCustom      utilPrefCustom;
    private UtilSendBroadcast   utilSendBroadcast;

    public ServiceDownload() {
        super("ServiceDownload");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        serializableObj     = intent.getSerializableExtra(ConstEX.SERIALIZABLE_REQUEST);
        event               = ((ReqMain.ReqBasic) serializableObj).event;
        app                 = (App) getApplicationContext();
        utilPrefCustom      = new UtilPrefCustom(this);
        utilLog             = new UtilLog(this);
        utilWeb             = new UtilWeb(this);
        util                = new Util(this);
        utilSendBroadcast   = new UtilSendBroadcast(this);
        utilDbEdite         = new UtilDbEdite(this);
        utilDbGet           = new UtilDbGet(this);

        startCurrentRequest();
    }

    private void startCurrentRequest() {
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_START, ((ReqMain.ReqBasic) serializableObj).msg);

        Object objHello = new ReqMain.ReqHello(
                util.id(this), "3", "" + util.getVersion(),
                util.getDeviceModel(), Locale.getDefault().toString(),
                util.getDeviceMaker(), util.getDeviceName(), "android", Build.VERSION.RELEASE);

        Log.d(TAG, "1444  " + event );

        switch (event) {
            case ConstBC.BC_EVENT_HELLO:                loadHello(serializableObj); break;
            case ConstBC.BC_EVENT_SIGNUP_PHONE:         loadSignUpPhone_Hello(serializableObj, objHello);break;
            case ConstBC.BC_EVENT_SIGNIN_PHONE:         loadSignInPhone_Hello(serializableObj, objHello);break;
            case ConstBC.BC_EVENT_SIGNUP_PIN:           loadSignUpPin(serializableObj); break;
            case ConstBC.BC_EVENT_SIGNUP_PHOTO:         loadSignUpPhoto(serializableObj); break;
            case ConstBC.BC_EVENT_SIGNUP_PASS:          loadSignUpPass(serializableObj); break;
            case ConstBC.BC_EVENT_SIGNUP_CARD_ID:       loadSignUpCardId(serializableObj); break;
            case ConstBC.BC_EVENT_GET_AUTH_TOKEN:       loadGetAuthToken(serializableObj); break;
            case ConstBC.BC_EVENT_GET_CARD_LIST:        loadGetCardList(serializableObj); break;
            case ConstBC.BC_EVENT_GET_TRANSACTION_LIST: loadGetTransactionList(serializableObj); break;
            case ConstBC.BC_EVENT_GET_TRANSACTION_LIST_BEFORE: loadGetTransactionListBefore(serializableObj); break;
            case ConstBC.BC_EVENT_GET_STARTED:          loadStarter(); break;
            case ConstBC.BC_EVENT_GET_MONEYPOLOLINK:    loadMoneypoloLink(serializableObj); break;

            case ConstBC.BC_EVENT_GET_CARD_LIST_AVAILABLE_IN_REG:    loadAvailableCardList(serializableObj); break;
            case ConstBC.BC_EVENT_CARD_VALIDATION:      loadValidateCard(serializableObj); break;
            case ConstBC.BC_EVENT_FORGOT_PASSWORD:      loadForgetPassword(serializableObj); break;
            case ConstBC.BC_EVENT_FORGOT_PASSWORD1:     loadForgetPassword1(serializableObj); break;
            case ConstBC.BC_EVENT_FORGOT_PASSWORD2:     loadForgetPassword2(serializableObj); break;
            case ConstBC.BC_EVENT_GET_ACCOUNT_DETAILS:  loadAccountDetails(serializableObj); break;
            case ConstBC.BC_EVENT_CHANGE_PASSWORD:      loadChangePassword(serializableObj); break;
            case ConstBC.BC_EVENT_SIGNIN_PIN:           loadAuthToken(serializableObj); break;
            case ConstBC.BC_EVENT_RESEND_SMS:           loadResendSms(serializableObj); break;

            case ConstBC.BC_EVENT_SAVE_CLIENT_PHONE_CONTACTS:  loadSaveClientPhoneContacts(serializableObj); break;
            case ConstBC.BC_EVENT_SYSTEM_MESSAGE:       loadSystemMessages(serializableObj); break;

        }
    }

    private void loadForgetPassword(Object obj) {
        AnswerText  answerText  = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String      fileName    = util.getClassName(obj.getClass()) + ".json";
        String      urlPart     = ((ReqMain.ReqForgotPassword) obj).urlPart;
        String      req         = ((ReqMain.ReqForgotPassword) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);

        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if (answerJSONObject.jsonObject.getJSONObject("error").getInt("code") !=0 ){
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {

                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, getString(R.string.error_getting_data), answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);
    }

    private void loadForgetPassword1(Object obj) {
        AnswerText  answerText  = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String      fileName    = util.getClassName(obj.getClass()) + ".json";
        String      urlPart     = ((ReqMain.ReqForgotPassword1) obj).urlPart;
        String      req         = ((ReqMain.ReqForgotPassword1) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);

        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if (answerJSONObject.jsonObject.getJSONObject("error").getInt("code") !=0 ){
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {
                    utilPrefCustom.setToken(answerJSONObject.jsonObject.getString("token"));
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, getString(R.string.error_getting_data), answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);
    }

    private void loadForgetPassword2(Object obj) {
        AnswerText  answerText  = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String      fileName    = util.getClassName(obj.getClass()) + ".json";
        String      urlPart     = ((ReqMain.ReqForgotPassword2) obj).urlPart;
        String      req         = ((ReqMain.ReqForgotPassword2) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);

        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if (answerJSONObject.jsonObject.getJSONObject("error").getInt("code") !=0 ){
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, getString(R.string.error_getting_data), answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);
    }

    //BC_EVENT_GET_CARD_LIST_AVAILABLE_IN_REG

    // *****************************************************************************************************
    private void standartBehavior(AnswerText answerText) {
        if (answerText.status.equals(ConstStatus.ST_CONNECTION_ABSENT)) {
            utilSendBroadcast.sendMyBroadcastToast(getString(R.string.STATUS_CONNECTION_ABSENT_MSG), 1);
        } else if (answerText.status.equals(ConstStatus.ST_MAKE_LATER)) {
            utilSendBroadcast.sendMyBroadcastToast(getString(R.string.STATUS_MAKE_LATER_MSG), 1);
        } else if (answerText.status.equals(ConstStatus.ST_NOINTERNET)) {
            utilSendBroadcast.sendMyBroadcastToast(getString(R.string.STATUS_INTERNET_ABSENT_MSG), 1);
        } else if (answerText.status.equals(ConstStatus.ST_ESC)) {
            // необходимо обновить программу
        } else utilSendBroadcast.sendMyBroadcastToast(answerText.text, 1);
    }

    // *****************************************************************************************************
    private void loadStarter() {
        UtilFileManager utilFileManager = new UtilFileManager(this);

        app.dbClose();

        utilFileManager.copyFile(Const.DB_NAME);

        try { utilPrefCustom.setIsFirst(getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA).versionCode);
        } catch (PackageManager.NameNotFoundException e) { utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "", e);}

        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);
        utilSendBroadcast.sendMyBroadcastResult(event, ConstStatus.ST_TRUE, "", "");
    }



    private void loadHello(Object obj) {
        AnswerText answerText = new AnswerText(ConstStatus.ST_TRUE, "", "");

        String fileName     = util.getClassName(obj.getClass()) + ".json";
        String urlPart      = ((ReqMain.ReqHello) obj).urlPart;
        String req          = ((ReqMain.ReqHello) obj).getReq();

        Log.d(TAG, "1444 hello " + event );

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if (answerJSONObject.jsonObject.getJSONObject("error").getInt("code")!=0){
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {

                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);
    }

    // *****************************************************************************************************
    private void loadSignUpPhone_Hello(Object obj, Object objHello) {
        AnswerText answerText   = new AnswerText(ConstStatus.ST_TRUE, "", "");

        String fileNameHello    = util.getClassName(objHello.getClass()) + ".json";
        String urlPartHello     = ((ReqMain.ReqHello) objHello).urlPart;
        String reqHello         = ((ReqMain.ReqHello) objHello).getReq();

//        Log.d(TAG, "1444 phone hello " + obj.toString() );


        AnswerJSONObject answerJSONObjectHello = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPartHello, fileNameHello, reqHello, true);
        if (!answerJSONObjectHello.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObjectHello.status, "", answerJSONObjectHello.log);
        } else {
            try {
                if (answerJSONObjectHello.jsonObject.getJSONObject("error").getInt("code") !=0 ){
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObjectHello.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObjectHello.log);
                } else {
                    // ничего не делаем
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObjectHello.log);
            }
        }

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            String fileName = util.getClassName(obj.getClass()) + ".json";
            String urlPart = ((ReqMain.ReqSignUpPhone) obj).urlPart;
            String req = ((ReqMain.ReqSignUpPhone) obj).getReq();

            AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
            if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
                answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
            } else {
                try {
                    if (answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ){
                        answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                                answerJSONObject.log);
                    } else {
                        // ничего не делаем
                    }
                } catch (Exception e) {
                    utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                    answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
                }
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);
    }

    // *****************************************************************************************************
    private void loadSignUpPin(Object obj) {
        AnswerText answerText   = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String fileName         = util.getClassName(obj.getClass()) + ".json";
        String urlPart          = ((ReqMain.ReqSignUpPin) obj).urlPart;
        String req              = ((ReqMain.ReqSignUpPin) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if (answerJSONObject.jsonObject.getJSONObject("error").getInt("code")!=0){
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {
                    utilPrefCustom.setSessionToken(answerJSONObject.jsonObject.getString("sessionTokenToUseIfNotNull"));
                    utilPrefCustom.setClientNeedsToSpecifyPassword(answerJSONObject.jsonObject.getBoolean("clientNeedsToSpecifyPassword"));
                    utilPrefCustom.setclientNeedsToSpecifyEmail(answerJSONObject.jsonObject.getBoolean("clientNeedsToSpecifyEmail"));
                    utilPrefCustom.setClientNeedsToPayForTheCard(answerJSONObject.jsonObject.getBoolean("clientNeedsToPayForTheCard"));
                    utilPrefCustom.setInitiallySelectedCardId(answerJSONObject.jsonObject.getString("initiallySelectedCardId"));
                    utilPrefCustom.setclientNeedsToProvidePassportPic(answerJSONObject.jsonObject.getBoolean("clientNeedsToProvidePassportPic"));
                    utilPrefCustom.setclientNeedsToProvideUtilityBillPic(answerJSONObject.jsonObject.getBoolean("clientNeedsToProvideUtilityBillPic"));
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);
    }

    // *****************************************************************************************************
    private void loadSignUpPhoto(Object obj) {
        AnswerText answerText   = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String fileName         = util.getClassName(obj.getClass()) + ".json";
        String urlPart          = ((ReqMain.ReqSignUpPhoto) obj).urlPart;


        String req = "{\n" +
                "  \"installationIdentifier\": \"" + ((ReqMain.ReqSignUpPhoto) obj).installationIdentifier + "\",\n" +
//                "  \"phoneNumber\": \"" + "+" + utilPrefCustom.getPhoneNumber() + "\",\n" +
                "  \"sessionToken\": \"" + ((ReqMain.ReqSignUpPhoto) obj).sessionToken + "\",\n" +
                ((((ReqMain.ReqSignUpPhoto) obj).photoType == 0) ? "  \"passportPicBytes\": \"" : "  \"utilityPicBytes\": \"") + app.imageInBase64 + "\"\n" +
                "}";

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if (answerJSONObject.jsonObject.getJSONObject("error").getInt("code")!=0){
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {
                    utilPrefCustom.setClientNeedsToSpecifyPassword(answerJSONObject.jsonObject.getBoolean("clientNeedsToSpecifyPassword"));
                    utilPrefCustom.setclientNeedsToSpecifyEmail(answerJSONObject.jsonObject.getBoolean("clientNeedsToSpecifyEmail"));
                    utilPrefCustom.setClientNeedsToPayForTheCard(answerJSONObject.jsonObject.getBoolean("clientNeedsToPayForTheCard"));
                    utilPrefCustom.setInitiallySelectedCardId(answerJSONObject.jsonObject.getString("initiallySelectedCardId"));
                    utilPrefCustom.setclientNeedsToProvidePassportPic(answerJSONObject.jsonObject.getBoolean("clientNeedsToProvidePassportPic"));
                    utilPrefCustom.setclientNeedsToProvideUtilityBillPic(answerJSONObject.jsonObject.getBoolean("clientNeedsToProvideUtilityBillPic"));
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);
    }

    // *****************************************************************************************************
    private void loadSignUpPass(Object obj) {
        AnswerText answerText = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String fileName = util.getClassName(obj.getClass()) + ".json";
        String urlPart = ((ReqMain.ReqSignUpPass) obj).urlPart;
        String req = ((ReqMain.ReqSignUpPass) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if (answerJSONObject.jsonObject.getJSONObject("error").getInt("code")!=0){
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {
                    utilPrefCustom.setClientNeedsToSpecifyPassword(answerJSONObject.jsonObject.getBoolean("clientNeedsToSpecifyPassword"));
                    utilPrefCustom.setclientNeedsToSpecifyEmail(answerJSONObject.jsonObject.getBoolean("clientNeedsToSpecifyEmail"));
                    utilPrefCustom.setClientNeedsToPayForTheCard(answerJSONObject.jsonObject.getBoolean("clientNeedsToPayForTheCard"));
                    utilPrefCustom.setInitiallySelectedCardId(answerJSONObject.jsonObject.getString("initiallySelectedCardId"));
                    utilPrefCustom.setclientNeedsToProvidePassportPic(answerJSONObject.jsonObject.getBoolean("clientNeedsToProvidePassportPic"));
                    utilPrefCustom.setclientNeedsToProvideUtilityBillPic(answerJSONObject.jsonObject.getBoolean("clientNeedsToProvideUtilityBillPic"));
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);
    }

    // *****************************************************************************************************
    private void loadSignUpCardId(Object obj) {
        AnswerText answerText   = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String fileName         = util.getClassName(obj.getClass()) + ".json";
        String urlPart          = ((ReqMain.ReqSignUpCardId) obj).urlPart;
        String req              = ((ReqMain.ReqSignUpCardId) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if (answerJSONObject.jsonObject.getJSONObject("error").getInt("code")!=0){
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {
                    utilPrefCustom.setClientNeedsToSpecifyPassword(answerJSONObject.jsonObject.getBoolean("clientNeedsToSpecifyPassword"));
                    utilPrefCustom.setclientNeedsToSpecifyEmail(answerJSONObject.jsonObject.getBoolean("clientNeedsToSpecifyEmail"));
                    utilPrefCustom.setClientNeedsToPayForTheCard(answerJSONObject.jsonObject.getBoolean("clientNeedsToPayForTheCard"));
                    utilPrefCustom.setInitiallySelectedCardId(answerJSONObject.jsonObject.getString("initiallySelectedCardId"));
                    utilPrefCustom.setclientNeedsToProvidePassportPic(answerJSONObject.jsonObject.getBoolean("clientNeedsToProvidePassportPic"));
                    utilPrefCustom.setclientNeedsToProvideUtilityBillPic(answerJSONObject.jsonObject.getBoolean("clientNeedsToProvideUtilityBillPic"));
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, getString(R.string.error_getting_data), answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);
    }






    // *****************************************************************************************************
    private void loadGetAuthToken(Object obj) {
        AnswerText  answerText  = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String      fileName    = util.getClassName(obj.getClass()) + ".json";
        String      urlPart     = ((ReqMain.ReqGetAuthToken) obj).urlPart;
        String      req         = ((ReqMain.ReqGetAuthToken) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);

        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if (answerJSONObject.jsonObject.getJSONObject("error").getInt("code")!=0){
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {



                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, getString(R.string.error_getting_data), answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);
    }

    // *****************************************************************************************************
    private void loadGetCardList(Object obj) {
        AnswerText answerText   = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String fileName         = util.getClassName(obj.getClass()) + ".json";
        String urlPart          = ((ReqMain.ReqGetCardList) obj).urlPart;
        String req              = ((ReqMain.ReqGetCardList) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {
                    utilDbEdite.writeCards(answerJSONObject.jsonObject.getJSONArray("cards"));
                    Log.d(TAG, "1444:cards " + answerJSONObject.jsonObject.toString() );
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);
    }

    // *****************************************************************************************************
    private void loadGetTransactionList(Object obj) {
        AnswerText answerText   = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String fileName         = util.getClassName(obj.getClass()) + ".json";
        String urlPart          = ((ReqMain.ReqGetTransactionList) obj).urlPart;
        String req              = ((ReqMain.ReqGetTransactionList) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if (answerJSONObject.jsonObject.getJSONObject("error").getInt("code")!=0){
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {
                    utilDbEdite.writeTransactions(answerJSONObject.jsonObject.getJSONArray("transactions"),
                            ((ReqMain.ReqGetTransactionList) obj).cardId);
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);
    }


    private void loadGetTransactionListBefore(Object obj) {
        AnswerText answerText   = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String fileName         = util.getClassName(obj.getClass()) + ".json";
        String urlPart          = ((ReqMain.ReqGetTransactionListBefore) obj).urlPart;
        String req              = ((ReqMain.ReqGetTransactionListBefore) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if (answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ){
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {
                    utilDbEdite.writeTransactions(answerJSONObject.jsonObject.getJSONArray("transactions"),
                            ((ReqMain.ReqGetTransactionListBefore) obj).cardId);
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

//        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
     //   } else standartBehavior(answerText);
    }







    // *****************************************************************************************************
    private void loadSignInPhone_Hello(Object obj, Object objHello) {
        AnswerText answerText   = new AnswerText(ConstStatus.ST_TRUE, "", "");

        String fileNameHello    = util.getClassName(objHello.getClass()) + ".json";
        String urlPartHello     = ((ReqMain.ReqHello) objHello).urlPart;
        String reqHello         = ((ReqMain.ReqHello) objHello).getReq();
        String requestSmsPin    = "0";



        // hello
        AnswerJSONObject answerJSONObjectHello = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPartHello, fileNameHello, reqHello, true);
        if (!answerJSONObjectHello.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObjectHello.status, "", answerJSONObjectHello.log);
        } else {
            try {
                if ( answerJSONObjectHello.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObjectHello.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObjectHello.log);
                } else {
                    // ничего не делаем
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObjectHello.log);
            }
        }



        // авторизация
        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            String fileName = util.getClassName(obj.getClass()) + ".json";
            String urlPart  = ((ReqMain.ReqSignInPhone) obj).urlPart;
            String req      = ((ReqMain.ReqSignInPhone) obj).getReq();

            AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
            if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
                answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
            } else {
                try {
                    if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
                        answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"), answerJSONObject.log);

                        if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") == 6 ) {
                            requestSmsPin = "6";

                            Log.d(TAG, "1444 requestSmsPin: " + requestSmsPin );
                        }
                    } else {
                        utilPrefCustom.setToken(answerJSONObject.jsonObject.getString("token"));
                    }
                } catch (Exception e) {
                    utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                    answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
                }
            }
        }

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {

            Object objCardList = new ReqMain.ReqGetCardList(utilPrefCustom.getToken());

            String fileName = util.getClassName(objCardList.getClass()) + ".json";
            String urlPart  = ((ReqMain.ReqGetCardList) objCardList).urlPart;
            String req      = ((ReqMain.ReqGetCardList) objCardList).getReq();

            AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
            if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
                answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
            } else {
                try {
                    if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ){
                        answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"), answerJSONObject.log);
                    } else {
                        utilDbEdite.writeCards(answerJSONObject.jsonObject.getJSONArray("cards"));
                    }
                } catch (Exception e) {
                    utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                    answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
                }
            }
        }

        // TODO change hardcode

//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//
//        String date = df.format(Calendar.getInstance().getTime());



        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, requestSmsPin, "");
        } else if ( requestSmsPin.equals("6")) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, requestSmsPin, "");
        } else {
            standartBehavior(answerText);
        }
    }

    private void loadMoneypoloLink(Serializable obj) {
        AnswerText  answerText  = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String      fileName    = util.getClassName(obj.getClass()) + ".json";
        String      urlPart     = ((ReqMain.ReqMoneyPoloLink) obj).urlPart;
        String      req         = ((ReqMain.ReqMoneyPoloLink) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);

        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {

                    utilPrefCustom.setMoneyPoloData(
                            answerJSONObject.jsonObject.getString("postUrl"),
                            answerJSONObject.jsonObject.getString("formData"),
                            answerJSONObject.jsonObject.getString("successUrl"),
                            answerJSONObject.jsonObject.getString("failureUrl"));
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, getString(R.string.error_getting_data), answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, answerText.text, "");
        } else standartBehavior(answerText);
    }

//    private void loadAvailableCardList(Serializable obj) {
//        AnswerText answerText   = new AnswerText(ConstStatus.ST_TRUE, "", "");
//        String fileName         = util.getClassName(obj.getClass()) + ".json";
//        String urlPart          = ((ReqMain.ReqGetCardsAvailableAtRegistrationPhase) obj).urlPart;
//        String req              = ((ReqMain.ReqGetCardsAvailableAtRegistrationPhase) obj).getReq();
//
////        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req);
////        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
////            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
////        } else {
////            try {
////                Log.d(TAG, "1444:cards " + answerJSONObject.jsonObject.toString() );
////                if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
////                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
////                            answerJSONObject.log);
////                } else {
////
////                    Log.d(TAG, "1444:cards " + answerJSONObject.jsonObject.toString() );
////                }
////            } catch (Exception e) {
////                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
////                answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
////            }
////        }
//
//        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req);
//
//        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
//            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
//        } else {
//            try {
//                if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
//                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"), answerJSONObject.log);
//                } else {
//                  //  utilDbEdite.writeTransactions(answerJSONObject.jsonObject.getJSONArray("transactions"), cardId);
//                    Log.d(TAG, "1444:cards " + answerJSONObject.jsonObject.toString() );
//                }
//            } catch (Exception e) {
//                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
//                answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
//            }
//        }
//
//        if (!app.getProgressBar().getStatus()) return;
//        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);
//
//        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
//            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
//        } else standartBehavior(answerText);
//    }

    private void loadAvailableCardList(Object obj) {
        AnswerText answerText   = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String fileName         = util.getClassName(obj.getClass()) + ".json";
        String urlPart          = ((ReqMain.ReqGetCardsAvailableAtRegistrationPhase) obj).urlPart;
        String req              = ((ReqMain.ReqGetCardsAvailableAtRegistrationPhase) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, false);
        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if (answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {
                    utilPrefCustom.setCardAvailable(answerJSONObject.jsonObject.get("cards").toString());
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);Log.d(TAG, "1444:cards     8 " );
    }

    private void loadValidateCard(Serializable obj) {
        AnswerText  answerText  = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String      fileName    = util.getClassName(obj.getClass()) + ".json";
        String      urlPart     = ((ReqMain.ReqValidateCard) obj).urlPart;
        String      req         = ((ReqMain.ReqValidateCard) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);

        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {


                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, getString(R.string.error_getting_data), answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, answerText.text, "");
        } else standartBehavior(answerText);
    }

    private void loadAccountDetails(Serializable obj) {
        AnswerText  answerText  = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String      fileName    = util.getClassName(obj.getClass()) + ".json";
        String      urlPart     = ((ReqMain.ReqGetAccountDetails) obj).urlPart;
        String      req         = ((ReqMain.ReqGetAccountDetails) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);

        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {
                    answerText.text = answerJSONObject.jsonObject.toString();
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, getString(R.string.error_getting_data), answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, answerText.text, "");
        } else standartBehavior(answerText);
    }

    private void loadChangePassword(Serializable obj) {
        AnswerText  answerText  = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String      fileName    = util.getClassName(obj.getClass()) + ".json";
        String      urlPart     = ((ReqMain.ReqChangePassword) obj).urlPart;
        String      req         = ((ReqMain.ReqChangePassword) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);

        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {
                    utilPrefCustom.setToken(answerJSONObject.jsonObject.getString("token"));

                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, getString(R.string.error_getting_data), answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, answerText.text, "");
        } else standartBehavior(answerText);
    }

    private void loadAuthToken(Serializable obj) {
        AnswerText answerText   = new AnswerText(ConstStatus.ST_TRUE, "", "");

        // авторизация
        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            String fileName = util.getClassName(obj.getClass()) + ".json";
            String urlPart  = ((ReqMain.ReqGetAuthToekn) obj).urlPart;
            String req      = ((ReqMain.ReqGetAuthToekn) obj).getReq();

            AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
            if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
                answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
            } else {
                try {
                    if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
                        answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"), answerJSONObject.log);
                    } else {
                        utilPrefCustom.setToken(answerJSONObject.jsonObject.getString("token"));
                    }
                } catch (Exception e) {
                    utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                    answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
                }
            }
        }

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {

            Object objCardList = new ReqMain.ReqGetCardList(utilPrefCustom.getToken());

            String fileName = util.getClassName(objCardList.getClass()) + ".json";
            String urlPart  = ((ReqMain.ReqGetCardList) objCardList).urlPart;
            String req      = ((ReqMain.ReqGetCardList) objCardList).getReq();

            AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
            if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
                answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
            } else {
                try {
                    if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ){
                        answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"), answerJSONObject.log);
                    } else {
                        utilDbEdite.writeCards(answerJSONObject.jsonObject.getJSONArray("cards"));
                    }
                } catch (Exception e) {
                    utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                    answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
                }
            }
        }

//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//
//        String date = df.format(Calendar.getInstance().getTime());

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, "", "");
        } else standartBehavior(answerText);
    }

    private void loadResendSms(Serializable obj) {
        AnswerText  answerText  = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String      fileName    = util.getClassName(obj.getClass()) + ".json";
        String      urlPart     = ((ReqMain.ReqResendSms) obj).urlPart;
        String      req         = ((ReqMain.ReqResendSms) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);

        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                }
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                answerText = new AnswerText(ConstStatus.ST_FALSE, getString(R.string.error_getting_data), answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, answerText.text, "");
        } else standartBehavior(answerText);
    }



    private void loadTransaction(){

        AnswerText answerText   = new AnswerText(ConstStatus.ST_TRUE, "", "");

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            String cardId = utilDbGet.getFirstCardID();
            Object objTransactions = new ReqMain.ReqGetTransactionListBefore(utilPrefCustom.getToken(),
                    cardId, "20", "2040-10-22T03:13:48");

            String fileName = util.getClassName(objTransactions.getClass()) + ".json";
            String urlPart  = ((ReqMain.ReqGetTransactionListBefore) objTransactions).urlPart;
            String req      = ((ReqMain.ReqGetTransactionListBefore) objTransactions).getReq();

            AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);
            if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
                answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
            } else {
                try {
                    if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
                        answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"), answerJSONObject.log);
                    } else {
                        utilDbEdite.writeTransactions(answerJSONObject.jsonObject.getJSONArray("transactions"), cardId);
                    }
                } catch (Exception e) {
                    utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге JSON объекта", e);
                    answerText = new AnswerText(ConstStatus.ST_FALSE, "Ошибка при получении данных с сервера", answerJSONObject.log);
                }
            }
        }

    }

    private void loadSaveClientPhoneContacts(Serializable obj) {
        AnswerText  answerText  = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String      fileName    = util.getClassName(obj.getClass()) + ".json";
        String      urlPart     = ((ReqMain.ReqSaveClientPhoneContacts) obj).urlPart;
        String      req         = ((ReqMain.ReqSaveClientPhoneContacts) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);

        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                }
            } catch (Exception e) {
                answerText = new AnswerText(ConstStatus.ST_FALSE, getString(R.string.error_getting_data), answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, answerText.text, "");
        } else standartBehavior(answerText);
    }


    private void loadSystemMessages(Serializable obj) {
        AnswerText  answerText  = new AnswerText(ConstStatus.ST_TRUE, "", "");
        String      fileName    = util.getClassName(obj.getClass()) + ".json";
        String      urlPart     = ((ReqMain.ReqGetSystemMessages) obj).urlPart;
        String      req         = ((ReqMain.ReqGetSystemMessages) obj).getReq();

        AnswerJSONObject answerJSONObject = utilWeb.getJSONFromHttpUrlConnection(BuildConfig.URL_MAIN + urlPart, fileName, req, true);

        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE)) {
            answerText = new AnswerText(answerJSONObject.status, "", answerJSONObject.log);
        } else {
            try {
                if ( answerJSONObject.jsonObject.getJSONObject("error").getInt("code") != 0 ) {
                    answerText = new AnswerText(ConstStatus.ST_FALSE, answerJSONObject.jsonObject.getJSONObject("error").getString("message"),
                            answerJSONObject.log);
                } else {
                    answerText.text = answerJSONObject.jsonObject.toString();
                }
            } catch (Exception e) {
                answerText = new AnswerText(ConstStatus.ST_FALSE, getString(R.string.error_getting_data), answerJSONObject.log);
            }
        }

        if (!app.getProgressBar().getStatus()) return;
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);

        if (answerText.status.equals(ConstStatus.ST_TRUE)) {
            utilSendBroadcast.sendMyBroadcastResult(event, answerText.status, answerText.text, "");
        } else standartBehavior(answerText);
    }



}