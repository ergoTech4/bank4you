package uk.bank4you.client.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import uk.bank4you.client.activities.ActSignIn;
import uk.bank4you.client.activities.ActSignUpPin;

public class IncomingSms extends BroadcastReceiver {

    public IncomingSms() {
    }


    public void onReceive(Context context, Intent intent) {

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();
        ActSignUpPin inst = ActSignUpPin.instance();
        ActSignIn inst2 = ActSignIn.instance();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                assert pdusObj != null;
                for (Object aPdusObj : pdusObj) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) aPdusObj);

                    String message = currentMessage.getDisplayMessageBody();

                    Log.i("SmsReceiver", "; message: " + message);

                    if (message.contains("Bank4YOU PIN")) {
                        String pin = message.substring(message.length() - 4, message.length());

                        try {
                            inst.updateList(pin);
                        } catch (NullPointerException ignored) {}

                        try {
                            inst2.updateList(pin);
                        } catch (NullPointerException ignored) {}
                    }

                }
            }

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver: " +e);

        }
    }

}