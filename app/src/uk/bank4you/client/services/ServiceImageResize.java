package uk.bank4you.client.services;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import uk.bank4you.client.App;
import uk.bank4you.client.utils.UtilFileManager;
import uk.bank4you.client.utils.UtilLog;
import uk.bank4you.client.constants.ConstBC;
import uk.bank4you.client.constants.ConstEX;
import uk.bank4you.client.constants.ConstProgress;
import uk.bank4you.client.constants.ConstStatus;

public class ServiceImageResize extends IntentService {
    private int event;
    private UtilLog utilLog;
    private UtilSendBroadcast utilSendBroadcast;
    private App app;

    public ServiceImageResize() {
        super("ServiceImageResize");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        event = intent.getIntExtra("EVENT", 0);
        utilLog = new UtilLog(this);
        utilSendBroadcast = new UtilSendBroadcast(this);
        app = (App) getApplicationContext();

        switch (event) {
            case ConstBC.BC_EVENT_RESIZE_IMAGE:
                resizeImage(
                        intent.getStringExtra(ConstEX.EX_imgFilePathFrom),
                        intent.getStringExtra(ConstEX.EX_imgFilePathTo),
                        intent.getStringExtra(ConstEX.EX_uri));
                break;

        }
    }

    // *****************************************************************************************************
    private void resizeImage(String imgFilePathFrom, String imgFilePathTo, String uriString) {
        UtilFileManager utilFileManager = new UtilFileManager(this);

        utilFileManager.resizeImage(imgFilePathFrom, imgFilePathTo, Uri.parse(uriString));
        app.imageInBase64 = utilFileManager.bitmapToBase64(imgFilePathTo);

        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_STOP, null);
        utilSendBroadcast.sendMyBroadcastResult(event, ConstStatus.ST_TRUE, "", imgFilePathTo);
    }
}