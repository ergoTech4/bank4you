package uk.bank4you.client.services.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.helpshift.Core;
import uk.bank4you.client.BuildConfig;
import uk.bank4you.client.utils.UtilLog;

import java.io.IOException;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private UtilLog utilLog;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        utilLog = new UtilLog(this);

        // Make a call to Instance API
        InstanceID instanceID = InstanceID.getInstance(this);
        try {
            // request token that will be used by the server to send push notifications
            String token = instanceID.getToken(BuildConfig.SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE);

            utilLog.myLog("GCM Registration Token: " + token);

            // pass along this data
            sendRegistrationToServer(token);
            saveToken(token);
        } catch (IOException e) {
            utilLog.myLogExeption(e);
        }
    }

    private void saveToken(String token) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.edit().putString("token", token).apply();
    }

    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
        Core.registerDeviceToken(this, token);
    }
}
