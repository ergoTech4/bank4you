package uk.bank4you.client.services;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import uk.bank4you.client.constants.ConstBC;
import uk.bank4you.client.constants.ConstEX;
import uk.bank4you.client.constants.ConstProgress;
import uk.bank4you.client.response.ReqMain;

import java.io.Serializable;

public class UtilStartService {
    private Context             context;
    private UtilSendBroadcast   utilSendBroadcast;

    public UtilStartService(Context context) {
        this.context        = context;
        utilSendBroadcast   = new UtilSendBroadcast(this.context);
    }

    public void startMyIntentService(Serializable serializableClass) {
        /*int event = ((ReqMain.ReqBasic)serializableClass).event;
        if (event != ConstIntents.BC_EVENT_CheckUpdates)
            utilSendBroadcast.sendMyBroadcastProgress(ConstIntents.BC_STATUS_PROGRESS_START, ((ReqMain.ReqBasic) serializableClass).msg);*/
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_START, ((ReqMain.ReqBasic) serializableClass).msg);
        Intent intent = new Intent(context, ServiceDownload.class);
        intent.putExtra(ConstEX.SERIALIZABLE_REQUEST, serializableClass);
        context.startService(intent);
    }

    public void startResizeImage(String imgFilePathFrom, String imgFilePathTo, Uri uri) {
        utilSendBroadcast.sendMyBroadcastProgress(ConstProgress.BC_STATUS_PROGRESS_START, "Обработка изображения");
        Intent intent = new Intent(context, ServiceImageResize.class);
        intent.putExtra(ConstEX.EX_EVENT, ConstBC.BC_EVENT_RESIZE_IMAGE);
        intent.putExtra(ConstEX.EX_imgFilePathFrom, imgFilePathFrom);
        intent.putExtra(ConstEX.EX_imgFilePathTo, imgFilePathTo);
        intent.putExtra(ConstEX.EX_uri, uri.toString());
        context.startService(intent);
    }
}