package uk.bank4you.client.services;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import uk.bank4you.client.constants.Const;
import uk.bank4you.client.constants.ConstDb;

/**
 * Created by ergo on 5/5/16.
 */

public class DatabaseHelper extends SQLiteOpenHelper {


    public DatabaseHelper(Context context) {
        super(context, Const.DB_NAME, null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE "+ ConstDb.tblCards + " (" +
                "_id" + " INTEGER , "+
                "cardID" + " INT NULL , "+
                "cardNo" + " TEXT NULL , "+
                "cardCurrency" + " TEXT NULL , "+
                "availableBalance" + " TEXT NULL , "+
                "holdBalance" + " TEXT NULL , "+
                "effectiveDate" + " TEXT NULL , "+
                "expiryDate" + " TEXT NULL , "+
                "cardEmbossLine" + " TEXT NULL)");

//        CREATE TABLE "cards" (
//                _id INTEGER
//                , cardID INT NULL,
//        cardNo TEXT NULL,
//                cardCurrency TEXT NULL, availableBalance
//        TEXT NULL, holdBalance TEXT NULL, effectiveDate TEXT NULL, expiryDate TEXT NULL,
//        cardEmbossLine TEXT NULL)



        db.execSQL("CREATE TABLE transactions (" +
                "_id INT ," +
                "category TEXT ," +
                "id INT ," +
                "dateTimeUtc TEXT ," +
                "amount TEXT ," +
                "currencyCode TEXT ," +
                "amountInCardCurrency TEXT ," +
                "additionalCommissionInCardCurrency TEXT ," +
                "info TEXT ," +
                "transactionType TEXT ," +
                "mcc TEXT ," +
                "comment TEXT ," +
                "cardId INT ," +
                "imgUrl TEXT ," +
                "fav INT , " +
                "PRIMARY KEY(id)" +
                ")");

//        InsertDepts(db);
//
//        CREATE TABLE transactions (
//        `_id`	INT,
//        `category`	TEXT,
//        `id`	INT,
//        `dateTimeUtc`	TEXT,
//        `amount`	TEXT,
//        `currencyCode`	TEXT,
//        `amountInCardCurrency`	TEXT,
//        `additionalCommissionInCardCurrency`	TEXT,
//        `info`	TEXT,
//        `transactionType`	TEXT,
//        `mcc`	TEXT,
//        `comment`	TEXT,
//        `cardId`	INT,
//        `imgUrl`	TEXT,
//        `fav`	INT,
//                PRIMARY KEY(id)
//        )
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ ConstDb.tblCards);
        db.execSQL("DROP TABLE IF EXISTS "+ ConstDb.tblTransactions);

        onCreate(db);
    }
}
