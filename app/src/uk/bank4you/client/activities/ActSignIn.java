package uk.bank4you.client.activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import uk.bank4you.client.Demo.ActCabinetDemo;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.*;
import uk.bank4you.client.response.ReqMain;

public class ActSignIn extends BasicActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final int REQUEST_CODE_SOME_FEATURES_PERMISSIONS = 123;
    private static final String TAG = "ActSignIn";
    private TextView    btnPass;
    private TextView    btnReg;
    private TextView    btnHaveCard;
    private TextView    tryDemo;
    private Button      btnLogin;
    private EditText    edtPhone;
    private EditText    edtPass;
    private ImageView   preview;
    private String      tryGetPhoneNumber = "";
    private EditText    edtPin1;
    private EditText    edtPin2;
    private EditText    edtPin3;
    private EditText    edtPin4;
    private Dialog      dialog;

    private static ActSignIn inst;

    public static ActSignIn instance() {
        return inst;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_sign_in);

        initialize();

        createDialogSMS();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int hasPhoneStatePermission     = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
            int hasReadContactsPermission   = checkSelfPermission(Manifest.permission.READ_CONTACTS);
            List<String> permissions = new ArrayList<String>();


            if (hasPhoneStatePermission != PackageManager.PERMISSION_GRANTED
                    && hasReadContactsPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.READ_PHONE_STATE);
                permissions.add(Manifest.permission.READ_CONTACTS);

            } else if (utilPrefCustom.getPhoneNumber().length() != 0) {
                    edtPhone.setText(utilPrefCustom.getPhoneNumber());
                    edtPhone.setSelection(utilPrefCustom.getPhoneNumber().length());
                } else {

                TelephonyManager telManager = (TelephonyManager) this
                        .getSystemService(Context.TELEPHONY_SERVICE);

                tryGetPhoneNumber = telManager.getLine1Number();

                if (tryGetPhoneNumber != null) {
                    edtPhone.setText(String.format("%s", tryGetPhoneNumber));
                    edtPhone.setSelection(tryGetPhoneNumber.length());
                }
            }

            if (!permissions.isEmpty()) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), REQUEST_CODE_SOME_FEATURES_PERMISSIONS);
            }

        } else {
            if (utilPrefCustom.getPhoneNumber().length() != 0) {
                edtPhone.setText(utilPrefCustom.getPhoneNumber());
                edtPhone.setSelection(utilPrefCustom.getPhoneNumber().length());
            } else {

                TelephonyManager telManager = (TelephonyManager) this
                        .getSystemService(Context.TELEPHONY_SERVICE);

                tryGetPhoneNumber = telManager.getLine1Number();

                if (tryGetPhoneNumber != null) {
                    edtPhone.setText(String.format("%s", tryGetPhoneNumber));
                    edtPhone.setSelection(tryGetPhoneNumber.length());
                }
            }

            if (utilPrefCustom.getIsFirst() < util.getVersion()) {
                utilLog.myLogFlurry(utilPrefCustom.getIsFirst() == 0 ? Const.EV_INSTALL : Const.EV_UPDATE, "", null);
                utilStartService.startMyIntentService(new ReqMain.ReqStarter());
            }
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = edtPhone.getText().toString();

                if ( phoneNumber.length() > 6 ) {
                    if ( !phoneNumber.startsWith("+") ) {
                        phoneNumber = "+" + phoneNumber;
                    }

                    utilPrefCustom.setPhoneNumber(phoneNumber);
                    utilStartService.startMyIntentService(
                            new ReqMain.ReqSignInPhone(util.id(ActSignIn.this),
                                    phoneNumber,
                                    edtPass.getText().toString()));
                    hideKeyboard();
                }
            }
        });

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String link = "https://bank4you.uk/info/prepaid_card_terms_and_condition.html";

                Intent intent = new Intent(ConstIntents.IN_ActWebView);
                intent.putExtra(ConstStatus.ST_TERMS, true);
                intent.putExtra(ConstEX.EX_URL, link);
                startActivity(intent);
            }
        });

        btnPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ConstIntents.IN_ActRestore));
            }
        });

        btnHaveCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ConstIntents.IN_ActValidateCard));
            }
        });

        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utilPrefCustom.setToken("");
                startActivity(new Intent(ConstIntents.IN_ActMain));
                finish();
            }
        });

        (findViewById(R.id.mainLL)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });

        tryDemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActSignIn.this, ActCabinetDemo.class));
            }
        });

        edtPhone.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ( keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 1 ) {
                    edtPass.requestFocus();
                    edtPass.onWindowFocusChanged(true);
                    return true;
                }
                return false;
            }
        });

        edtPass.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 1)) {
                    String phoneNumber = edtPhone.getText().toString();

                    if ( phoneNumber.length() > 6 && edtPass.getText().toString().length() > 3 ) {
                        if ( !phoneNumber.startsWith("+") ) {
                            phoneNumber = "+" + phoneNumber;
                        }

                        utilPrefCustom.setPhoneNumber(phoneNumber);
                        utilStartService.startMyIntentService(
                                new ReqMain.ReqSignInPhone(util.id(ActSignIn.this),
                                        phoneNumber,
                                        edtPass.getText().toString()));
                        hideKeyboard();
                    }
                    return true;
                }
                return false;
            }
        });

        edtPin4.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 1) {
                    String pin = edtPin1.getText().toString() + edtPin2.getText().toString() + edtPin3.getText().toString() + edtPin4.getText().toString();

                    if ( pin.length() == 4 ) {
                        String phoneNumber = utilPrefCustom.getPhoneNumber();

                        utilStartService.startMyIntentService(
                                new ReqMain.ReqGetAuthToekn(util.id(ActSignIn.this),
                                        phoneNumber,
                                        pin));
                        hideKeyboard();

                        edtPin1.setText("");
                        edtPin2.setText("");
                        edtPin3.setText("");
                        edtPin4.setText("");

//                        if ( phoneNumber.length() > 6 ) {
//                            if (!phoneNumber.startsWith("+")) {
//                                phoneNumber = "+" + phoneNumber;
//                            }
//                        }
                      //  utilStartService.startMyIntentService(new ReqMain.ReqForgotPassword1(phoneNumber, pin));
                    }

                    return true;
                }
                return false;
            }
        });

    }

    private void initialize() {
        btnLogin    = (Button) findViewById(R.id.btnLogin);

        btnReg      = (TextView) findViewById(R.id.btnReg);
        btnPass     = (TextView) findViewById(R.id.btnPass);
        btnHaveCard = (TextView) findViewById(R.id.btn_have_card);
        tryDemo     = (TextView) findViewById(R.id.try_demo_button);

        preview     = (ImageView) findViewById(R.id.btn_preview);

        edtPhone    = (EditText) findViewById(R.id.edit_field_phone);
        edtPass     = (EditText) findViewById(R.id.edtPass);
    }

    @Override
    protected void callOnSignInPhone(String status, String text) {
        if ( text.equals("6") ) {
            dialog.show();
            edtPin1.requestFocus();
        }

        if (status.equals(ConstStatus.ST_TRUE)) {
            startActivity(new Intent(ConstIntents.IN_ActCabinet).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
    }

    private void createDialogSMS() {
        dialog = new Dialog( this, R.style.Base_Theme_AppCompat_Dialog);
        dialog.setContentView(R.layout.dialog_pin);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        final Button    send       = (Button)       dialog.getWindow().findViewById(R.id.call_pin_send);
        final Button    resend     = (Button)       dialog.getWindow().findViewById(R.id.call_pin_resend);

        edtPin1  = (EditText) dialog.findViewById(R.id.edtPin1);
        edtPin2  = (EditText) dialog.findViewById(R.id.edtPin2);
        edtPin3  = (EditText) dialog.findViewById(R.id.edtPin3);
        edtPin4  = (EditText) dialog.findViewById(R.id.edtPin4);

        edtPin1.addTextChangedListener(new GenericTextWatcher(edtPin1));
        edtPin2.addTextChangedListener(new GenericTextWatcher(edtPin2));
        edtPin3.addTextChangedListener(new GenericTextWatcher(edtPin3));
        edtPin4.addTextChangedListener(new GenericTextWatcher(edtPin4));


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pin = edtPin1.getText().toString() + edtPin2.getText().toString() + edtPin3.getText().toString() + edtPin4.getText().toString();

                if ( pin.length() == 4 ) {
                    String phoneNumber = utilPrefCustom.getPhoneNumber();

                    utilStartService.startMyIntentService(
                            new ReqMain.ReqGetAuthToekn(util.id(ActSignIn.this),
                                    phoneNumber,
                                    pin));
                    hideKeyboard();
                    edtPin1.setText("");
                    edtPin2.setText("");
                    edtPin3.setText("");
                    edtPin4.setText("");
                }

                dialog.dismiss();
            }
        });

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = utilPrefCustom.getPhoneNumber();
                utilStartService.startMyIntentService(
                        new ReqMain.ReqResendSms(util.id(ActSignIn.this),
                                phoneNumber));
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch ( requestCode ) {
            case REQUEST_CODE_SOME_FEATURES_PERMISSIONS: {
                for( int i = 0; i < permissions.length; i++ ) {
                    if( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
                        TelephonyManager telManager  = (TelephonyManager)this
                                .getSystemService(Context.TELEPHONY_SERVICE);

                        tryGetPhoneNumber = telManager.getLine1Number();
                        edtPhone.setText(tryGetPhoneNumber);

                        Log.d( "Permissions", "Permission Granted: " + permissions[i] );
                    } else if( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
                        Log.d( "Permissions", "Permission Denied: " + permissions[i] );
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    private class GenericTextWatcher implements TextWatcher {
        private View view;
        private GenericTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        public void onTextChanged(CharSequence cs, int start, int before, int count) {}

        public void afterTextChanged(Editable editable) {
            switch(view.getId()){
                case R.id.edtPin1:
                    if (!edtPin1.getText().toString().equals("")) {
                        edtPin2.requestFocus();
                        if (!edtPin2.getText().toString().equals("")) {
                            edtPin2.setSelection(1);
                        }
                    }
                    break;
                case R.id.edtPin2:
                    if (!edtPin2.getText().toString().equals("")) {
                        edtPin3.requestFocus();
                        if (!edtPin3.getText().toString().equals("")) {
                            edtPin3.setSelection(1);
                        }
                    } else {
                        edtPin1.requestFocus();
                        if (!edtPin1.getText().toString().equals("")) {
                            edtPin1.setSelection(1);
                        }
                    }
                    break;
                case R.id.edtPin3:
                    if (!edtPin3.getText().toString().equals("")) {
                        edtPin4.requestFocus();
                        if (!edtPin4.getText().toString().equals("")) {
                            edtPin4.setSelection(1);
                        }
                    } else {
                        edtPin2.requestFocus();
                        if (!edtPin2.getText().toString().equals("")) {
                            edtPin2.setSelection(1);
                        }
                    }
                    break;
                case R.id.edtPin4:
                    if (edtPin4.getText() == null || edtPin4.getText().toString().equals("")) {
                        edtPin3.requestFocus();
                        if (!edtPin3.getText().toString().equals("")) {
                            edtPin3.setSelection(1);
                        }
                    }

                    break;
            }
        }
    }


    public void updateList(final String smsMessage) {

        edtPin1.setText(String.valueOf(smsMessage.charAt(0)));
        edtPin2.setText(String.valueOf(smsMessage.charAt(1)));
        edtPin3.setText(String.valueOf(smsMessage.charAt(2)));
        edtPin4.setText(String.valueOf(smsMessage.charAt(3)));

        String pin = edtPin1.getText().toString() + edtPin2.getText().toString() + edtPin3.getText().toString() + edtPin4.getText().toString();
        String phoneNumber = utilPrefCustom.getPhoneNumber();

        utilStartService.startMyIntentService(
                new ReqMain.ReqGetAuthToekn(util.id(ActSignIn.this),
                        phoneNumber,
                        pin));

        edtPin1.setText("");
        edtPin2.setText("");
        edtPin3.setText("");
        edtPin4.setText("");
    }

    @Override
    protected void onResume() {
        super.onResume();
        inst = this;
//
//        try {
//            final String[] SELF_PROJECTION = new String[]{ContactsContract.Profile.DISPLAY_NAME};
//
//            Cursor cursor = this.getContentResolver().query(
//                    ContactsContract.Profile.CONTENT_URI, SELF_PROJECTION, null, null, null);
//
//            assert cursor != null;
//            cursor.moveToFirst();
//
//            Log.d(TAG, "1333 name: " + cursor.getString(0));
//
//            cursor.close();
//
//        } catch (Exception ignored) {
//            ignored.printStackTrace();
//        }
    }
}
