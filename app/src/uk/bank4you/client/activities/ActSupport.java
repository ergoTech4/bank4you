package uk.bank4you.client.activities;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.helpshift.support.Support;

import uk.bank4you.client.Demo.ActCabinetDemo;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstEX;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.constants.ConstStatus;

public class ActSupport extends BasicActivity {

    private LinearLayout    cardsLL, payLL, settingsLL, supportLL;
    private ImageView       imageBack;
    private RelativeLayout  chat, callUs, termsCond, faq;
    private boolean         demo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_support);

        initialize();

        initializeMenu();

        demo = getIntent().getBooleanExtra("demo", false);

        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Support.showConversation(ActSupport.this);
            }
        });

        callUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createCallDialog();
            }
        });

        termsCond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String link = "http://bank4you.uk/info/prepaid_card_terms_and_condition.html";

                Intent intent = new Intent(ConstIntents.IN_ActWebView);
                intent.putExtra(ConstEX.EX_URL, link);
                startActivity(intent);
            }
        });

        faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConstIntents.IN_ActFAQ);
                startActivity(intent);
            }
        });
    }

    private void createCallDialog(){
        final Dialog dialog = new Dialog( this, R.style.Base_Theme_AppCompat_Dialog);
        dialog.setContentView(R.layout.dialog_call_support);

        final Button cancel    = (Button)       dialog.getWindow().findViewById(R.id.call_support_cancel);
        final Button call      = (Button)       dialog.getWindow().findViewById(R.id.call_support_call);

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call("tel:+4402036375915" );
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void initialize() {
        imageBack   = (ImageView) findViewById(R.id.img_back);

        chat        = (RelativeLayout) findViewById(R.id.chat_rl);
        callUs      = (RelativeLayout) findViewById(R.id.call_us_rl);
        termsCond   = (RelativeLayout) findViewById(R.id.terms_and_conditions);
        faq         = (RelativeLayout) findViewById(R.id.faq_rl);
    }

    private void initializeMenu() {

        cardsLL         = (LinearLayout) findViewById(R.id.act_cabinet_menu_cards);
        payLL           = (LinearLayout) findViewById(R.id.act_cabinet_menu_pay);
        settingsLL      = (LinearLayout) findViewById(R.id.act_cabinet_menu_settings);
        supportLL       = (LinearLayout) findViewById(R.id.act_cabinet_menu_support);

        cardsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !cardsLL.isSelected() ) {
                    cardsLL.setSelected(true);
                    payLL.setSelected(false);
                    settingsLL.setSelected(false);
                    supportLL.setSelected(false);

                    if (demo) {
                        startActivity(new Intent(ActSupport.this, ActCabinetDemo.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    } else {
                        startActivity(new Intent(ConstIntents.IN_ActCabinet).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    }                }
            }
        });

        payLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !payLL.isSelected() ) {
                    payLL.setSelected(true);
                    cardsLL.setSelected(false);
                    settingsLL.setSelected(false);
                    supportLL.setSelected(false);

                    startActivity(new Intent(ConstIntents.IN_ActPay).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                }
            }
        });

        settingsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !settingsLL.isSelected() ) {
                    settingsLL.setSelected(true);
                    payLL.setSelected(false);
                    cardsLL.setSelected(false);
                    supportLL.setSelected(false);

                    Intent intent = new Intent(ConstIntents.IN_ActSettings).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("demo", demo);

                    startActivity(intent);
                }
            }
        });

        supportLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !supportLL.isSelected() ) {
                    supportLL.setSelected(true);
                    payLL.setSelected(false);
                    settingsLL.setSelected(false);
                    cardsLL.setSelected(false);
                }
            }
        });
    }

    @SuppressWarnings("MissingPermission")
    private void call(String finalUrl) {
        try {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(finalUrl));

            this.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        supportLL.setSelected(true);
        payLL.setSelected(false);
        settingsLL.setSelected(false);
        cardsLL.setSelected(false);
    }

}
