package uk.bank4you.client.activities;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import uk.bank4you.client.R;
import uk.bank4you.client.fragments.FrCardTypeStudent;
import uk.bank4you.client.multyfields.AvailableCard;
import uk.bank4you.client.response.ReqMain;
import uk.bank4you.client.constants.*;
import uk.bank4you.client.fragments.FrCardTypeTaxFree;

public class ActNewCardTypeSelected extends BasicActivity {

    private static final String TAG = "ActNewCardTypeSelected";
    private ViewPager pager;
    private PagerAdapter pagerAdapter;
    private Button btnOrderCard;
    private LinearLayout currencyLL;
    private TextView txtPrice;
    private TextView txtCharges;
    private TextView txtAdv;
//    private EnumCurrency enumCurrency;
    private ImageView imgBack;

    private ArrayList<ArrayList<AvailableCard>> arrayListsCards;
    private ArrayList<AvailableCard> currentCards ;


    private String jsonCardAvailable;
    private int currencyID = 0;

   // private AvailableCard currentCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_new_card_type_selected);

        initialize();

        jsonCardAvailable = utilPrefCustom.getCardAvailable();

        parseJsonAvailableCard();

        if (arrayListsCards.size() == 0) {
            utilStartService.startMyIntentService(
                    new ReqMain.ReqGetCardsAvailableAtRegistrationPhase());
        } else {

            currentCards = arrayListsCards.get(0);

            setUpCurrency(currentCards);
        }
        pager.setAdapter(pagerAdapter);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
//                setCurrency();

                if ( arrayListsCards.get(position).size() < currencyID ) {
                    currencyID = 0;
                }

                currentCards = arrayListsCards.get(position);
                setUpCurrency(currentCards);
            }
        });

        btnOrderCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRequest(currentCards.get(currencyID));
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txtAdv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FrCardTypeTaxFree frag1 = (FrCardTypeTaxFree)pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                frag1.showAdv();
            }
        });

        txtCharges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActNewCardTypeSelected.this, ActivityWebView.class);

                intent.putExtra(ConstEX.EX_URL, currentCards.get(0).tariffUrl);

                startActivity(intent);
            }
        });
    }

    private void setUpCurrency(ArrayList<AvailableCard> currentList) {

       // ArrayList<String> currencyArray = new ArrayList<String>();

        LinearLayout.LayoutParams params= new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

       // params.setMargins( 22, 14, 22, 14 );
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float density = metrics.density;

        currencyLL.removeAllViews();

        for ( int i = 0; i < currentList.size(); i++ ) {
            //currencyArray.add(currentList.get(i).currency);
            final TextView tv = new TextView(this);

            tv.setPadding( (int)(30 * density), (int)(8 * density), (int)(30 * density), (int)(8 * density) );
            tv.setText(currentList.get(i).currency);
            tv.setId(i);
            tv.setLayoutParams(params);
            tv.setTextColor(getResources().getColor(R.color.black));
            tv.setBackgroundResource(R.color.transparent);

            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    util.myToast("currency id : " + tv.getId());
                    currencyID = tv.getId();
                    setUpCurrency(currentCards);
                }
            });

            if ( i == currencyID ) {
                tv.setBackgroundResource(R.color.background_green);
//                tv.setPaintFlags(tv.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
            }
            currencyLL.addView(tv);

        }


        txtPrice.setText(String.format("%s %s", currentList.get(currencyID).orderPriceInChosenCurrency,
                currentList.get(currencyID).currency));

    }

    private void parseJsonAvailableCard() {
        arrayListsCards = new ArrayList<ArrayList<AvailableCard>>();
        try {
            JSONArray array = new JSONArray(jsonCardAvailable);

            for ( int i = 0; i < array.length(); i++ ) {
                ArrayList<AvailableCard> availableCards = new ArrayList<AvailableCard>();

                JSONArray jsonArray = array.getJSONObject(i).getJSONArray("options");

                String name = array.getJSONObject(i).getString("name");

                for (int j = 0; j < jsonArray.length(); j++ ) {
                    AvailableCard availableCard = new AvailableCard();

                    JSONObject taxfreeJson1 = jsonArray.getJSONObject(j);

                    availableCard.name                      = name;
                    availableCard.id                        = taxfreeJson1.getString("id");
                    availableCard.currency                  = taxfreeJson1.getString("currency");
                    availableCard.orderPriceInChosenCurrency= taxfreeJson1.getString("orderPriceInChosenCurrency");
                    availableCard.cardFrontImageUrl         = taxfreeJson1.getString("cardFrontImageUrl");
                    availableCard.cardBackImageUrl          = taxfreeJson1.getString("cardBackImageUrl");
                    availableCard.rotatedViewUrl            = taxfreeJson1.getString("rotatedViewUrl");
                    availableCard.tariffUrl                 = taxfreeJson1.getString("tariffUrl");

                    availableCards.add(availableCard);
//                    Log.d(TAG, "1444  availableCard.currency: " + availableCards.get(j).currency);
                }
                arrayListsCards.add(availableCards);
            }
//            Log.d(TAG, "1444 availableCard.id " + arrayListsCards.get(1).get(0).id + " currency: "
//                    + arrayListsCards.get(1).get(0).currency
//                    + "  name: " + arrayListsCards.get(1).get(0).name );
//
//            Log.d(TAG, "1444 availableCard.id " + arrayListsCards.get(1).get(1).id + " currency: "
//                    + arrayListsCards.get(1).get(1).currency
//                    + "  name: " + arrayListsCards.get(1).get(1).name );

//            JSONArray taxfreeJson = array.getJSONObject(0).getJSONArray("options");
//            JSONArray studentsJson = array.getJSONObject(1).getJSONArray("options");
//
//
//            for (int i = 0; i < taxfreeJson.length(); i++ ) {
//                AvailableCard availableCard = new AvailableCard();
//                JSONObject taxfreeJson1 = taxfreeJson.getJSONObject(i);
//
//                availableCard.name = array.getJSONObject(0).getString("name");
//
//                availableCard.id                        = taxfreeJson1.getString("id");
//                availableCard.currency                  = taxfreeJson1.getString("currency");
//                availableCard.orderPriceInChosenCurrency= taxfreeJson1.getString("orderPriceInChosenCurrency");
//                availableCard.cardFrontImageUrl         = taxfreeJson1.getString("cardFrontImageUrl");
//                availableCard.cardBackImageUrl          = taxfreeJson1.getString("cardBackImageUrl");
//                availableCard.rotatedViewUrl            = taxfreeJson1.getString("rotatedViewUrl");
//                availableCard.tariffUrl                 = taxfreeJson1.getString("tariffUrl");
//
////                Log.d(TAG, "1444 availableCard.name " + availableCard.name + " currncy: " + availableCard.currency);
//                availableCardsTaxFree.add(availableCard);
//
//            }
//
//            for (int i = 0; i < studentsJson.length(); i++ ) {
//                AvailableCard availableCard = new AvailableCard();
//                JSONObject studentsJson1 = studentsJson.getJSONObject(i);
//
//                availableCard.name = array.getJSONObject(1).getString("name");
//
//                availableCard.id                        = studentsJson1.getString("id");
//                availableCard.currency                  = studentsJson1.getString("currency");
//                availableCard.orderPriceInChosenCurrency= studentsJson1.getString("orderPriceInChosenCurrency");
//                availableCard.cardFrontImageUrl         = studentsJson1.getString("cardFrontImageUrl");
//                availableCard.cardBackImageUrl          = studentsJson1.getString("cardBackImageUrl");
//                availableCard.rotatedViewUrl            = studentsJson1.getString("rotatedViewUrl");
//                availableCard.tariffUrl                 = studentsJson1.getString("tariffUrl");
//
////                Log.d(TAG, "1444 availableCard.name " + availableCard.name + " currncy: " + availableCard.currency);
//                availableCardsStud.add(availableCard);
//
//            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initialize() {

        pagerAdapter    = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        pager           = (ViewPager) findViewById(R.id.pager);
        btnOrderCard    = (Button) findViewById(R.id.btnOs);
        imgBack         = (ImageView) findViewById(R.id.img_back);

//        llEUR           = (LinearLayout) findViewById(R.id.llEur);
//        llGbp           = (LinearLayout) findViewById(R.id.llGbp);
//        lineEur         = (LinearLayout) findViewById(R.id.lineEur);
//        lineGbp         = (LinearLayout) findViewById(R.id.lineGbp);
        currencyLL      = (LinearLayout) findViewById(R.id.currency_ll);

//        txtEur          = (TextView) findViewById(R.id.txtEur);
//        txtGbp          = (TextView) findViewById(R.id.txtGbp);
        txtPrice        = (TextView) findViewById(R.id.txtPrice);
        txtCharges      = (TextView) findViewById(R.id.txtCharges);
        txtAdv          = (TextView) findViewById(R.id.txtAdv);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new FrCardTypeTaxFree(arrayListsCards.get(position), currencyID, position);
        }

        @Override
        public int getCount() {
            return arrayListsCards.size();
        }


        public float getPageWidth(int position) {
            return 0.88f;
        }

    }

    @Override
    protected void callOnSignUpCardId(String status) {
        if (status.equals(ConstStatus.ST_TRUE)) {
            Intent intent = new Intent(ConstIntents.IN_ActNewCardResult);

            utilPrefCustom.setCardSuccesLink(currentCards.get(currencyID).rotatedViewUrl);

            intent.putExtra(ConstEX.EX_CARD_NAME, currentCards.get(currencyID).name);

            intent.putExtra(ConstEX.EX_CARD_TYPE, currentCards.get(currencyID).id);

            intent.putExtra(ConstEX.EX_CARD_CURRENCY, currentCards.get(currencyID).currency);

            intent.putExtra(ConstEX.EX_CARD_AMMOUNT, currentCards.get(currencyID).orderPriceInChosenCurrency);

            startActivity(intent);

//            finish();
        }
    }

    public void sendRequest(AvailableCard availableCard) {
        utilStartService.startMyIntentService(new ReqMain.ReqSignUpCardId(util.id(ActNewCardTypeSelected.this),
                utilPrefCustom.getSessionToken(),
                availableCard.id));
    }

    @Override
    protected void callOnGetAvailableCardList(){
        jsonCardAvailable = utilPrefCustom.getCardAvailable();

        parseJsonAvailableCard();

        currentCards = arrayListsCards.get(0);

        setUpCurrency(currentCards);
    }

}
