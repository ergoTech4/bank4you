package uk.bank4you.client.activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.helpshift.support.Support;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import uk.bank4you.client.R;
import uk.bank4you.client.adapters.AdapterListTransactions;
import uk.bank4you.client.constants.ConstEX;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.constants.ConstLM;
import uk.bank4you.client.cursor_loaders.CursorLoaderTransactions;
import uk.bank4you.client.fragments.FrCabinetCard;
import uk.bank4you.client.fragments.FrFavorites;
import uk.bank4you.client.response.ReqMain;
import uk.bank4you.client.utils.UtilDbGet;

public class ActPay extends BasicActivity {
    private static final String TAG = "ActPay";

    private ViewPager               pager;
    private PagerAdapter            pagerAdapter;
//    private CirclePageIndicator     titleIndicator;
    private UtilDbGet               utilDbGet;
//    private ImageView               btnChat;
//    private SwipyRefreshLayout      mSwipeRefreshLayout;
//    private ArrayList<String>       cards = new ArrayList<String>();
    private LinearLayout            cardsLL, payLL, settingsLL, supportLL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_pay);

        initialize();

        initializeMenu();

        pager.setAdapter(pagerAdapter);
//        titleIndicator.setViewPager(pager);

//        adapterListTransactions = new AdapterListTransactions(this);
//        lvTransactions.setAdapter(adapterListTransactions);
//        getSupportLoaderManager().restartLoader(ConstLM.LM_TRANSACTIONS, null, this);

//        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
//
//            @Override
//            public void onPageSelected(int position) {
//                String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());
//
//                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//                String date = df.format(Calendar.getInstance().getTime());
//
//                if (cards.contains(cardId) && utilDbGet.getNextDate(0, cardId) != null) {
//                    utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionListBefore(
//                            utilPrefCustom.getToken(), cardId,  utilDbGet.getNextDate(0, cardId), date));
//                    Log.d(TAG, "1444 if getNextDate: " + utilDbGet.getNextDate(0, cardId) + "   present date: " + date);
//
//                } else {
//                    cards.add(cardId);
//                    utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionListBefore(
//                            utilPrefCustom.getToken(), cardId, "20", date));
//                    Log.d(TAG, "1444 else getNextDate: " + utilDbGet.getNextDate(0, cardId) + "   present date: " + date);
//
//                }
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {}
//        });
//
//        lvTransactions.setOnItemClickListener(new ListView.OnItemClickListener(){
//            @Override
//            public void onItemClick(AdapterView<?> a, View view, int pos, long l) {
//                Cursor cur = adapterListTransactions.getCursor();
//                cur.moveToPosition(pos);
//                Intent intent = new Intent(ConstIntents.IN_ActTransaction);
//                intent.putExtra(ConstEX.EX_TRANSACTION_ID, cur.getInt(cur.getColumnIndex("id")));
//                intent.putExtra(ConstEX.EX_CARD_NUMBER, utilDbGet.getCardDataByOrderNumber(pager.getCurrentItem()).cardNo);
//                startActivity(intent);
//            }
//        });
//
//        btnChat.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Support.showConversation(ActPay.this);
//            }
//        });
//

 //        lvTransactions.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
//            @Override
//            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
//                // Do work to refresh the list here.
//
//                String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());
//
//                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//                String date = df.format(Calendar.getInstance().getTime());
//
//                utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionList(
//                        utilPrefCustom.getToken(), cardId,  utilDbGet.getNextDate(0, cardId), date));
//
//            }
//        });



//        lvTransactions.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
//            @Override
//            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
//
//                String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());
//
//                utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionListBefore(
//                        utilPrefCustom.getToken(), cardId, "20",
//                        utilDbGet.getPrevDate(adapterListTransactions.getCount(), cardId)));
//
//            }
//
//            @Override
//            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
//                String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());
//
//                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//                String date = df.format(Calendar.getInstance().getTime());
//
//                utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionList(
//                        utilPrefCustom.getToken(), cardId,  utilDbGet.getNextDate(0, cardId), date));
//            }
//        });
//
//        @Override
//        public void onRefresh() {
//            String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());
////
//            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//            String date = df.format(Calendar.getInstance().getTime());
//
//            utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionList(
//                    utilPrefCustom.getToken(), cardId,  utilDbGet.getNextDate(0, cardId), date));
//
//        }


//        mSwipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh(SwipyRefreshLayoutDirection direction) {
//                if ( direction == SwipyRefreshLayoutDirection.TOP ) {
//                    String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());
//
//                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//                    String date = df.format(Calendar.getInstance().getTime());
//
//                    utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionList(
//                        utilPrefCustom.getToken(), cardId,  utilDbGet.getNextDate(0, cardId), date));
//
//                } else if ( direction == SwipyRefreshLayoutDirection.BOTTOM ){
//                    String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());
//
//                    utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionListBefore(
//                            utilPrefCustom.getToken(), cardId, "20",
//                            utilDbGet.getPrevDate(adapterListTransactions.getCount(), cardId)));
//                }
//            }
//        });


        Log.d(TAG, "1444 fav tranzactions: " + utilDbGet.getFavTransactionCount());

    }

    private void initializeMenu() {

        cardsLL         = (LinearLayout) findViewById(R.id.act_cabinet_menu_cards);
        payLL           = (LinearLayout) findViewById(R.id.act_cabinet_menu_pay);
        settingsLL      = (LinearLayout) findViewById(R.id.act_cabinet_menu_settings);
        supportLL       = (LinearLayout) findViewById(R.id.act_cabinet_menu_support);

        cardsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !cardsLL.isSelected() ) {
                    cardsLL.setSelected(true);
                    payLL.setSelected(false);
                    settingsLL.setSelected(false);
                    supportLL.setSelected(false);

                    startActivity(new Intent(ConstIntents.IN_ActCabinet).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                }
            }
        });

        payLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !payLL.isSelected() ) {
                    payLL.setSelected(true);
                    cardsLL.setSelected(false);
                    settingsLL.setSelected(false);
                    supportLL.setSelected(false);
                }
            }
        });

        settingsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !settingsLL.isSelected() ) {
                    settingsLL.setSelected(true);
                    payLL.setSelected(false);
                    cardsLL.setSelected(false);
                    supportLL.setSelected(false);

                    startActivity(new Intent(ConstIntents.IN_ActSettings).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                }
            }
        });

        supportLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !supportLL.isSelected() ) {
                    supportLL.setSelected(true);
                    payLL.setSelected(false);
                    settingsLL.setSelected(false);
                    cardsLL.setSelected(false);

                    startActivity(new Intent(ConstIntents.IN_ActSupport).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                }
            }
        });
    }

    private void initialize() {
        utilDbGet       = new UtilDbGet(this);
        pagerAdapter    = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        pager           = (ViewPager) findViewById(R.id.pager);
//        titleIndicator  = (CirclePageIndicator)findViewById(R.id.titles);

//        lvTransactions  = (ListView) findViewById(R.id.list_transactions);
//        btnChat         = (ImageView) findViewById(R.id.btnChat);

//        mSwipeRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipeContainer);

    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new FrFavorites().newInstance(position);
        }

        @Override
        public int getCount() {
            return utilDbGet.getFavTransactionCount();
        }

        public float getPageWidth(int position) {
            return 0.38f;
        }

//        @Override
//        public float getPageWidth(int position) {
//            return 0.88f;
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        payLL.setSelected(true);
        supportLL.setSelected(false);
        cardsLL.setSelected(false);
        settingsLL.setSelected(false);
    }
}
