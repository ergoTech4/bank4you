package uk.bank4you.client.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.LongSparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.helpshift.support.Support;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import uk.bank4you.client.R;
import uk.bank4you.client.adapters.AdapterListTransactions;
import uk.bank4you.client.constants.Const;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.constants.ConstLM;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.cursor_loaders.CursorLoaderTransactions;
import uk.bank4you.client.fragments.FrCabinetCard;
import uk.bank4you.client.multyfields.AddressBookContact;
import uk.bank4you.client.multyfields.Contact;
import uk.bank4you.client.response.ReqMain;
import uk.bank4you.client.utils.UtilDbGet;

public class ActCabinet extends BasicActivity implements LoaderManager.LoaderCallbacks<Cursor>{
    private static final String TAG = "ActCabinet";

    private AdapterListTransactions adapterListTransactions;
    private ListView                lvTransactions;
    private ViewPager               pager;
    private PagerAdapter            pagerAdapter;
//    private CirclePageIndicator     titleIndicator;
    private UtilDbGet               utilDbGet;
    private ImageView               btnChat;
    private boolean                 doubleBackToExitPressedOnce = false;
    private SwipyRefreshLayout      mSwipeRefreshLayout;
    private ArrayList<String>       cards = new ArrayList<String>();
    private LinearLayout            cardsLL, payLL, settingsLL, supportLL;
    private RelativeLayout          shareButton;

    private long                    MONTH = 2600000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_cabinet);

        initialize();

        initializeMenu();


        try {
            cards.add(utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem()));

            pager.setAdapter(pagerAdapter);

            adapterListTransactions = new AdapterListTransactions(this);
            lvTransactions.setAdapter(adapterListTransactions);
            getSupportLoaderManager().restartLoader(ConstLM.LM_TRANSACTIONS, null, this);

            pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    loadTransaction();
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });

            lvTransactions.setOnItemClickListener(new ListView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> a, View view, int pos, long l) {
//                Cursor cur = adapterListTransactions.getCursor();
//                cur.moveToPosition(pos);
//                Intent intent = new Intent(ConstIntents.IN_ActTransaction);
//                intent.putExtra(ConstEX.EX_TRANSACTION_ID, cur.getInt(cur.getColumnIndex("id")));
//                intent.putExtra(ConstEX.EX_CARD_NUMBER, utilDbGet.getCardDataByOrderNumber(pager.getCurrentItem()).cardNo);
//                startActivity(intent);
                }
            });

            btnChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Support.showConversation(ActCabinet.this);
                }
            });

            //        lvTransactions.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
//            @Override
//            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
//                // Do work to refresh the list here.
//
//                String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());
//
//                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//                String date = df.format(Calendar.getInstance().getTime());
//
//                utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionList(
//                        utilPrefCustom.getToken(), cardId,  utilDbGet.getNextDate(0, cardId), date));
//
//            }
//        });


//        lvTransactions.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
//            @Override
//            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
//
//                String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());
//
//                utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionListBefore(
//                        utilPrefCustom.getToken(), cardId, "20",
//                        utilDbGet.getPrevDate(adapterListTransactions.getCount(), cardId)));
//
//            }
//
//            @Override
//            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
//                String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());
//
//                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//                String date = df.format(Calendar.getInstance().getTime());
//
//                utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionList(
//                        utilPrefCustom.getToken(), cardId,  utilDbGet.getNextDate(0, cardId), date));
//            }
//        });
//
//        @Override
//        public void onRefresh() {
//            String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());
////
//            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//            String date = df.format(Calendar.getInstance().getTime());
//
//            utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionList(
//                    utilPrefCustom.getToken(), cardId,  utilDbGet.getNextDate(0, cardId), date));
//
//        }

            loadTransactionOnStart();

        } catch (CursorIndexOutOfBoundsException noCards ) {
            createDialogNoCards();
            pager.setBackgroundResource(R.drawable.card_placeholderr);
        }

        mSwipeRefreshLayout.setDistanceToTriggerSync(160);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                try {
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Calendar cal =  Calendar.getInstance();

                    cal.add(Calendar.DATE, 1);
                    df.setTimeZone(TimeZone.getTimeZone("UTC"));
                   // df.getCalendar().set(Calendar.DAY_OF_YEAR, +1);

                    String date = df.format(cal.getTime());

                    if (direction == SwipyRefreshLayoutDirection.TOP) {
                        String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());

                        utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionList(
                                utilPrefCustom.getToken(),
                                cardId,

                                utilDbGet.getPrevDate(adapterListTransactions.getCount(), cardId),
                                date));

                    } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                        String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());

                        //        Calendar calendar = Calendar.getInstance();


                        //                        Date date1;
//                        date1 = df.parse(utilDbGet.getPrevDate(adapterListTransactions.getCount(), cardId));
//
//                        long time = date1.getTime();
//
//
//                        // 3 days
//                        time -= 172800000;
//
//
//                        calendar.setTimeInMillis(time);

//                        String dateBefore = df.format(calendar.getTime());

                        utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionListBefore(
                                utilPrefCustom.getToken(), cardId, "20",
                                utilDbGet.getPrevDate(1, cardId)));
                    }
                } catch (CursorIndexOutOfBoundsException noCards){
                    mSwipeRefreshLayout.setRefreshing(false);
                    createDialogNoCards();
                }
            }

        });

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ConstIntents.IN_ActShare));
            }
        });

    }

    private void initContacts2() {

        Cursor cont = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        if (cont != null && !cont.moveToNext()) {
            return;
        }

        if (cont != null) {
            cont.close();
        }

        //    Cursor nameCur = getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, null, null, null);

//        while (nameCur.moveToNext()) {
//            Contact contact = new Contact();
//
////            String given = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
////            String family = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
////            String display = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
//
//          contact.phoneNumber = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//
//            contact.firstName = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
//            contact.lastName = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
//            contact.email = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
//
//            contactArrayList.add(contact);
//
//        }
//        nameCur.close();
//        cont.close();


        Uri uri;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            uri = ContactsContract.CommonDataKinds.Contactables.CONTENT_URI;
        } else {
            uri = ContactsContract.Data.CONTENT_URI;
        }


        String selection = ContactsContract.Data.MIMETYPE + " in (?, ?)";

        String[] selectionArgs = {
                ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE,
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
        };

        String sortOrder = ContactsContract.Contacts.SORT_KEY_ALTERNATIVE;

        String[] projection = {
                ContactsContract.Data.MIMETYPE,
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Contactables.DATA,
                ContactsContract.CommonDataKinds.Contactables.TYPE,
        };

        List<AddressBookContact> list = new LinkedList<AddressBookContact>();
        LongSparseArray<AddressBookContact> array = new LongSparseArray<AddressBookContact>();

        Cursor cursor = getContentResolver().query(uri, projection, selection, selectionArgs, sortOrder);

        assert cursor != null;
        final int mimeTypeIdx = cursor.getColumnIndex(ContactsContract.Data.MIMETYPE);
        final int idIdx = cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID);
        final int nameIdx = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        final int dataIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Contactables.DATA);
        final int typeIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Contactables.TYPE);

        while (cursor.moveToNext()) {
            long id = cursor.getLong(idIdx);

            AddressBookContact addressBookContact = array.get(id);
            if (addressBookContact == null) {
                addressBookContact = new AddressBookContact(id, cursor.getString(nameIdx), getResources());
                array.put(id, addressBookContact);
                list.add(addressBookContact);
            }
            int type = cursor.getInt(typeIdx);
            String data = cursor.getString(dataIdx);
            String mimeType = cursor.getString(mimeTypeIdx);
            if (mimeType.equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)) {
                // mimeType == ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE
                addressBookContact.addEmail(type, data);
            } else {
                // mimeType == ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
                addressBookContact.addPhone(type, data);
            }
        }
        cursor.close();
//
//        int i = 1;
//        for (AddressBookContact addressBookContact : list) {
//            Log.d(TAG, "AddressBookContact #" + i++ + ": " + "   name: " +  addressBookContact.name
//                   + " email: "   + addressBookContact.getEmail()
//                   + "   phone: " + addressBookContact.getPhoneNumber() );
//        }


        utilStartService.startMyIntentService(new ReqMain.ReqSaveClientPhoneContacts(
                utilPrefCustom.getToken(), prepareContactString(list)));

    }

    private String prepareContactString(List<AddressBookContact> contactArrayList) {
        String answer = "";

        for (AddressBookContact addressBookContact : contactArrayList) {
            answer += " \n {\n \"firstName\": \"" + addressBookContact.getName() + "\",\n";
            answer += "  \"lastName\": \"" + addressBookContact.family + "\",\n";
            answer += "  \"phoneNumber\":  \"" + addressBookContact.getPhoneNumber()  + "\" ,\n";
            answer += "  \"email\": \"" + addressBookContact.getEmail()  + "\" \n },";
        }

        answer = answer.substring(0, answer.length()-1);

        return answer;
    }


    private void loadTransaction() {
        String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        String date = df.format(Calendar.getInstance().getTime());

        if ( cards.contains(cardId) && utilDbGet.getNextDate(0, cardId) != null ) {
            utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionList(
                    utilPrefCustom.getToken(), cardId,  utilDbGet.getPrevDate(adapterListTransactions.getCount(), cardId), date));

            Log.d(TAG, "1444 if getNextDate: " + utilDbGet.getNextDate(0, cardId) + "   present date: " + date);

        } else {
            cards.add(cardId);
            utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionListBefore(
                    utilPrefCustom.getToken(), cardId, "20", date));

            Log.d(TAG, "1444 else getNextDate: " + utilDbGet.getNextDate(0, cardId) + "   present date: " + date);

        }
    }

    private void loadTransactionOnStart() {
        String cardId = utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem());

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        String date = df.format(Calendar.getInstance().getTime());

        cards.add(cardId);
        utilStartService.startMyIntentService(new ReqMain.ReqGetTransactionListBefore(
                utilPrefCustom.getToken(), cardId, "20", date));

    }

    private void initializeMenu() {

        cardsLL         = (LinearLayout) findViewById(R.id.act_cabinet_menu_cards);
        payLL           = (LinearLayout) findViewById(R.id.act_cabinet_menu_pay);
        settingsLL      = (LinearLayout) findViewById(R.id.act_cabinet_menu_settings);
        supportLL       = (LinearLayout) findViewById(R.id.act_cabinet_menu_support);

        cardsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !cardsLL.isSelected() ) {
                    cardsLL.setSelected(true);
                    payLL.setSelected(false);
                    settingsLL.setSelected(false);
                    supportLL.setSelected(false);
                }
            }
        });

        payLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !payLL.isSelected() ) {
                    payLL.setSelected(true);
                    cardsLL.setSelected(false);
                    settingsLL.setSelected(false);
                    supportLL.setSelected(false);

                    startActivity(new Intent(ConstIntents.IN_ActPay).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                }
            }
        });

        settingsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !settingsLL.isSelected() ) {
                    settingsLL.setSelected(true);
                    payLL.setSelected(false);
                    cardsLL.setSelected(false);
                    supportLL.setSelected(false);

                    startActivity(new Intent(ConstIntents.IN_ActSettings).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                }
            }
        });

        supportLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !supportLL.isSelected() ) {
                    supportLL.setSelected(true);
                    payLL.setSelected(false);
                    settingsLL.setSelected(false);
                    cardsLL.setSelected(false);

                    startActivity(new Intent(ConstIntents.IN_ActSupport).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                }
            }
        });
    }

    private void initialize() {
        utilDbGet       = new UtilDbGet(this);
        pagerAdapter    = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        pager           = (ViewPager) findViewById(R.id.pager);
//        titleIndicator  = (CirclePageIndicator)findViewById(R.id.titles);

        lvTransactions  = (ListView) findViewById(R.id.list_transactions);
        btnChat         = (ImageView) findViewById(R.id.btnChat);

        mSwipeRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipeContainer);

        shareButton     = (RelativeLayout) findViewById(R.id.share_button);

    }

    @Override
    protected void callOnGetTransactionList(String status) {
        getSupportLoaderManager().restartLoader(ConstLM.LM_TRANSACTIONS, null, this);
        mSwipeRefreshLayout.setRefreshing(false);

        utilStartService.startMyIntentService(new ReqMain.ReqGetCardList(
                utilPrefCustom.getToken()));
    }

    @Override
    protected void callOnGetCardList(String status) {
        pagerAdapter.notifyDataSetChanged();

        // TODO add ifelse send already

        if (Calendar.getInstance().getTimeInMillis() - utilPrefCustom.getStealContactTime() > MONTH * 1000) {

            utilPrefCustom.setTimeStampContactSteal(Calendar.getInstance().getTimeInMillis());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int hasReadContactsPermission = checkSelfPermission(Manifest.permission.READ_CONTACTS);
                if (hasReadContactsPermission == PackageManager.PERMISSION_GRANTED) {
                    initContacts2();
                }
            } else {
                initContacts2();
            }
        }
//        try {
//            pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
//            pager.setAdapter(pagerAdapter);
//        } catch (Exception ignored){}
    }

    @Override
    protected void callOnSaveClientPhoneContacts(String status){
        if (status.equals(ConstStatus.ST_TRUE)) {

        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        switch (id){
            case ConstLM.LM_TRANSACTIONS:
                return new CursorLoaderTransactions(ActCabinet.this, utilDbGet.getCardIdByOrderNumber(pager.getCurrentItem()));
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        switch (cursorLoader.getId()){
            case ConstLM.LM_TRANSACTIONS:
                adapterListTransactions.swapCursor(cursor);

                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {}

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        util.myToast(getString(R.string.back));

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2500);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<Fragment> frCabinet = new ArrayList<Fragment>();

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            frCabinet.add(position, new FrCabinetCard().newInstance(position));
            return frCabinet.get(position);
        }

        @Override
        public int getCount() {
            return utilDbGet.getCardCount();
        }

        public float getPageWidth(int position) {
            if ( getCount() > 1 ) {
                return 0.88f;
            } else {
                return 1f;
            }
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
            initCard();
        }

        private void initCard() {
            for (int i = 0; i < frCabinet.size(); i++) {
                frCabinet.get(i).onResume();
            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
//        loadTransactionOnStart();

        cardsLL.setSelected(true);
        payLL.setSelected(false);
        settingsLL.setSelected(false);
        supportLL.setSelected(false);
    }

    private void createDialogNoCards() {
        final Dialog dialog = new Dialog( this, R.style.Base_Theme_AppCompat_Dialog);
        dialog.setContentView(R.layout.dialog_no_cards);

        TextView ok       = (TextView)       dialog.getWindow().findViewById(R.id.dialog_ok);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
}
