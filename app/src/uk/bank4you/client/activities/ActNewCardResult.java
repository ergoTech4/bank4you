package uk.bank4you.client.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.Currency;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.xmlpull.v1.XmlPullParser;

import java.math.BigDecimal;
import java.util.Set;

import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstEX;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.response.ReqMain;

public class ActNewCardResult extends BasicActivity {

    private static final String TAG = "ActNewCardResult";

    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID    = "AUa-2rGdVAF1dSSDt_arhOmc6ROYQRD6V8LzE7WNfHOWsqmKoxKXqO3yY_JWvEWQw9eZ-OdpyMiRqGAv";
    private static final String CONFIG_ENVIRONMENT  = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    private static final int REQUEST_CODE_PAYMENT   = 1;

    private TextView    btnSignIn, cardType, cardCurrency;
//    private ImageView   imgCard;

    private RelativeLayout btnPayPal, btnMoneyPolo;

    private String      cardAmount;

    private String      currency, tempCurrency;
    private ImageView   back_button;
//    private Spinner     spinner;



    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_new_card_result1);
        initialize();
        
        String name         = getIntent().getStringExtra(ConstEX.EX_CARD_NAME);
        final String cardId = getIntent().getStringExtra(ConstEX.EX_CARD_TYPE);
        cardAmount          = getIntent().getStringExtra(ConstEX.EX_CARD_AMMOUNT);
        currency            = getIntent().getStringExtra(ConstEX.EX_CARD_CURRENCY);

        tempCurrency    = "";
        if ( currency.equals("EUR") ) {
            tempCurrency = "€";
        } else if (currency.equals("GBP")){
            tempCurrency = "£";
        } else if (currency.equals("USD")){
            tempCurrency = "$";
        }

        cardCurrency.setText(String.format("%s %s", tempCurrency, cardAmount));

        cardType.setText(String.format("%s (%s) %s", name, currency, getString(R.string.card_fee)));

        // запустим сервис работающий в фоне
        Intent intent = new Intent(ActNewCardResult.this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentHome = new Intent(ConstIntents.IN_ActSignIn);
                intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentHome);
            }
        });

        btnPayPal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBuyPayPalPressed();
                utilPrefCustom.setCardID(cardId);
            }
        });

        btnMoneyPolo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utilStartService.startMyIntentService(
                        new ReqMain.ReqMoneyPoloLink(utilPrefCustom.getSessionToken(), cardId));
                utilPrefCustom.setCardID(cardId);
            }
        });

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        final String[] values = new String[] { getString(R.string.pay_from_card), getString(R.string.paypal_peyment)};
//
//        SpinnerAdapter adapter = new SpinnerAdapter(this, R.layout.spinner_item, values);
//
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        spinner.setAdapter(adapter);
//
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                btnPay.setText(values[position]);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
    }

    private void initialize() {
        btnSignIn       = (TextView) findViewById(R.id.btnSignIn);
        cardType        = (TextView) findViewById(R.id.act_new_card_result_card_type);
        cardCurrency    = (TextView) findViewById(R.id.act_new_card_result_card_currency);

        back_button     = (ImageView) findViewById(R.id.img_back);
        btnPayPal       = (RelativeLayout) findViewById(R.id.paypal_rl);
        btnMoneyPolo    = (RelativeLayout) findViewById(R.id.moneypolo_rl);

//        spinner         = (Spinner)findViewById(R.id.spinner);

    }

    public void onBuyPayPalPressed() {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(ActNewCardResult.this, PaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }


    private PayPalPayment getThingToBuy(String paymentIntent) {
        PayPalPayment payment = new PayPalPayment(new BigDecimal(cardAmount), currency, "Order card", paymentIntent);
        payment.custom(utilPrefCustom.getPhoneNumber());
        return payment;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
//                        Intent intentHome = new Intent(ConstIntents.IN_ActSignIn);
//                        intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(intentHome);
                        Intent intent = new Intent(ActNewCardResult.this, ActSuccessPay.class);

                        intent.putExtra(ConstStatus.ST_SUCCESS, true);
                        startActivity(intent);


                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    protected void callOnStartedWeb(String text) {
        startActivity(new Intent(ConstIntents.IN_ActMoneyWebView));
    }

//    private class SpinnerAdapter extends ArrayAdapter<String > {
//
//        private Context             context;
//
//        public SpinnerAdapter(Context context, int spinner_item, String[] values){
//            super(context, spinner_item, values);
//            this.context    = context;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            return getCustomView(position, convertView, parent);
//        }
//
//        @Override
//        public View getDropDownView(int position, View convertView,ViewGroup parent) {
//            return getCustomView(position, convertView, parent);
//        }
//
//        private View getCustomView(int position, View convertView, ViewGroup parent) {
//            final Holder    holder;
//
//            LayoutInflater inflater = (LayoutInflater) context
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//
//            if ( convertView == null ) {
//                convertView = inflater.inflate(R.layout.spinner_item, null);
//
//                holder          = new Holder();
//
//                holder.name     = (TextView) convertView.findViewById(R.id.spinner_text_view);
//                holder.image    = (ImageView) convertView.findViewById(R.id.spinner_image_view);
//
//                convertView.setTag(holder);
//            } else {
//
//                holder  = (Holder) convertView.getTag();
//            }
//
//            if ( position == 0 ) {
//                holder.name.setText(getString(R.string.pay_from_card));
//                holder.image.setImageResource(R.drawable.visa);
//            } else {
//                holder.name.setText(getString(R.string.paypal_peyment));
//                holder.image.setImageResource(R.drawable.paypal);
//            }
//            return convertView;
//        }
//
//
//        private class Holder {
//            TextView    name;
//            ImageView   image;
//        }
//
//    }
}
