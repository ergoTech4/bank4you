package uk.bank4you.client.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstEX;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.constants.ConstStatus;

public class ActivityWebView extends BasicActivity {

    private static final String TAG = "ActivityWebView";

    private WebView         wv;
    private TextView        agree, cancel;
    private RelativeLayout  rl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_webview);

        String url = getIntent().getExtras().getString(ConstEX.EX_URL);

        initialize();

      //  wv.setWebChromeClient(new WebChromeClient());
       // wv.setWebViewClient(new MyCallback());
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setBuiltInZoomControls(true);
        wv.getSettings().setDomStorageEnabled(true);

        wv.loadUrl(url);

        if ( getIntent().getExtras().getBoolean(ConstStatus.ST_TERMS, false)) {
            wv.setWebViewClient(new WebViewClient(){
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    dismissDialog();
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    showMyProgressDialog(getString(R.string.loading_page));
                }

                @Override
                public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error){
                    handler.proceed();
                }

            });

            rl.setVisibility(View.VISIBLE);

            agree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(ConstIntents.IN_ActSignUp));
                    ActivityWebView.this.finish();
                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        } else {
            wv.setWebViewClient(new WebViewClient(){
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    dismissDialog();
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    showMyProgressDialog(getString(R.string.loading_page));
                }

                @Override
                public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error){
                    handler.proceed();
                }

            });
        }
    }

    private void initialize() {
        wv      = (WebView) findViewById(R.id.webview);
        rl      = (RelativeLayout) findViewById(R.id.act_webview_rl);
        agree   = (TextView) findViewById(R.id.act_webview_tv_agree);
        cancel  = (TextView) findViewById(R.id.act_webview_tv_cancel);
    }

    private class MyCallback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return(false);
        }
    }
}
