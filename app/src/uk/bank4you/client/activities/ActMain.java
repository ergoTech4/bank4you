package uk.bank4you.client.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import com.viewpagerindicator.CirclePageIndicator;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.fragments.FrMainCardStudent;
import uk.bank4you.client.fragments.FrMainCardTaxFree;
import uk.bank4you.client.fragments.FrMainStart;

public class ActMain extends BasicActivity {

    private Button              btnStart;
    private ViewPager           pager;
    private PagerAdapter        pagerAdapter;
    private CirclePageIndicator titleIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);


        if ( utilPrefCustom.getToken().length() > 1 ) {
            Intent intent = new Intent(ConstIntents.IN_ActSignIn);
            startActivity(intent);
            finish();
        } else {
            initialize();

            pager.setAdapter(pagerAdapter);
//            titleIndicator.setStrokeColor(getResources().getColor(R.color.transparent));
            titleIndicator.setViewPager(pager);

            btnStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ConstIntents.IN_ActSignIn);
                    startActivity(intent);
                    finish();
                }
            });
        }



        try {
            pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if (position >= 1) {
                        titleIndicator.setFillColor(getResources().getColor(R.color.gray));
                    } else {
                        titleIndicator.setFillColor(getResources().getColor(R.color.white));
                    }
//                titleIndicator.setCurrentItem(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        } catch (NullPointerException ignore ) {}
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: return new FrMainStart();
                case 1: return new FrMainCardTaxFree();
                case 2: return new FrMainCardStudent();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    private void initialize() {
        pagerAdapter    = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        btnStart        = (Button) findViewById(R.id.btnStart);
        pager           = (ViewPager) findViewById(R.id.pager);
        titleIndicator  = (CirclePageIndicator)findViewById(R.id.titles);
    }
}
