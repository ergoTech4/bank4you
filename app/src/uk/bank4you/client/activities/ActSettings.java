package uk.bank4you.client.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import uk.bank4you.client.Demo.ActCabinetDemo;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstEX;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.response.ReqMain;

public class ActSettings extends BasicActivity {
    private static final String TAG = "ActSettings";


    private LinearLayout    cardsLL, payLL, settingsLL, supportLL;
    private RelativeLayout  changePassword, topRl;
    private Button          logout;
    private TextView        email, phoneNumber, userName, appVersion;

    private boolean         demo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_settings);

        initialize();

        initializeMenu();

        demo = getIntent().getBooleanExtra("demo", false);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            appVersion.setText(String.format("%s %s", getString(R.string.app_version), pInfo.versionName));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if ( demo ) {
            userName.setText(String.format("%s %s", "Richard", "Williams"));

            email.setText(String.format("%s %s", getString(R.string._email_), "R.Williams@bank4you.uk"));
            phoneNumber.setText(String.format("%s %s", getString(R.string._phone_number), "+440203637912"));
        } else {
            utilStartService.startMyIntentService(new ReqMain.ReqGetAccountDetails(utilPrefCustom.getToken()));
            phoneNumber.setText(String.format("%s %s", getString(R.string._phone_number), utilPrefCustom.getPhoneNumber()));
        }

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConstIntents.IN_ActSignIn);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);

                finish();
            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( demo ) {
                    util.myToast(getString(R.string.pls_login));
                } else {
                    Intent intent = new Intent(ConstIntents.IN_ActChangePass);

                    startActivity(intent);
                }
            }
        });


        topRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String link = "http://bank4you.uk/benefits-of-bank4you-card/";

                Intent intent = new Intent(ConstIntents.IN_ActWebView);
                intent.putExtra(ConstEX.EX_URL, link);
                startActivity(intent);
            }
        });

    }

    private void initializeMenu() {

        cardsLL         = (LinearLayout) findViewById(R.id.act_cabinet_menu_cards);
        payLL           = (LinearLayout) findViewById(R.id.act_cabinet_menu_pay);
        settingsLL      = (LinearLayout) findViewById(R.id.act_cabinet_menu_settings);
        supportLL       = (LinearLayout) findViewById(R.id.act_cabinet_menu_support);

        cardsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !cardsLL.isSelected() ) {
                    cardsLL.setSelected(true);
                    payLL.setSelected(false);
                    settingsLL.setSelected(false);
                    supportLL.setSelected(false);

                    if (demo) {
                        startActivity(new Intent(ActSettings.this, ActCabinetDemo.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    } else {
                        startActivity(new Intent(ConstIntents.IN_ActCabinet).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    }
                }
            }
        });

        payLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !payLL.isSelected() ) {
                    payLL.setSelected(true);
                    cardsLL.setSelected(false);
                    settingsLL.setSelected(false);
                    supportLL.setSelected(false);
//                    startActivity(new Intent(ConstIntents.IN_ActPay).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));


                }
            }
        });

        settingsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !settingsLL.isSelected() ) {
                    settingsLL.setSelected(true);
                    payLL.setSelected(false);
                    cardsLL.setSelected(false);
                    supportLL.setSelected(false);
                }
            }
        });

        supportLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !supportLL.isSelected() ) {
                    supportLL.setSelected(true);
                    payLL.setSelected(false);
                    settingsLL.setSelected(false);
                    cardsLL.setSelected(false);

                    Intent intent = new Intent(ConstIntents.IN_ActSupport).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("demo", demo);

                    startActivity(intent);
                }
            }
        });
    }

    private void initialize() {
        changePassword  = (RelativeLayout) findViewById(R.id.settings_change_password);
        topRl           = (RelativeLayout) findViewById(R.id.settings_top_rl);
        logout          = (Button) findViewById(R.id.settings_logout);

        email           = (TextView) findViewById(R.id.settings_email);
        phoneNumber     = (TextView) findViewById(R.id.settings_phone_number);
        userName        = (TextView) findViewById(R.id.settings_name);
        appVersion      = (TextView) findViewById(R.id.settings_app_version);

    }

    @Override
    protected void callGetAccountDetails(String text, String status) {
        try {
            JSONObject  answer  = new JSONObject(text);

            userName.setText(String.format("%s %s", answer.getString("firstName"), answer.getString("lastName")));

            if ( !answer.getString("email").equals("null")) {
                email.setText(String.format("%s %s", getString(R.string._email_), answer.getString("email")));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        settingsLL.setSelected(true);
        payLL.setSelected(false);
        supportLL.setSelected(false);
        cardsLL.setSelected(false);
    }
}
