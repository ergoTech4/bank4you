package uk.bank4you.client.activities;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.helpshift.support.Support;
import com.rengwuxian.materialedittext.MaterialEditText;

import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstBC;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.response.ReqMain;

public class ActChangePassword extends BasicActivity {


    private ImageView   imgBack;
    private ImageView   btnChat;
    private Button      change;

    private MaterialEditText oldPassword, newPassword, confirmPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_change_password);

        initialize();

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String oldPasswordText = oldPassword.getText().toString();
                String newPasswordText = newPassword.getText().toString();
                String confirmPasswordText = confirmPassword.getText().toString();

                if ( oldPasswordText.length() >= 4 && newPasswordText.length() >= 4 ) {
                    if ( newPasswordText.equals(confirmPasswordText) ) {
                        utilStartService.startMyIntentService(
                                new ReqMain.ReqChangePassword(utilPrefCustom.getToken(),
                                        newPasswordText, oldPasswordText));
                    } else {
                        util.myToast(getString(R.string.password_not_match));
                    }
                } else {
                    util.myToast(getString(R.string.password_too_short));
                }
            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Support.showConversation(ActChangePassword.this);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        (findViewById(R.id.mainLL)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });

        confirmPassword.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ( keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 1 ) {
                    String oldPasswordText = oldPassword.getText().toString();
                    String newPasswordText = newPassword.getText().toString();
                    String confirmPasswordText = confirmPassword.getText().toString();

                    if ( oldPasswordText.length() >= 4 && newPasswordText.length() >= 4 ) {
                        if ( newPasswordText.equals(confirmPasswordText) ) {
                            utilStartService.startMyIntentService(
                                    new ReqMain.ReqChangePassword(utilPrefCustom.getToken(),
                                            newPasswordText, oldPasswordText));
                        } else {
                            util.myToast(getString(R.string.password_not_match));
                        }
                    } else {
                        util.myToast(getString(R.string.password_too_short));
                    }

                    return true;
                }
                return false;
            }
        });

    }

    private void initialize() {
        imgBack         = (ImageView) findViewById(R.id.img_back);
        btnChat         = (ImageView) findViewById(R.id.btnChat);

        oldPassword     = (MaterialEditText) findViewById(R.id.change_password_old);
        newPassword     = (MaterialEditText) findViewById(R.id.change_password_new);
        confirmPassword = (MaterialEditText) findViewById(R.id.change_password_confirm);

        change          = (Button) findViewById(R.id.change_password_change);
    }

    @Override
    protected void callGetAccountDetails(String text, String status){
        if (status.equals(ConstStatus.ST_TRUE)) {
            util.myToast(getString(R.string.password_success_changed));
            finish();
        }
    }

}
