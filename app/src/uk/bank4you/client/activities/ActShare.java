package uk.bank4you.client.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.linkedin.platform.utils.Scope;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.response.ReqMain;

public class ActShare extends BasicActivity {

    private static final String TAG = "ActShare";

    private ImageView       backButton;

    private String          androidAppUrl, iosAppUrl, promoCode, inviteUrl;
    private String          telegramMsg, whatsAppMsg, viberMsg, emailMsg, smsMsg, facebookMsg, linkedInMsg ;

    private RelativeLayout  email, sms, viber, whatsapp, telegram, facebookButton, linkedin;

    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.act_share);

        initialize();
        clickers();

        utilStartService.startMyIntentService(new ReqMain.ReqGetSystemMessages(
                utilPrefCustom.getToken()));
    }


    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.W_SHARE);
    }

    private void clickers() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setType("message/rfc822");
                emailIntent.setData(new Uri.Builder().scheme("mailto").build());

                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Bank4you");
                if ( inviteUrl.length() > 5 ) {
                    emailIntent.putExtra(Intent.EXTRA_TEXT, emailMsg + "\n" + inviteUrl);
                } else {
                    emailIntent.putExtra(Intent.EXTRA_TEXT, emailMsg + "\n" + androidAppUrl);

                }
                emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(Intent.createChooser(emailIntent, "Send email..."));

            }
        });

        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"));

                if ( inviteUrl.length() > 5 ) {
                    intent.putExtra("sms_body", smsMsg + "\n" + inviteUrl);
                } else {
                    intent.putExtra("sms_body", smsMsg + "\n" + androidAppUrl);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        viber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final boolean isAppInstalled = isAppAvailable(ActShare.this.getApplicationContext(), "com.viber.voip");

                if (isAppInstalled) {
                    Intent myIntent = new Intent(Intent.ACTION_SEND);
                    myIntent.setType("text/plain");
                    myIntent.setPackage("com.viber.voip");
                    if ( inviteUrl.length() > 5 ) {
                        myIntent.putExtra(Intent.EXTRA_TEXT, viberMsg + "\n" + inviteUrl);
                    } else {
                        myIntent.putExtra(Intent.EXTRA_TEXT, viberMsg + "\n" + androidAppUrl);
                    }
                    startActivity(Intent.createChooser(myIntent, "Share with"));
                } else {
                    Toast.makeText(ActShare.this, "Viber not Installed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final boolean isAppInstalled = isAppAvailable(ActShare.this.getApplicationContext(), "com.whatsapp");

                if (isAppInstalled) {
                    Intent myIntent = new Intent(Intent.ACTION_SEND);
                    myIntent.setType("text/plain");
                    myIntent.setPackage("com.whatsapp");

                    if ( inviteUrl.length() > 5 ) {
                        myIntent.putExtra(Intent.EXTRA_TEXT, whatsAppMsg + "\n" + inviteUrl);
                    } else {
                        myIntent.putExtra(Intent.EXTRA_TEXT, whatsAppMsg + "\n" + androidAppUrl);
                    }

                    startActivity(Intent.createChooser(myIntent, "Share with"));
                } else {
                    Toast.makeText(ActShare.this, "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        telegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent waIntent = new Intent(Intent.ACTION_SEND);

                final boolean isAppInstalled = isAppAvailable(ActShare.this.getApplicationContext(), "org.telegram.messenger");

                if (isAppInstalled) {
                    Intent myIntent = new Intent(Intent.ACTION_SEND);
                    myIntent.setType("text/plain");
                    myIntent.setPackage("org.telegram.messenger");

                    if ( inviteUrl.length() > 5 ) {
                        myIntent.putExtra(Intent.EXTRA_TEXT, telegramMsg + "\n" + inviteUrl);
                    } else {
                        myIntent.putExtra(Intent.EXTRA_TEXT, telegramMsg + "\n" + androidAppUrl);
                    }

                    startActivity(Intent.createChooser(myIntent, "Share with"));
                } else {
                    Toast.makeText(ActShare.this, "Telegram not Installed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final boolean isAppInstalled = isAppAvailable(ActShare.this.getApplicationContext(), "com.facebook.katana");

                if (isAppInstalled) {

                    shareFB();

                } else {
                    Toast.makeText(ActShare.this, "Facebook not Installed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        linkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final boolean isAppInstalled = isAppAvailable(ActShare.this.getApplicationContext(), "com.linkedin.android");

                if (isAppInstalled) {
                    Intent myIntent = new Intent(Intent.ACTION_SEND);
                    myIntent.setType("text/plain");
//                    myIntent.setClassName("com.linkedin.android",
//                            "com.linkedin.android.home.UpdateStatusActivity");

                    myIntent.setPackage("com.linkedin.android");

                    if ( inviteUrl.length() > 5 ) {
                        myIntent.putExtra(Intent.EXTRA_TEXT, linkedInMsg + "\n" + inviteUrl);
                    } else {
                        myIntent.putExtra(Intent.EXTRA_TEXT, linkedInMsg + "\n" + androidAppUrl);
                    }
                    startActivity(Intent.createChooser(myIntent, "Share with"));
                } else {
                    Toast.makeText(ActShare.this, "LinkedIn not Installed", Toast.LENGTH_SHORT).show();
                }
            }
        });
//        LISessionManager.getInstance(getApplicationContext()).init(ActShare.this, buildScope(), new AuthListener() {
//            @Override
//            public void onAuthSuccess() {
//                // Authentication was successful.  You can now do
//                // other calls with the SDK.
//
//                String url = "https://api.linkedin.com/v1/people/~/shares";
//
//                String payload = "{" +
//                        "\"comment\":\"Check out developer.linkedin.com! " +
//                        "http://linkd.in/1FC2PyG\"," +
//                        "\"visibility\":{" +
//                        "    \"code\":\"anyone\"}" +
//                        "}";
//
//                APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
//
//                APIHelper.postRequest(ActShare.this, url, payload, new ApiListener() {
//                    @Override
//                    public void onApiSuccess(ApiResponse apiResponse) {
//                        // Success!
//
//                    }
//
//                    @Override
//                    public void onApiError(LIApiError liApiError) {
//                        // Error making POST request!
//                    }
//                });
//            }
//
//            @Override
//            public void onAuthError(LIAuthError error) {
//                // Handle authentication errors
//            }
//        }, true);

    }

    private void shareFB() {
        String url;

        if ( inviteUrl.length() > 5 ) {
            url = inviteUrl;
        } else {
            url = androidAppUrl;
        }

        ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
                .setContentTitle("Bank4you")
                .setContentDescription(facebookMsg)
                .setContentUrl(Uri.parse(url))
                .build();

        ShareDialog shareDialog = new ShareDialog(this);

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            shareDialog.show(shareLinkContent);
        } else {
            Log.d(TAG, "OOLOLOLOLOLOOLOO");
        }

//        ShareDialog.show(this, shareLinkContent);
    }


    public static boolean isAppAvailable(Context context, String appName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void initialize() {
        backButton     = (ImageView) findViewById(R.id.btn_preview);

        email           = (RelativeLayout) findViewById(R.id.share_email);
        sms             = (RelativeLayout) findViewById(R.id.share_sms);
        viber           = (RelativeLayout) findViewById(R.id.share_viber);
        whatsapp        = (RelativeLayout) findViewById(R.id.share_whatsapp);
        telegram        = (RelativeLayout) findViewById(R.id.share_telegram);
        facebookButton = (RelativeLayout) findViewById(R.id.share_facebook);
        linkedin        = (RelativeLayout) findViewById(R.id.share_linkedin);
    }

    @Override
    protected void callOnGetSystemMessage(String text, String status) {
        JSONArray   systemMessages;

        if ( status.equals(ConstStatus.ST_TRUE)) {
            utilPrefCustom.saveSystemMessages(text);
        } else {
            text = utilPrefCustom.getSystemMessage();
        }

        try {
            JSONObject jsonObject = new JSONObject(text);

            systemMessages  = jsonObject.getJSONArray("systemMessages");
            promoCode       = jsonObject.optString("promoCode");
            androidAppUrl   = jsonObject.optString("androidAppUrl");
            iosAppUrl       = jsonObject.optString("iosAppUrl");
            inviteUrl       = jsonObject.optString("inviteUrl");

            for ( int i = 0; i < systemMessages.length(); i ++ ) {
                JSONObject object = new JSONObject(systemMessages.getString(i));

                if ( object.optString("messageCode").equals("STRING_TELEGRAM_MESSAGE") ) {
                    telegramMsg = object.optString("message");
                } else if ( object.optString("messageCode").equals("STRING_WHATSAPP_MESSAGE") ) {
                    whatsAppMsg = object.optString("message");
                } else if ( object.optString("messageCode").equals("STRING_VIBER_MESSAGE") ) {
                    viberMsg = object.optString("message");
                } else if ( object.optString("messageCode").equals("STRING_EMAIL_MESSAGE") ) {
                    emailMsg = object.optString("message");
                } else if ( object.optString("messageCode").equals("STRING_SMS_MESSAGE") ) {
                    smsMsg = object.optString("message");
                } else if ( object.optString("messageCode").equals("STRING_FACEBOOK_MESSAGE") ) {
                    facebookMsg = object.optString("message");
                } else if ( object.optString("messageCode").equals("STRING_LINKEDIN_MESSAGE") ) {
                    linkedInMsg = object.optString("message");
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
