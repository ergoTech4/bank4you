package uk.bank4you.client.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.helpshift.support.Support;

import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.response.ReqMain;

public class ActSignUpPin extends BasicActivity {

    public static final String TAG = "ActSignUpPin";

    private ImageView   btnNewCardFotoPasport;
    private ImageView   imgBack;
    private EditText    edtPin1;
    private EditText    edtPin2;
    private EditText    edtPin3;
    private EditText    edtPin4;
    private TextView    btnChat;

    private static ActSignUpPin inst;

    public static ActSignUpPin instance() {
        return inst;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_pin);

        initialize();

        edtPin1.addTextChangedListener(new GenericTextWatcher(edtPin1));
        edtPin2.addTextChangedListener(new GenericTextWatcher(edtPin2));
        edtPin3.addTextChangedListener(new GenericTextWatcher(edtPin3));
        edtPin4.addTextChangedListener(new GenericTextWatcher(edtPin4));

        btnNewCardFotoPasport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pin = edtPin1.getText().toString() + edtPin2.getText().toString() + edtPin3.getText().toString() + edtPin4.getText().toString();
                if ( pin.length() == 4 ) {
                    utilStartService.startMyIntentService(new ReqMain.ReqSignUpPin(pin,
                            util.id(ActSignUpPin.this),
                            utilPrefCustom.getPhoneNumber()));
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        (findViewById(R.id.mainLL)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Support.showConversation(ActSignUpPin.this);
            }
        });

        edtPin4.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ( keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 1 ) {
                    String pin = edtPin1.getText().toString() + edtPin2.getText().toString() + edtPin3.getText().toString() + edtPin4.getText().toString();
                    if ( pin.length() == 4 ) {
                        utilStartService.startMyIntentService(new ReqMain.ReqSignUpPin(pin,
                                util.id(ActSignUpPin.this),
                                utilPrefCustom.getPhoneNumber()));
                    }
                    return true;
                }
                return false;
            }
        });

    }

    public void updateList(final String smsMessage) {

        edtPin1.setText(String.valueOf(smsMessage.charAt(0)));
        edtPin2.setText(String.valueOf(smsMessage.charAt(1)));
        edtPin3.setText(String.valueOf(smsMessage.charAt(2)));
        edtPin4.setText(String.valueOf(smsMessage.charAt(3)));

        utilStartService.startMyIntentService(new ReqMain.ReqSignUpPin(smsMessage,
                util.id(ActSignUpPin.this),
                utilPrefCustom.getPhoneNumber()));
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        inst = this;
        edtPin1.requestFocus();

        edtPin1.postDelayed(new Runnable() {

            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(edtPin1, 0);
            }
        },200); //use 300 to make it run when coming back from lock screen
    }

    @Override
    protected void callOnSignUpPin(String status) {
        if ( status.equals(ConstStatus.ST_TRUE) ) {
            openNextSignUpScreen();
//            startActivity(new Intent(ConstIntents.IN_ActNewCardFotoPassport));

            utilStartService.startMyIntentService(
                    new ReqMain.ReqGetCardsAvailableAtRegistrationPhase());
            ActSignUpPin.this.finish();
        }
    }

    private void initialize() {
        btnNewCardFotoPasport = (ImageView) findViewById(R.id.btnNewCardFotoPasport);
        imgBack = (ImageView) findViewById(R.id.img_back);
        btnChat = (TextView) findViewById(R.id.btnChat);

        edtPin1 = (EditText) findViewById(R.id.edtPin1);
        edtPin2 = (EditText) findViewById(R.id.edtPin2);
        edtPin3 = (EditText) findViewById(R.id.edtPin3);
        edtPin4 = (EditText) findViewById(R.id.edtPin4);
    }

    private class GenericTextWatcher implements TextWatcher {
        private View view;
        private GenericTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        public void onTextChanged(CharSequence cs, int start, int before, int count) {}

        public void afterTextChanged(Editable editable) {
            switch(view.getId()){
                case R.id.edtPin1:
                    if (!edtPin1.getText().toString().equals("")) {
                        edtPin2.requestFocus();
                        if (!edtPin2.getText().toString().equals("")) {
                            edtPin2.setSelection(1);
                        }
                    }
                    break;
                case R.id.edtPin2:
                    if (!edtPin2.getText().toString().equals("")) {
                        edtPin3.requestFocus();
                        if (!edtPin3.getText().toString().equals("")) {
                            edtPin3.setSelection(1);
                        }
                    } else {
                        edtPin1.requestFocus();
                        if (!edtPin1.getText().toString().equals("")) {
                            edtPin1.setSelection(1);
                        }
                    }
                    break;
                case R.id.edtPin3:
                    if (!edtPin3.getText().toString().equals("")) {
                        edtPin4.requestFocus();
                        if (!edtPin4.getText().toString().equals("")) {
                            edtPin4.setSelection(1);
                        }
                    } else {
                        edtPin2.requestFocus();
                        if (!edtPin2.getText().toString().equals("")) {
                            edtPin2.setSelection(1);
                        }
                    }
                    break;
                case R.id.edtPin4:
                    if (edtPin4.getText() == null || edtPin4.getText().toString().equals("")) {
                        edtPin3.requestFocus();
                        if (!edtPin3.getText().toString().equals("")) {
                            edtPin3.setSelection(1);
                        }
                    }

                    break;
            }
        }
    }


}
