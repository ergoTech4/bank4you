package uk.bank4you.client.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.helpshift.support.Support;

import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.response.ReqMain;

public class ActNewPassword extends BasicActivity {

    private ImageView   btnChangePass;
    private ImageView   imgBack;
    private EditText    newPassword;
    private TextView    btnChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_new_password);

        initialize();

        btnChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String password = newPassword.getText().toString();

                if ( password.length() >= 4 ) {
                    utilStartService.startMyIntentService(new ReqMain.ReqForgotPassword2(utilPrefCustom.getToken(), password));
                } else {
                    util.myToast(getString(R.string.password_too_short));
                }
            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Support.showConversation(ActNewPassword.this);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        (findViewById(R.id.mainLL)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });

        newPassword.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ( keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 1 ) {

                    String password = newPassword.getText().toString();

                    if ( password.length() >= 4 ) {
                        utilStartService.startMyIntentService(new ReqMain.ReqForgotPassword2(utilPrefCustom.getToken(), password));
                    } else {
                        util.myToast(getString(R.string.password_too_short));
                    }
                    return true;
                }
                return false;
            }
        });

    }

    private void initialize() {
        btnChangePass   = (ImageView) findViewById(R.id.btn_change_pass);
        imgBack         = (ImageView) findViewById(R.id.img_back);
        newPassword     = (EditText) findViewById(R.id.edit_field_password);
        btnChat         = (TextView) findViewById(R.id.btnChat);
    }


    protected void callOnForgotPassword(String text, String status){
        if (status.equals(ConstStatus.ST_TRUE)) {
            Toast.makeText(this, getString(R.string.password_success_changed), Toast.LENGTH_LONG).show();
            Intent intentHome = new Intent(ConstIntents.IN_ActSignIn);
            intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentHome);
        } else {
            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        }
    }

}
