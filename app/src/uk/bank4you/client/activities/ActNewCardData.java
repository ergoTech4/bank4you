package uk.bank4you.client.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.helpshift.support.Support;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.response.ReqMain;

public class ActNewCardData extends BasicActivity {

    private ImageView btnNewCardType;
    private ImageView imgBack;
    private EditText edtEmail;
    private EditText edtPass;
    private EditText edtPromo;
    private TextView btnChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_new_card_data);

        initialize();

        btnNewCardType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ( !util.isValidEmail(edtEmail.getText().toString()) && edtPass.getText().toString().length() < 6 ) {
                    util.myToast(getString(R.string.enter_email_and_password));

                } else if ( edtPass.getText().toString().length() < 6 ) {
                    util.myToast(getString(R.string.enter_password));

                } else {
                    utilStartService.startMyIntentService(new ReqMain.ReqSignUpPass(util.id(ActNewCardData.this),
                            utilPrefCustom.getSessionToken(),
                            edtPass.getText().toString(),
                            edtEmail.getText().toString(),
                            edtPromo.getText().toString()));
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Support.showConversation(ActNewCardData.this);
            }
        });

        edtEmail.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_ENTER)) {
                    edtPass.requestFocus();
                    return true;
                }
                return false;
            }
        });

        edtPass.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ( keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 1 ) {

                    if ( !util.isValidEmail(edtEmail.getText().toString()) && edtPass.getText().toString().length() < 6 ) {
                        util.myToast(getString(R.string.enter_email_and_password));

                    } else if ( edtPass.getText().toString().length() < 6 ) {
                        util.myToast(getString(R.string.enter_password));

                    } else {
                        utilStartService.startMyIntentService(new ReqMain.ReqSignUpPass(util.id(ActNewCardData.this),
                                utilPrefCustom.getSessionToken(),
                                edtPass.getText().toString(),
                                edtEmail.getText().toString(),
                                edtPromo.getText().toString()));
                    }

                    return true;
                }
                return false;
            }
        });
    }

    private void initialize() {
        btnNewCardType  = (ImageView) findViewById(R.id.btnNewCardType);
        imgBack         = (ImageView) findViewById(R.id.img_back);
        btnChat         = (TextView) findViewById(R.id.btnChat);
        edtEmail        = (EditText) findViewById(R.id.edtEmail);
        edtPass         = (EditText) findViewById(R.id.edtPass);
        edtPromo        = (EditText) findViewById(R.id.edtPromo);
    }

    @Override
    protected void callOnSignUpPass(String status) {
        if (status.equals(ConstStatus.ST_TRUE)) {
            startActivity(new Intent(ConstIntents.IN_ActNewCardTypeSelected));
            finish();
        }
    }
}
