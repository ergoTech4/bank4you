package uk.bank4you.client.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.helpshift.support.Support;
import uk.bank4you.client.BuildConfig;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.Const;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.response.ReqMain;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ActNewCardFotoPassport extends BasicActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final int REQUEST_CODE_SOME_FEATURES_PERMISSIONS = 12;
    private ImageView btnNewCardFotoCheck;
    private TextView btnChat;
    private ImageView imgPhoto;

    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private String currentImagePath;
    private BottomSheetLayout bottomSheet;
    private ImageView imgDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_new_card_foto_pasport);

        initialize();

        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                //.showImageForEmptyUri(R.drawable.ic_empty_img)
                //.showImageOnFail(R.drawable.ic_empty_img)
                .resetViewBeforeLoading(true)
                .cacheOnDisc(false)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();

        utilFileManager.createImageCashFolder();

        btnNewCardFotoCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (app.imageInBase64 == null) {
                    util.myToast("Select passport photo.");
                } else {
                    utilStartService.startMyIntentService(new ReqMain.ReqSignUpPhoto(util.id(ActNewCardFotoPassport.this),
                            utilPrefCustom.getSessionToken(), 0));
                }

            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Support.showConversation(ActNewCardFotoPassport.this);
            }
        });

        imgPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheet.showWithSheetView(LayoutInflater.from(ActNewCardFotoPassport.this).inflate(R.layout.my_sheet_layout, bottomSheet, false));

                TextView txtTakePhoto   = (TextView) findViewById(R.id.txtTakePhoto);
                TextView txtGallery     = (TextView) findViewById(R.id.txtGallery);

                txtTakePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCamera();
                    }
                });

                txtGallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openGallery();
                    }
                });
            }
        });
    }

    private void openCamera() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int hasCAMERAPermission = checkSelfPermission(Manifest.permission.CAMERA);
            int hasSTORAGEPermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            List<String> permissions = new ArrayList<String>();

            if (hasCAMERAPermission != PackageManager.PERMISSION_GRANTED && hasSTORAGEPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.CAMERA);
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            } else {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + BuildConfig.IMAGES_PHOTO_FOLDER;
                File dir = new File(dirPath);
                dir.mkdir();
                currentImagePath = dirPath + "passport.jpg";
                Uri imageUri = Uri.fromFile(new File(currentImagePath));
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(cameraIntent, 2);
            }

            if (!permissions.isEmpty()) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), REQUEST_CODE_SOME_FEATURES_PERMISSIONS);
            }

        } else {

            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + BuildConfig.IMAGES_PHOTO_FOLDER;
            File dir = new File(dirPath);
            dir.mkdir();
            currentImagePath = dirPath + "passport.jpg";
            Uri imageUri = Uri.fromFile(new File(currentImagePath));
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(cameraIntent, 2);
        }
    }

    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, 1);
    }

    private void initialize() {
        btnNewCardFotoCheck = (ImageView) findViewById(R.id.btnNewCardFotoCheck);
        btnChat = (TextView) findViewById(R.id.btnChat);
        imgPhoto = (ImageView) findViewById(R.id.imgPhoto);
        bottomSheet = (BottomSheetLayout) findViewById(R.id.bottomsheet);
        imgDone = (ImageView) findViewById(R.id.imgDone);
    }

    @Override
    protected void callOnSignUpPhoto(String status) {
        if (status.equals(ConstStatus.ST_TRUE)) {
            startActivity(new Intent(ConstIntents.IN_ActNewCardFotoCheck));
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                if (requestCode == 2) {
                    if (currentImagePath != null && (new File(currentImagePath)).exists()) {
                        utilStartService.startResizeImage(currentImagePath,
                                utilFileManager.getDirPathImageCashFolder() + "passport.jpg",
                                Uri.parse("file://" + currentImagePath));
                    }
                } else if (requestCode == 1) {
                    Uri selectedImageUri = data.getData();
                    String imagePath = util.getRealPathFromURI(selectedImageUri);
                    String dirPath = utilFileManager.getDirPathImageCashFolder();
                    String extension = imagePath.substring(imagePath.lastIndexOf("."));
                    String imageFilePathTo = dirPath + utilFileManager.createFileName() + extension;
                    utilStartService.startResizeImage(imagePath, imageFilePathTo, selectedImageUri);
                }
                bottomSheet.dismissSheet();
                break;
        }
    }

    @Override
    protected void callOnResizedImage(String status, String filePathTo) {
        imageLoader.displayImage("file://" + filePathTo, imgPhoto, options);
        imgDone.setVisibility(View.VISIBLE);
        //imgPhoto.setImageResource(R.drawable.ic_photo_done);
    }

    @Override
    protected void onResume() {
        super.onResume();
        imgPhoto.setImageResource(R.drawable.ic_photo_take);
        imgDone.setVisibility(View.GONE);
        app.imageInBase64 = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch ( requestCode ) {
            case REQUEST_CODE_SOME_FEATURES_PERMISSIONS: {
                for( int i = 0; i < permissions.length; i++ ) {
                    if( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {

                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + BuildConfig.IMAGES_PHOTO_FOLDER;
                        File dir = new File(dirPath);
                        dir.mkdir();
                        currentImagePath = dirPath + "passport.jpg";
                        Uri imageUri = Uri.fromFile(new File(currentImagePath));
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        startActivityForResult(cameraIntent, 2);

                        Log.d( "Permissions", "Permission Granted: " + permissions[i] );
                    } else if( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
                        Log.d( "Permissions", "Permission Denied: " + permissions[i] );
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

}
