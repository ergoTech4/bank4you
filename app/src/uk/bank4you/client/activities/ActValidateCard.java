package uk.bank4you.client.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.helpshift.support.Support;

import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.response.ReqMain;

public class ActValidateCard extends BasicActivity {

    private static final String TAG = "ActValidateCard";

    private TextView    btnChat;
    private ImageView   imgBack;
    private Button      activateCard;
    private EditText    batch, cardNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_validate_card);

        initialize();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Support.showConversation(ActValidateCard.this);
            }
        });

        activateCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( batch.getText().toString().length() < 4 ) {
                    util.myToast(getString(R.string.batch_number_incorrect));
                } else if ( cardNumber.getText().toString().length() < 16 ) {
                    util.myToast(getString(R.string.card_number_incorrect));
                } else {
                    utilStartService.startMyIntentService(new ReqMain.ReqValidateCard(batch.getText().toString(), cardNumber.getText().toString()));
                }

            }
        });

        batch.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ( keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 1  ) {

                    if ( batch.getText().toString().length() < 4 ) {
                        util.myToast(getString(R.string.batch_number_incorrect));
                    } else if ( cardNumber.getText().toString().length() < 16 ) {
                        util.myToast(getString(R.string.card_number_incorrect));
                    } else {
                        utilStartService.startMyIntentService(new ReqMain.ReqValidateCard(batch.getText().toString(), cardNumber.getText().toString()));
                    }

                    return true;
                }
                return false;
            }
        });

        cardNumber.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ( keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 1  ) {
                    batch.requestFocus();

                    return true;
                }
                return false;
            }
        });

        findViewById(R.id.mainLL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        cardNumber.requestFocus();

        cardNumber.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(batch, 0);
            }
        }, 200); //use 300 to make it run when coming back from lock screen
    }

    private void initialize() {
        cardNumber      = (EditText) findViewById(R.id.validate_card_number);
        batch           = (EditText) findViewById(R.id.validate_batch);
        btnChat         = (TextView) findViewById(R.id.btnChat);
        imgBack         = (ImageView) findViewById(R.id.img_back);
        activateCard    = (Button) findViewById(R.id.btn_activate);
    }

    @Override
    protected void callOnCardValidation(String msg, String status) {
        if (status.equals(ConstStatus.ST_TRUE)) {

            AlertDialog.Builder adb = new AlertDialog.Builder(this);

            adb.setTitle(R.string.success);
            adb.setMessage(R.string.card_validate_success);
            adb.setIcon(android.R.drawable.ic_dialog_info);
            adb.setPositiveButton(R.string.close, myClickListener);
            adb.setCancelable(false);

            adb.show();
        }
    }

    DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    finish();
                    break;
            }
        }
    };
}
