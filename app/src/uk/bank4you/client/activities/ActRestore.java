package uk.bank4you.client.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.helpshift.support.Support;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.Const;
import uk.bank4you.client.constants.ConstEX;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.response.ReqMain;

public class ActRestore extends BasicActivity {

    private ImageView   btnSendPhone;
    private ImageView   imgBack;
    private EditText    edtPhone;
    private TextView    btnChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_restore);

        initialize();

        btnSendPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String phoneNumber = edtPhone.getText().toString();

                if ( phoneNumber.length() > 6 ) {
                    if ( !phoneNumber.startsWith("+")){
                        phoneNumber = "+" + phoneNumber;
                    }
                    utilPrefCustom.setPhoneNumber(phoneNumber);
                    utilStartService.startMyIntentService(new ReqMain.ReqForgotPassword(phoneNumber));
                } else {
                    util.myToast(getString(R.string.phone_number_too_short));
                }
            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Support.showConversation(ActRestore.this);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        (findViewById(R.id.mainLL)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });


        edtPhone.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ( keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 1 ) {
                    String phoneNumber = edtPhone.getText().toString();

                    if ( phoneNumber.length() > 6 ) {
                        if ( !phoneNumber.startsWith("+")){
                            phoneNumber = "+" + phoneNumber;
                        }
                        utilPrefCustom.setPhoneNumber(phoneNumber);
                        utilStartService.startMyIntentService(new ReqMain.ReqForgotPassword(phoneNumber));
                    } else {
                        util.myToast(getString(R.string.phone_number_too_short));
                    }

                    return true;
                }
                return false;
            }
        });
    }

    private void initialize() {
        btnSendPhone    = (ImageView) findViewById(R.id.btn_send_phone);
        imgBack         = (ImageView) findViewById(R.id.img_back);
        edtPhone        = (EditText) findViewById(R.id.edit_field_phone);
        btnChat         = (TextView) findViewById(R.id.btnChat);
    }


    protected void callOnForgotPassword(String text, String status){
        if (status.equals(ConstStatus.ST_TRUE)) {
            utilPrefCustom.setPhoneNumber(edtPhone.getText().toString());
            startActivity(new Intent(ConstIntents.IN_ActRestorePin));
        }
    }

}
