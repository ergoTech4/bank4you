package uk.bank4you.client.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstCardTypes;
import uk.bank4you.client.constants.ConstEX;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.multyfields.AvailableCard;

public class ActSuccessPay extends BasicActivity {

    private static final String TAG = "ActSuccessPay";
    private TextView    btnSignIn, title, description;
    private ImageView   imgCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_success_pay);

        initialize();

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        if ( getIntent().getExtras().getBoolean(ConstStatus.ST_SUCCESS, false) ) {
            title.setText(getString(R.string.congratulations));
            description.setText(getString(R.string.success_card_order));
        } else {
            title.setText(getString(R.string.payment_failed));
            description.setText(getString(R.string.cant_proccess_payment));
        }

        ImageLoader.getInstance().displayImage(utilPrefCustom.getCardSuccesLink(), imgCard, options);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentHome = new Intent(ConstIntents.IN_ActSignIn);
                intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentHome);
            }
        });
    }

    private void initialize() {
        btnSignIn       = (TextView) findViewById(R.id.btnSignIn);
        title           = (TextView) findViewById(R.id.act_title_text);
        description     = (TextView) findViewById(R.id.act_descript_text);
        imgCard         = (ImageView) findViewById(R.id.imgCard);
    }

    @Override
    public void onBackPressed() {
        Intent intentHome = new Intent(ConstIntents.IN_ActSignIn);
        intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentHome);
    }
}
