package uk.bank4you.client.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.helpshift.support.Support;

import java.util.ArrayList;
import java.util.List;

import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.response.ReqMain;
import uk.bank4you.client.services.IncomingSms;
import uk.bank4you.client.utils.Util;

public class ActSignUp extends BasicActivity {

    private static final String TAG = "ActSignUp";
    private ImageView   btnSendPhone;
    private ImageView   imgBack;
    private EditText    edtPhone;
    private TextView    btnChat;
    private String      tryGetPhoneNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_sign_up);

        initialize();

        if ( utilPrefCustom.getPhoneNumber().length() != 0 ) {
            edtPhone.setText(utilPrefCustom.getPhoneNumber());
            edtPhone.setSelection(utilPrefCustom.getPhoneNumber().length());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasPHONEPermission = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);

            if ( hasPHONEPermission == PackageManager.PERMISSION_GRANTED ) {
                TelephonyManager telManager = (TelephonyManager) this
                        .getSystemService(Context.TELEPHONY_SERVICE);

                tryGetPhoneNumber = telManager.getLine1Number();

                if (tryGetPhoneNumber != null) {
                    edtPhone.setText(String.format("%s", tryGetPhoneNumber));
                    edtPhone.setSelection(tryGetPhoneNumber.length());
                }
            }
        } else {
            TelephonyManager telManager = (TelephonyManager) this
                    .getSystemService(Context.TELEPHONY_SERVICE);

            tryGetPhoneNumber = telManager.getLine1Number();

            if ( tryGetPhoneNumber != null ) {
                edtPhone.setText(String.format("%s", tryGetPhoneNumber));
                edtPhone.setSelection(tryGetPhoneNumber.length());
            }
        }

        btnSendPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String phoneNumber = edtPhone.getText().toString();
                if ( phoneNumber.length() > 6 ) {
                    if ( !phoneNumber.startsWith("+")){
                        phoneNumber = "+" + phoneNumber;
                    }

                    utilPrefCustom.setPhoneNumber(phoneNumber);
                    utilStartService.startMyIntentService(new ReqMain.ReqSignUpPhone(phoneNumber, util.id(ActSignUp.this)));
                } else {
                    util.myToast(getString(R.string.phone_number_too_short));
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        (findViewById(R.id.mainLL)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Support.showConversation(ActSignUp.this);
            }
        });

        edtPhone.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 1) {

                    String phoneNumber = edtPhone.getText().toString();
                    if ( phoneNumber.length() > 6 ) {
                        if ( !phoneNumber.startsWith("+")){
                            phoneNumber = "+" + phoneNumber;
                        }

                        utilPrefCustom.setPhoneNumber(phoneNumber);
                        utilStartService.startMyIntentService(new ReqMain.ReqSignUpPhone(phoneNumber, util.id(ActSignUp.this)));
                    } else {
                        util.myToast(getString(R.string.phone_number_too_short));
                    }

                    return true;
                }
                return false;
            }
        });

        utilStartService.startMyIntentService(
                new ReqMain.ReqGetCardsAvailableAtRegistrationPhase());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int hasSMSPermission = checkSelfPermission(Manifest.permission.READ_SMS);
            List<String> permissions = new ArrayList<String>();


            if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.READ_SMS);
            }

            if (!permissions.isEmpty()) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), 1);
            }
        }

    }

    private void initialize() {
        btnSendPhone    = (ImageView) findViewById(R.id.btn_send_phone);
        imgBack         = (ImageView) findViewById(R.id.img_back);
        edtPhone        = (EditText) findViewById(R.id.edit_field_phone);
        btnChat         = (TextView) findViewById(R.id.btnChat);
    }

    @Override
    protected void onResume() {
        super.onResume();
        edtPhone.requestFocus();

        edtPhone.postDelayed(new Runnable() {

            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(edtPhone, 0);
            }
        },200); //use 300 to make it run when coming back from lock screen
    }

    @Override
    protected void callOnSignUpPhone(String status) {
        if (status.equals(ConstStatus.ST_TRUE)) {
            startActivity(new Intent(ConstIntents.IN_ActSignUpPin));
        }
    }
}
