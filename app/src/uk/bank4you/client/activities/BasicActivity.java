package uk.bank4you.client.activities;

import android.app.ProgressDialog;
import android.content.*;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import uk.bank4you.client.App;
import uk.bank4you.client.BuildConfig;
import uk.bank4you.client.constants.*;
import uk.bank4you.client.services.UtilStartService;
import uk.bank4you.client.utils.Util;
import uk.bank4you.client.utils.UtilFileManager;
import uk.bank4you.client.utils.UtilLog;
import uk.bank4you.client.utils.UtilPrefCustom;

public abstract class BasicActivity extends FragmentActivity implements DialogInterface.OnCancelListener{
    protected UtilLog           utilLog;
    protected UtilPrefCustom    utilPrefCustom;
    protected Util              util;
    protected UtilFileManager   utilFileManager;
    protected App               app;
    protected UtilStartService  utilStartService;

    // настроим слушатель
    private MyReceiver          receiver;
    private IntentFilter        filter;

    private ProgressDialog      progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app             = ((App) getApplication());
        utilPrefCustom  = new UtilPrefCustom(this);
        util            = new Util(this);
        utilLog         = new UtilLog(this);
        receiver        = new MyReceiver();
        filter          = new IntentFilter(BuildConfig.BC_ACTION);

        utilStartService    = new UtilStartService(this);
        utilFileManager     = new UtilFileManager(this);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(receiver);
        dismissDialog();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
        if ( app.getProgressBar().getStatus())
            showMyProgressDialog(app.getProgressBar().getText() );
    }

    // *************************************************************************************************
    protected void showMyProgressDialog(String msg) {
        if ( progressDialog == null || !progressDialog.isShowing() ) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(!msg.equals("Завантаження додатку"));
            progressDialog.setOnCancelListener(this);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);

            try {
                progressDialog.show();
            } catch (Exception e) {
                new UtilLog().myLogExeption(e);
                app.setMyProgressBar("", false);
            }
        }
    }

    protected void dismissDialog() {
        if ( progressDialog != null && progressDialog.isShowing() ) {
            try { progressDialog.dismiss();
            } catch (Exception e) {
                utilLog.myLogFlurry(ConstStatus.ST_ERROR, "", e);}
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        app.setMyProgressBar("", false);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int event = intent.getIntExtra("EVENT", 0);

            switch (event) {
                case ConstBC.BC_EVENT_PROGRESS:
                    switch (intent.getIntExtra("STATUS", 0)) {
                        case ConstProgress.BC_STATUS_PROGRESS_START:
                            showMyProgressDialog(intent.getStringExtra("TEXT"));
                            break;
                        case ConstProgress.BC_STATUS_PROGRESS_STOP:
                            dismissDialog();
                            break;
                    }
                    break;
                case ConstBC.BC_EVENT_TOAST:
                    Toast.makeText(BasicActivity.this, intent.getStringExtra("TEXT"), Toast.LENGTH_LONG).show();
                    break;
                case ConstBC.BC_EVENT_SIGNUP_PHONE:
                    callOnSignUpPhone(intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_SIGNIN_PHONE:
                    callOnSignInPhone(intent.getStringExtra("STATUS"), intent.getStringExtra("TEXT"));
                    break;
                case ConstBC.BC_EVENT_SIGNUP_PIN:
                    callOnSignUpPin(intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_SIGNUP_PHOTO:
                    callOnSignUpPhoto(intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_SIGNUP_PASS:
                    callOnSignUpPass(intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_SIGNUP_CARD_ID:
                    callOnSignUpCardId(intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_HELLO:
                    callOnHello(intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_GET_AUTH_TOKEN:
                    callOnGetToken(intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_GET_CARD_LIST:
                    callOnGetCardList(intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_GET_TRANSACTION_LIST:
                    callOnGetTransactionList(intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_RESIZE_IMAGE:
                    callOnResizedImage(intent.getStringExtra("TEXT"), intent.getStringExtra("LOG"));
                    break;
                case ConstBC.BC_EVENT_GET_STARTED:
                    callOnStarted();
                    break;
                case ConstBC.BC_EVENT_GET_MONEYPOLOLINK:
                    callOnStartedWeb(intent.getStringExtra("TEXT"));
                    break;
                case ConstBC.BC_EVENT_GET_CARD_LIST_AVAILABLE_IN_REG:
                    callOnGetAvailableCardList();
                    break;
                case ConstBC.BC_EVENT_CARD_VALIDATION:
                    callOnCardValidation(intent.getStringExtra("TEXT"), intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_FORGOT_PASSWORD:
                    callOnForgotPassword(intent.getStringExtra("TEXT"), intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_FORGOT_PASSWORD1:
                    callOnForgotPassword(intent.getStringExtra("TEXT"), intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_FORGOT_PASSWORD2:
                    callOnForgotPassword(intent.getStringExtra("TEXT"), intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_GET_TRANSACTION_LIST_BEFORE:
                    callOnGetTransactionList(intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_GET_ACCOUNT_DETAILS:
                    callGetAccountDetails(intent.getStringExtra("TEXT"), intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_CHANGE_PASSWORD:
                    callGetAccountDetails(intent.getStringExtra("TEXT"), intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_SIGNIN_PIN:
                    callOnSignInPhone(intent.getStringExtra("STATUS"), intent.getStringExtra("TEXT"));
                    break;
                case ConstBC.BC_EVENT_SAVE_CLIENT_PHONE_CONTACTS:
                    callOnSaveClientPhoneContacts(intent.getStringExtra("STATUS"));
                    break;
                case ConstBC.BC_EVENT_SYSTEM_MESSAGE:
                    callOnGetSystemMessage(intent.getStringExtra("TEXT"), intent.getStringExtra("STATUS"));
                    break;
            }
        }
    }


    protected void callOnSignUpPhone(String status) {}

    protected void callOnSignInPhone(String status, String text) {}

    protected void callOnSignUpPin(String status) {}
    protected void callOnSignUpPhoto(String status) {}
    protected void callOnSignUpPass(String status) {}
    protected void callOnSignUpCardId(String status) {}
    protected void callOnHello(String status) {}
    protected void callOnGetToken(String status) {}
    protected void callOnGetCardList(String status) {}
    protected void callOnGetTransactionList(String status) {}
    protected void callOnResizedImage(String STATUS, String filePathInCash) {}
    protected void callOnStarted() {}
    protected void callOnStartedWeb(String text) {}
    protected void callOnGetAvailableCardList() {}
    protected void callOnCardValidation(String msg, String status){}
    protected void callOnForgotPassword(String text, String status){}
    protected void callGetAccountDetails(String text, String status){}
    protected void callOnSaveClientPhoneContacts(String status){}

    protected void callOnGetSystemMessage(String text, String status) {}


    // *************************************************************************************************
    // исправлял issue: https://code.google.com/p/android/issues/detail?id=19917
    @Override
    public void onSaveInstanceState(Bundle outState) {}

    // *************************************************************************************************
    protected void openNextSignUpScreen() {
        if ( utilPrefCustom.getclientNeedsToProvidePassportPic() ) {
            startActivity(new Intent(ConstIntents.IN_ActNewCardFotoPassport));

        } else if ( utilPrefCustom.getclientNeedsToProvideUtilityBillPic() ) {
            startActivity(new Intent(ConstIntents.IN_ActNewCardFotoCheck));

        } else if ( utilPrefCustom.getClientNeedsToSpecifyPassword() || utilPrefCustom.getclientNeedsToSpecifyEmail() ) {
            startActivity(new Intent(ConstIntents.IN_ActNewCardData));
        } else if ( utilPrefCustom.getInitiallySelectedCardId() == null
                || utilPrefCustom.getInitiallySelectedCardId().equals("")
                || utilPrefCustom.getInitiallySelectedCardId().equals("null") ) {
            startActivity(new Intent(ConstIntents.IN_ActNewCardTypeSelected));
        }
//        else if ( utilPrefCustom.getClientNeedsToPayForTheCard() ) {
//            Intent intent = new Intent(ConstIntents.IN_ActNewCardResult);
//            intent.putExtra(ConstEX.EX_CARD_TYPE, utilPrefCustom.getInitiallySelectedCardId());
//            startActivity(intent);
//        }
    }

    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)this.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException ignored){}
    }
}
