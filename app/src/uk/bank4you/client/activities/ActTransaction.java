package uk.bank4you.client.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.helpshift.support.Support;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;
import de.hdodenhof.circleimageview.CircleImageView;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstEX;
import uk.bank4you.client.multyfields.Transaction;
import uk.bank4you.client.utils.UtilDbGet;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ActTransaction extends BasicActivity {
    private ImageView btnChat;
    private Transaction transaction;
    private UtilDbGet utilDbGet;
    private CircleImageView circleImageView;
    private TextView txtName;
    private TextView txtComment;
    private TextView txtCurrency;
    private TextView txtAmount;
    private TextView txtDate;
    private TextView txtTime;
    private TextView txtNumber;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_transaction);

        initialize();

        int transactionId = getIntent().getExtras().getInt(ConstEX.EX_TRANSACTION_ID);
        String cardNumber = getIntent().getExtras().getString(ConstEX.EX_CARD_NUMBER);
        transaction = utilDbGet.getTransaction(transactionId);

        ImageAware imageAware = new ImageViewAware(circleImageView, false);
        imageLoader.displayImage(transaction.imgUrl, imageAware, options);

        txtName.setText(transaction.info);
        txtComment.setText(transaction.comment.equals("null") ? "": transaction.comment);
        txtAmount.setText(transaction.amount);

        if (transaction.currencyCode.equals("USD")) {
            txtCurrency.setText("$");
        } else if (transaction.currencyCode.equals("GBP")) {
            txtCurrency.setText("£");
        } else if (transaction.currencyCode.equals("EUR")) {
            txtCurrency.setText("€");
        }

        cardNumber = cardNumber.substring(0, 4) + " ****" + " **** " + cardNumber.substring(12, 16);
        txtNumber.setText(cardNumber);

        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = formatter.parse(transaction.dateTimeUtc);

            DateFormat dateFormat2 = new SimpleDateFormat("MMMM dd, yyyy");
            txtDate.setText(dateFormat2.format(date));

            DateFormat dateFormat3 = new SimpleDateFormat("EEEE, HH:mm");
            txtTime.setText(dateFormat3.format(date));
        } catch (ParseException e) {
            utilLog.myLogExeption(e);
        }

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Support.showConversation(ActTransaction.this);
            }
        });
    }

    private void initialize() {
        utilDbGet = new UtilDbGet(this);
        btnChat = (ImageView) findViewById(R.id.btnChat);
        circleImageView = (CircleImageView) findViewById(R.id.img_logo);
        txtName = (TextView) findViewById(R.id.txtName);
        txtComment = (TextView) findViewById(R.id.txtComment);
        txtCurrency = (TextView) findViewById(R.id.txtCurrency);
        txtAmount = (TextView) findViewById(R.id.txtAmount);
        txtDate = (TextView) findViewById(R.id.txtDate);
        txtTime = (TextView) findViewById(R.id.txtTime);
        txtNumber = (TextView) findViewById(R.id.txtNumber);
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.shape_empty)
                .showImageForEmptyUri(R.drawable.shape_empty)
                .showImageOnFail(R.drawable.shape_empty)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
    }
}
