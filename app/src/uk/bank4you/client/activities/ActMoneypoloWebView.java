package uk.bank4you.client.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.apache.http.util.EncodingUtils;

import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstStatus;

public class ActMoneypoloWebView extends BasicActivity {

    private static final String TAG = "ActMoneypoloWebView";
    private WebView browser;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_webview);

        initialize();

        String url = utilPrefCustom.getMoneyPoloLink();

        browser.getSettings().setJavaScriptEnabled(true);

        byte[] post = EncodingUtils.getBytes(utilPrefCustom.getFormData(), "SHA512");
        browser.postUrl(url, post);

        browser.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {

                if ( !url.contains(utilPrefCustom.getSuccessLink()) ) {
                    super.onPageFinished(view, url);
                }

                dismissDialog();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                if ( url.contains(utilPrefCustom.getSuccessLink()) ) {
                    Intent intent = new Intent(ActMoneypoloWebView.this, ActSuccessPay.class);
                    intent.putExtra(ConstStatus.ST_SUCCESS, true);
                    startActivity(intent);
                } else if (url.contains(utilPrefCustom.getFailureLink())) {
                    Intent intent = new Intent(ActMoneypoloWebView.this, ActSuccessPay.class);
                    intent.putExtra(ConstStatus.ST_SUCCESS, false);
                    startActivity(intent);
                } else {
                    super.onPageStarted(view, url, favicon);
                }

                showMyProgressDialog(getString(R.string.loading_page));
            }
        });

    }

    private void initialize() {
        browser = (WebView) findViewById(R.id.webview);
    }

    @Override
    public void onCancel(DialogInterface dialog) {

    }

}
