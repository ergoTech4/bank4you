package uk.bank4you.client.Demo;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.bank4you.client.R;
import uk.bank4you.client.multyfields.Transaction;
import uk.bank4you.client.utils.UtilDbGet;

public class AdapterListTransactionsDemo extends BaseAdapter {
    private static final String TAG = "AdapterListTransactionsDemo";

    private UtilDbGet           utilDbGet;
    private ImageLoader         imageLoader;
    private DisplayImageOptions options;
    private LayoutInflater      inflater;
    private Context             context;
    private DateFormat          dateFormat2 = new SimpleDateFormat("EEEE, MMMM dd");
    private DateFormat          formatter   = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    private DateFormat          dateFormat  = new SimpleDateFormat("HH:mm");

    private ArrayList<Transaction>  transactions;

    private HashMap<String, Locale> localeList = new HashMap<String, Locale>();

	public AdapterListTransactionsDemo(Context context, ArrayList<Transaction> transactions) {

        this.context = context;
        this.transactions = transactions;

        utilDbGet   = new UtilDbGet(context);
        imageLoader = ImageLoader.getInstance();

        options     = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.shape_empty)
                .showImageForEmptyUri(R.drawable.shape_empty)
                .showImageOnFail(R.drawable.shape_empty)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
        inflater = LayoutInflater.from(context);

    }

	private class Holder {
        CircleImageView imgLogo;
        TextView        txtName, txtComment, txtTime, txtSumm, txtCategory, txtTitle;
        LinearLayout    llTitle, llDiv;
    }


//    public View newView(Context context, Cursor cursor, ViewGroup parent) {
//        View convertView    = inflater.inflate(R.layout.list_item_transactions, parent, false);
//        Holder holder   = new Holder();
//
//        holder.txtName      = (TextView) convertView.findViewById(R.id.txt_name);
//        holder.txtComment   = (TextView) convertView.findViewById(R.id.txt_comment);
//        holder.txtTime      = (TextView) convertView.findViewById(R.id.txt_time);
//        holder.txtSumm      = (TextView) convertView.findViewById(R.id.txt_summ);
//        holder.imgLogo      = (CircleImageView) convertView.findViewById(R.id.img_logo);
//        holder.txtTitle     = (TextView) convertView.findViewById(R.id.txtTitle);
//        holder.llTitle      = (LinearLayout) convertView.findViewById(R.id.llTitle);
//        holder.llDiv        = (LinearLayout) convertView.findViewById(R.id.llDiv);
//
//        convertView.setTag(holder);
//
//        return convertView;
//    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final View      view;
        final Holder    holder;

        if ( convertView == null ) {
            view            = inflater.inflate(R.layout.list_item_transactions, parent, false);
            holder          = new Holder();

            holder.txtName      = (TextView) view.findViewById(R.id.txt_name);
            holder.txtComment   = (TextView) view.findViewById(R.id.txt_comment);
            holder.txtTime      = (TextView) view.findViewById(R.id.txt_time);
            holder.txtSumm      = (TextView) view.findViewById(R.id.txt_summ);
            holder.imgLogo      = (CircleImageView) view.findViewById(R.id.img_logo);
            holder.txtTitle     = (TextView) view.findViewById(R.id.txtTitle);
            holder.llTitle      = (LinearLayout) view.findViewById(R.id.llTitle);
            holder.llDiv        = (LinearLayout) view.findViewById(R.id.llDiv);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (Holder) view.getTag();
        }

        holder.txtName.setText(transactions.get(position).info);

        String comment = transactions.get(position).comment;
        holder.txtComment.setText(comment.equals("null") ? "": comment);

        String dateTimeUtc = transactions.get(position).dateTimeUtc;

        try {
            formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = formatter.parse(dateTimeUtc);
            dateFormat = new SimpleDateFormat("HH:mm");
            holder.txtTime.setText(dateFormat.format(date));

            dateFormat2 = new SimpleDateFormat("EEEE, MMMM dd");
            holder.txtTitle.setText(dateFormat2.format(date));

                String prevDate = transactions.get(position - 1).dateTimeUtc;
                if (prevDate != null && prevDate.substring(0, 10).equals(dateTimeUtc.substring(0, 10))) {
                    holder.llTitle.setVisibility(View.GONE);
                } else {
                    holder.llTitle.setVisibility(View.VISIBLE);
                }

                String nextDate = transactions.get(position + 1).dateTimeUtc;
                if (nextDate != null) {
                    holder.llDiv.setVisibility(nextDate.substring(0, 10).equals(dateTimeUtc.substring(0, 10)) ?
                            View.VISIBLE:View.GONE);
                } else {
                    holder.llDiv.setVisibility(View.VISIBLE);
                }
        } catch (IndexOutOfBoundsException e) {
            try {
                Date date = formatter.parse(dateTimeUtc);

                holder.txtTime.setText(dateFormat.format(date));
                holder.txtTitle.setText(dateFormat2.format(date));
                holder.llTitle.setVisibility(View.VISIBLE);
                holder.llDiv.setVisibility(View.VISIBLE);
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String amount = transactions.get(position).amount;


        if ( amount.startsWith("-") ) {
            holder.txtSumm.setTextColor(context.getResources().getColor(R.color.black));
        } else {
            holder.txtSumm.setTextColor(context.getResources().getColor(R.color.green));
        }

        String currencyCode = transactions.get(position).currencyCode;

        Currency    currency = Currency.getInstance(currencyCode);

        if ( currencyCode.equals("RUB") ) {
            holder.txtSumm.setText(String.format("₽ %s", amount));
//        } else if (currencyCode.equals("GBP")){
//            holder.txtSumm.setText(String.format("%s %s", currency.getSymbol(), amount));
        } else if (currencyCode.equals("CZK")) {
            holder.txtSumm.setText(String.format("Kč %s", amount));
        } else if (currencyCode.equals("EUR") || currencyCode.equals("USD") ) {
            holder.txtSumm.setText(String.format("%s %s", amount, currency.getSymbol()));
        } else {
            holder.txtSumm.setText(String.format("%s %s", currency.getSymbol(), amount));
        }


        String imgUrl = transactions.get(position).imgUrl;
        imageLoader.displayImage(imgUrl, holder.imgLogo, options);

        if (holder.imgLogo.getTag() == null || !holder.imgLogo.getTag().equals(imgUrl)) {
            ImageAware imageAware = new ImageViewAware(holder.imgLogo, false);
            imageLoader.displayImage(imgUrl, imageAware, options);
            holder.imgLogo.setTag(imgUrl);
        }

        return view;
    }

    @Override
    public int getCount() {
        return transactions.size();
    }

    @Override
    public Object getItem(int position) {
        return transactions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}