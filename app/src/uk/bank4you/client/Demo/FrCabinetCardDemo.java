package uk.bank4you.client.Demo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import uk.bank4you.client.App;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstEX;
import uk.bank4you.client.fragments.FrBasic;
import uk.bank4you.client.multyfields.Card;

public class FrCabinetCardDemo extends FrBasic {
    private App app;
    private int orderPosition;
    private TextView txtAmmount;
    private TextView txtCurrency;
    private TextView txtPan;
    private TextView txtDate;
    private TextView txtName;
    private ImageView imgLogo;
    private ArrayList<Card>         cards = new ArrayList<Card>();

    public static final FrCabinetCardDemo newInstance(int orderPosition) {
        FrCabinetCardDemo fragment = new FrCabinetCardDemo();

        final Bundle args = new Bundle();
        args.putInt(ConstEX.EX_CARD_ORDER_POSITION, orderPosition);
        fragment.setArguments(args);

        return fragment;
    }


    private void makeCardsListFromJson() {
        try {
            JSONObject jsonObject = new JSONObject(loadJSONFromAsset());

            JSONArray jsonArray = jsonObject.getJSONArray("cards");

            int size = jsonArray.length();

            for ( int i = 0; i < size; i++ ) {

                JSONObject object = jsonArray.getJSONObject(i);

                Card card = new Card(object.getString("availableBalance"),
                        object.getString("cardCurrency"),
                        object.getString("cardEmbossLine"),
                        object.getString("cardID"),
                        object.getString("cardNo"),
                        object.getString("effectiveDate"),
                        object.getString("expiryDate"),
                        object.getString("holdBalance"));

                cards.add(card);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("cards.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orderPosition = getArguments().getInt(ConstEX.EX_CARD_ORDER_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fr_cabinet_card, container, false);
        app = ((App) getActivity().getApplication());

        initialize(v);

        makeCardsListFromJson();

        Card card = cards.get(orderPosition);


        if (card.cardCurrency.equals("USD")) {
//            txtCurrency.setText("$");
            txtAmmount.setText(String.format("%s $", card.availableBalance));
        } else if (card.cardCurrency.equals("GBP")) {
//            txtCurrency.setText("£");
            txtAmmount.setText(String.format("£ %s", card.availableBalance));
        } else if (card.cardCurrency.equals("EUR")) {
            txtAmmount.setText(String.format("%s €", card.availableBalance));
        }

        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Calendar c = Calendar.getInstance();
            c.setTime(formatter.parse(card.expiryDate));
            c.add(Calendar.DATE, 1);

            DateFormat dateFormat = new SimpleDateFormat("MM");
            String month = dateFormat.format(c.getTime());

            DateFormat dateFormat1 = new SimpleDateFormat("yy");
            String year = dateFormat1.format(c.getTime());

            txtDate.setText(String.format("%s/%s", month, year));
        } catch (ParseException e) {
            utilLog.myLogExeption(e);
        }

        txtName.setText(card.cardEmbossLine.toUpperCase());

        String cardNo = card.cardNo;
        String cardNo2 = cardNo.substring(0, 4) + " ****" + " **** " + cardNo.substring(12, 16);

        txtPan.setText(cardNo2);
        int cardSystem = utilCreditCard.getCardNameId(cardNo);

        if (cardSystem == 0) {
            imgLogo.setImageResource(R.drawable.ic_visa);
        } else if (cardSystem == 1) {
            imgLogo.setImageResource(R.drawable.ic_master);
        }

        return v;
    }

    private void initialize(View v) {
        txtAmmount = (TextView) v.findViewById(R.id.txtAmmount);
        txtCurrency = (TextView) v.findViewById(R.id.txtCurrency);
        txtPan = (TextView) v.findViewById(R.id.txtPan);
        txtDate = (TextView) v.findViewById(R.id.txtDate);
        txtName = (TextView) v.findViewById(R.id.txtName);
        imgLogo = (ImageView) v.findViewById(R.id.imgLogo);
    }
}