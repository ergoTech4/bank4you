package uk.bank4you.client.Demo;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.helpshift.support.Support;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import uk.bank4you.client.R;
import uk.bank4you.client.activities.BasicActivity;
import uk.bank4you.client.constants.ConstIntents;
import uk.bank4you.client.constants.ConstLM;
import uk.bank4you.client.cursor_loaders.CursorLoaderTransactions;
import uk.bank4you.client.multyfields.Card;
import uk.bank4you.client.multyfields.Transaction;
import uk.bank4you.client.utils.UtilDbGet;

public class ActCabinetDemo extends BasicActivity implements LoaderManager.LoaderCallbacks<Cursor>{
    private static final String TAG = "ActCabinet";

    private AdapterListTransactionsDemo adapterListTransactions;
    private ListView                lvTransactions;
    private ViewPager               pager;
    private PagerAdapter            pagerAdapter;
//    private CirclePageIndicator     titleIndicator;
    private UtilDbGet               utilDbGet;
    private ImageView               btnChat;
    private boolean                 doubleBackToExitPressedOnce = false;
    private SwipyRefreshLayout      mSwipeRefreshLayout;
    private ArrayList<Card>         cards = new ArrayList<Card>();
    private ArrayList<Transaction>  transactions = new ArrayList<Transaction>();
    private LinearLayout            cardsLL, payLL, settingsLL, supportLL;
    private TextView                exitDemo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_cabinet);

        initialize();

        initializeMenu();

        makeCardsListFromJson();
        makeTransactionListFromJson();

        pager.setAdapter(pagerAdapter);

        adapterListTransactions = new AdapterListTransactionsDemo(this, transactions);

        lvTransactions.setAdapter(adapterListTransactions);

        getSupportLoaderManager().restartLoader(ConstLM.LM_TRANSACTIONS, null, this);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });

        lvTransactions.setOnItemClickListener(new ListView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> a, View view, int pos, long l) {

            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Support.showConversation(ActCabinetDemo.this);
            }
        });



        mSwipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setDistanceToTriggerSync(140);
//
//        mSwipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh(SwipyRefreshLayoutDirection direction) {
//
//            }
//        });

        exitDemo.setVisibility(View.VISIBLE);

        exitDemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void makeCardsListFromJson() {
        try {
            JSONObject jsonObject = new JSONObject(loadJSONFromAsset("cards.json"));

            JSONArray jsonArray = jsonObject.getJSONArray("cards");

            int size = jsonArray.length();

            for ( int i = 0; i < size; i++ ) {

                JSONObject object = jsonArray.getJSONObject(i);

                Card card = new Card(object.getString("availableBalance"),
                        object.getString("cardCurrency"),
                        object.getString("cardEmbossLine"),
                        object.getString("cardID"),
                        object.getString("cardNo"),
                        object.getString("effectiveDate"),
                        object.getString("expiryDate"),
                        object.getString("holdBalance"));

                cards.add(card);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void makeTransactionListFromJson() {
        try {
            JSONObject jsonObject = new JSONObject(loadJSONFromAsset("transactions.json"));

            JSONArray jsonArray = jsonObject.getJSONArray("transactions");

            int size = jsonArray.length();

            for ( int i = 0; i < size; i++ ) {

                JSONObject object = jsonArray.getJSONObject(i);

                Transaction transaction = new Transaction(
                        object.getString("additionalCommissionInCardCurrency"),
                        object.getString("amount"),
                        object.getString("amountInCardCurrency"),
                        "0",
                        object.getString("category"),
                        object.getString("comment"),
                        object.getString("currencyCode"),
                        object.getString("dateTimeUtc"),
                        object.getString("id"),
                        object.getString("imgUrl"),
                        object.getString("info"),
                        object.getString("mcc"),
                        object.getString("transactionType"));

                transactions.add(transaction);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String loadJSONFromAsset(String name) {
        String json = null;
        try {
            InputStream is = this.getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void initializeMenu() {

        cardsLL         = (LinearLayout) findViewById(R.id.act_cabinet_menu_cards);
        payLL           = (LinearLayout) findViewById(R.id.act_cabinet_menu_pay);
        settingsLL      = (LinearLayout) findViewById(R.id.act_cabinet_menu_settings);
        supportLL       = (LinearLayout) findViewById(R.id.act_cabinet_menu_support);

        cardsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !cardsLL.isSelected() ) {
                    cardsLL.setSelected(true);
                    payLL.setSelected(false);
                    settingsLL.setSelected(false);
                    supportLL.setSelected(false);
                }
            }
        });

        payLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !payLL.isSelected() ) {
                    payLL.setSelected(true);
                    cardsLL.setSelected(false);
                    settingsLL.setSelected(false);
                    supportLL.setSelected(false);

                    Intent intent = new Intent(ConstIntents.IN_ActPay).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("demo", true);

                    startActivity(intent);
                }
            }
        });

        settingsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !settingsLL.isSelected() ) {
                    settingsLL.setSelected(true);
                    payLL.setSelected(false);
                    cardsLL.setSelected(false);
                    supportLL.setSelected(false);

                    Intent intent = new Intent(ConstIntents.IN_ActSettings).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("demo", true);

                    startActivity(intent);

                }
            }
        });

        supportLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !supportLL.isSelected() ) {
                    supportLL.setSelected(true);
                    payLL.setSelected(false);
                    settingsLL.setSelected(false);
                    cardsLL.setSelected(false);

                    Intent intent = new Intent(ConstIntents.IN_ActSupport).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("demo", true);

                    startActivity(intent);
                }
            }
        });
    }

    private void initialize() {
        utilDbGet       = new UtilDbGet(this);
        pagerAdapter    = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        pager           = (ViewPager) findViewById(R.id.pager);
//        titleIndicator  = (CirclePageIndicator)findViewById(R.id.titles);

        lvTransactions  = (ListView) findViewById(R.id.list_transactions);
        btnChat         = (ImageView) findViewById(R.id.btnChat);

        mSwipeRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipeContainer);

        exitDemo        = (TextView) findViewById(R.id.cabinet_exit_demo);

    }

    @Override
    protected void callOnGetTransactionList(String status) {
        getSupportLoaderManager().restartLoader(ConstLM.LM_TRANSACTIONS, null, this);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        switch (id){
            case ConstLM.LM_TRANSACTIONS:
                return new CursorLoaderTransactions(ActCabinetDemo.this, cards.get(pager.getCurrentItem()).cardID);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        switch (cursorLoader.getId()){
            case ConstLM.LM_TRANSACTIONS:
//                adapterListTransactions.swapCursor(cursor);

                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {}

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        util.myToast(getString(R.string.back));

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2500);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new FrCabinetCardDemo().newInstance(position);
        }

        @Override
        public int getCount() {
            return cards.size();
        }

        public float getPageWidth(int position) {
            if ( getCount() > 1 ) {
                return 0.88f;
            } else {
                return 1f;
            }
        }

//        @Override
//        public float getPageWidth(int position) {
//            return 0.88f;
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        cardsLL.setSelected(true);
        payLL.setSelected(false);
        settingsLL.setSelected(false);
        supportLL.setSelected(false);
    }
}
