package uk.bank4you.client.constants;

public final class ConstProgress {
    /* PROGRESS STATUS */
    public static final int BC_STATUS_PROGRESS_START = 1;
    public static final int BC_STATUS_PROGRESS_STOP = 2;
}