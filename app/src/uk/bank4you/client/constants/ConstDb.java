package uk.bank4you.client.constants;

public final class ConstDb {
    private ConstDb() {}

    /* TABLES */
    public static final String tblCards         = "cards";
    public static final String tblTransactions  = "transactions";

    public static final String[] columnsCards = {
            "cardID",
            "cardNo",
            "cardCurrency",
            "availableBalance",
            "holdBalance",
            "effectiveDate",
            "expiryDate",
            "cardEmbossLine"};

    public static final String[] columnsTransactions = {
            "category",
            "id",
            "dateTimeUtc",
            "amount",
            "currencyCode",
            "amountInCardCurrency",
            "additionalCommissionInCardCurrency",
            "info",
            "transactionType",
            "mcc",
            "comment",
            "cardId",
            "imgUrl",
            "fav"};

    public static final String[] columnsTransactionsLight = {
            "_id",
            "category",
            "id",
            "dateTimeUtc",
            "amount",
            "currencyCode",
            "amountInCardCurrency",
            "additionalCommissionInCardCurrency",
            "info",
            "transactionType",
            "mcc",
            "comment",
            "cardId",
            "imgUrl",
            "fav"};

}