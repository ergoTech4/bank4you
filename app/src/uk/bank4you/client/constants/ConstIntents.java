package uk.bank4you.client.constants;


import uk.bank4you.client.BuildConfig;
import uk.bank4you.client.activities.*;

public final class ConstIntents {
    public static final String IN_ActChat = BuildConfig.PACKAGE + ".activities." + ActChat.class.getSimpleName();
    public static final String IN_ActMain = BuildConfig.PACKAGE + ".activities." + ActMain.class.getSimpleName();
    public static final String IN_ActNewCardData = BuildConfig.PACKAGE + ".activities." + ActNewCardData.class.getSimpleName();
    public static final String IN_ActNewCardFotoCheck = BuildConfig.PACKAGE + ".activities." + ActNewCardFotoCheck.class.getSimpleName();
    public static final String IN_ActNewCardFotoPassport = BuildConfig.PACKAGE + ".activities." + ActNewCardFotoPassport.class.getSimpleName();
    public static final String IN_ActNewCardResult = BuildConfig.PACKAGE + ".activities." + ActNewCardResult.class.getSimpleName();
    public static final String IN_ActNewCardTypeSelected = BuildConfig.PACKAGE + ".activities." + ActNewCardTypeSelected.class.getSimpleName();
    public static final String IN_ActSignUpPin = BuildConfig.PACKAGE + ".activities." + ActSignUpPin.class.getSimpleName();
    public static final String IN_ActSignIn = BuildConfig.PACKAGE + ".activities." + ActSignIn.class.getSimpleName();
    public static final String IN_ActSignUp = BuildConfig.PACKAGE + ".activities." + ActSignUp.class.getSimpleName();
    public static final String IN_ActCabinet = BuildConfig.PACKAGE + ".activities." + ActCabinet.class.getSimpleName();
    public static final String IN_ActRestore = BuildConfig.PACKAGE + ".activities." + ActRestore.class.getSimpleName();
    public static final String IN_ActTransaction = BuildConfig.PACKAGE + ".activities." + ActTransaction.class.getSimpleName();
    public static final String IN_ActMoneyWebView = BuildConfig.PACKAGE + ".activities." + ActMoneypoloWebView.class.getSimpleName();
    public static final String IN_ActWebView = BuildConfig.PACKAGE + ".activities." + ActivityWebView.class.getSimpleName();
    public static final String IN_ActValidateCard = BuildConfig.PACKAGE + ".activities." + ActValidateCard.class.getSimpleName();
    public static final String IN_ActRestorePin = BuildConfig.PACKAGE + ".activities." + ActRestorePin.class.getSimpleName();
    public static final String IN_ActNewPassword = BuildConfig.PACKAGE + ".activities." + ActNewPassword.class.getSimpleName();
    public static final String IN_ActSupport = BuildConfig.PACKAGE + ".activities." + ActSupport.class.getSimpleName();
    public static final String IN_ActFAQ = BuildConfig.PACKAGE + ".activities." + ActFAQ.class.getSimpleName();
    public static final String IN_ActPay = BuildConfig.PACKAGE + ".activities." + ActPay.class.getSimpleName();
    public static final String IN_ActSettings = BuildConfig.PACKAGE + ".activities." + ActSettings.class.getSimpleName();
    public static final String IN_ActChangePass = BuildConfig.PACKAGE + ".activities." + ActChangePassword.class.getSimpleName();
    public static final String IN_ActShare = BuildConfig.PACKAGE + ".activities." + ActShare.class.getSimpleName();
}