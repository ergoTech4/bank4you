package uk.bank4you.client.constants;

public final class ConstCardTypes {
    /* EXTRA DATA NAMES */
    public static final String CARD_TF_GBP = "05a29735-3766-4034-0001-000000000001";
    public static final String CARD_TF_EUR = "05a29735-3766-4034-0001-000000000002";
    public static final String CARD_ST_GBP = "05a29735-3766-4034-0002-000000000001";
    public static final String CARD_ST_EUR = "05a29735-3766-4034-0002-000000000002";

}