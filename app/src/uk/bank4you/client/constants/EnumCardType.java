package uk.bank4you.client.constants;

public enum EnumCardType {
    CARD_ST, CARD_TF
}
