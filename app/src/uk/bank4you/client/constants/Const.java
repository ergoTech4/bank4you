package uk.bank4you.client.constants;


import uk.bank4you.client.BuildConfig;

import java.text.SimpleDateFormat;

public final class Const {

    private Const() {}

    public static final String PATH_LOCAL = "/data/data/" + BuildConfig.PACKAGE +"/databases/";
    public static final String DB_NAME = "MB.sqlite";

  //  public static final String EMAIL_DEV = "drivelove@gmail.com";

    /* EVENTS */
    public static final String EV_INSTALL = "INSTALL";
    public static final String EV_UPDATE = "UPDATE";
    public static final String EV_SCREEN = "SCREEN ";
    public static final String EV_QUERY = "QUERY ";
    public static final String EV_ERR_CATCHED = "CATCHED ";
    public static final String EV_DIFFERENT = "DIFF: ";

    public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    public static final SimpleDateFormat simpleDateFormatNew = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
}