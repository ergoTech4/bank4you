package uk.bank4you.client.cursor_loaders;

import android.content.Context;
import android.database.Cursor;
import uk.bank4you.client.constants.ConstDb;
import uk.bank4you.client.utils.SimpleCursorLoader;

public class CursorLoaderTransactions extends SimpleCursorLoader {
    private String cardId;

    public CursorLoaderTransactions(Context context, String cardId) {
        super(context);
        this.cardId = cardId;
    }

    @Override
    public Cursor loadInBackground() {
        return db.query(ConstDb.tblTransactions, ConstDb.columnsTransactionsLight,
                "cardId = " + cardId, null, null, null, "id DESC");
    }
}