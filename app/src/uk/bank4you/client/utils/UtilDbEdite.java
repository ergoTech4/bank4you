package uk.bank4you.client.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import org.json.JSONArray;
import uk.bank4you.client.App;
import uk.bank4you.client.constants.Const;
import uk.bank4you.client.constants.ConstDb;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.multyfields.AnswerText;

public class UtilDbEdite {

    private static final String TAG = "UtilDbEdite";
    private SQLiteDatabase  db;
    private UtilLog         utilLog;
    private App             app;
    private Context         context;

    public UtilDbEdite(Context context) {
        utilLog     = new UtilLog(context);
        app         = (App) (context).getApplicationContext();
        db          = app.getDb();
        this.context = context;
    }

    // метод для создания строки для вставки в БД *******************************************
    private String createInsert(final String tableName, final String[] columnNames) {
        if (tableName == null || columnNames == null || columnNames.length == 0) {
            throw new IllegalArgumentException();
        }
        final StringBuilder s = new StringBuilder();
        s.append("INSERT OR IGNORE INTO ").append(tableName).append(" (");
        for (String column : columnNames) {
            s.append(column).append(" ,");
        }
        int length = s.length();
        s.delete(length - 2, length);
        s.append(") VALUES( ");
        for (int i = 0; i < columnNames.length; i++) {
            s.append(" ? ,");
        }
        length = s.length();
        s.delete(length - 2, length);
        s.append(")");
        return s.toString();
    }

    // запрос на удаление **********************************************************************

    public AnswerText deleteContentInTable(String tableName) {
        try {
            db.execSQL("DELETE FROM " + tableName);
        } catch (Exception e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "Чистка в БД таблицы " + tableName, e);
            return new AnswerText(ConstStatus.ST_FALSE, "", "Чистка в БД таблицы " + tableName);
        }
        return new AnswerText(ConstStatus.ST_TRUE, "", "");
    }

    // запрос на запись ************************************************************************
    public AnswerText writeCards(JSONArray cards) {
        String table = ConstDb.tblCards;
        String status = ConstStatus.ST_TRUE;
        String msgLog = "";
        deleteContentInTable(table);

        String INSERT_QUERY = createInsert(table, ConstDb.columnsCards);
        final SQLiteStatement statement = db.compileStatement(INSERT_QUERY);

        try {
            db.beginTransaction();
            for (int i = 0; i < cards.length(); i++) {
                statement.clearBindings();
                statement.bindString(1, cards.getJSONObject(i).getString("cardID"));
                statement.bindString(2, cards.getJSONObject(i).getString("cardNo"));
                statement.bindString(3, cards.getJSONObject(i).getString("cardCurrency"));
                statement.bindString(4, cards.getJSONObject(i).getString("availableBalance"));
                statement.bindString(5, cards.getJSONObject(i).getString("holdBalance"));
                statement.bindString(6, cards.getJSONObject(i).getString("effectiveDate"));
                statement.bindString(7, cards.getJSONObject(i).getString("expiryDate"));
                statement.bindString(8, cards.getJSONObject(i).getString("cardEmbossLine"));
                statement.execute();
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            status = ConstStatus.ST_FALSE;
            msgLog = "Запись в БД " + table;
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, msgLog, e);
        } finally {
            db.endTransaction();
        }

        return new AnswerText(status, "", msgLog);
    }

    public AnswerText writeTransactions(JSONArray transactions, String cardId) {
        String table = ConstDb.tblTransactions;
        String status = ConstStatus.ST_TRUE;
        String msgLog = "";
//        deleteContentInTable(table);

        String INSERT_QUERY = createInsert(table, ConstDb.columnsTransactions);
        final SQLiteStatement statement = db.compileStatement(INSERT_QUERY);


            db.beginTransaction();
            for (int i = 0; i < transactions.length(); i++) {
                try {
                    statement.clearBindings();
                    statement.bindString(1, transactions.getJSONObject(i).getString("category"));
                    statement.bindString(2, transactions.getJSONObject(i).getString("id"));
                    statement.bindString(3, transactions.getJSONObject(i).getString("dateTimeUtc"));
                    statement.bindString(4, transactions.getJSONObject(i).getString("amount"));
                    statement.bindString(5, transactions.getJSONObject(i).getString("currencyCode"));
                    statement.bindString(6, transactions.getJSONObject(i).getString("amountInCardCurrency"));
                    statement.bindString(7, transactions.getJSONObject(i).getString("additionalCommissionInCardCurrency"));
                    statement.bindString(8, transactions.getJSONObject(i).getString("info"));
                    statement.bindString(9, transactions.getJSONObject(i).getString("transactionType"));
                    statement.bindString(10, transactions.getJSONObject(i).getString("mcc"));
                    statement.bindString(11, transactions.getJSONObject(i).getString("comment"));
                    statement.bindString(12, cardId);
                    statement.bindString(13, transactions.getJSONObject(i).getString("imgUrl"));

//                    if ( i % 3 == 0 ) {
//                        statement.bindString(14, "1");
//                    } else {
//                        statement.bindString(14, "0");
//                    }
                    statement.execute();
                } catch (Exception ignored) {
                    Log.d(TAG, "sql error: "  + ignored);
                }
            }
            db.setTransactionSuccessful();


        db.endTransaction();
        return new AnswerText(status, "", msgLog);
    }
}