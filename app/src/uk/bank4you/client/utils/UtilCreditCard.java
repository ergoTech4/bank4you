package uk.bank4you.client.utils;
// Валидация Карточек **********************************************************************************************************************

import android.content.Context;
import android.widget.Toast;

public class UtilCreditCard {
    /** Подходит ли карточка под VISA, MasterCard ... */
    public int getCardNameId(String number) {
        int INVALID = -1;
        int VISA = 0;
        int MASTERCARD = 1;

        int valid = INVALID;
        String digit1 = number.substring(0, 1);
        String digit2 = number.substring(0, 2);

        if (digit1.equals("4")) valid = VISA;
        else if ((digit2.compareTo("51") >= 0 && digit2.compareTo("55") <= 0) || digit1.equals("6") ) valid = MASTERCARD;
        return valid;
    }
}
