package uk.bank4you.client.utils;

import android.content.Context;
import android.util.Log;
import uk.bank4you.client.App;
import uk.bank4you.client.BuildConfig;
import uk.bank4you.client.constants.Const;

public class UtilLog {
    private Context context;

    public UtilLog(Context context) {
        this.context = context;
    }

    public UtilLog() {}

    public void myLog(String msg) {
        if (BuildConfig.TEST_VER) {
            if (msg!=null && msg.length() > 3300) {
                Log.v(BuildConfig.TAG, msg.substring(0, 3300));
                myLog(msg.substring(3300));
            } else
                Log.v(BuildConfig.TAG, "" + msg);
        }
    }

    public void myLogExeption(Exception e) {
        myLog(e != null ? "" + Log.getStackTraceString(e) : "");
    }

    // все ошибки отправляем в ACRA
    public void myLogFlurry(final String eventName, final String msg, final Exception e) {
        final Util util = new Util(context);
        myLog(eventName + "\n" + msg + (e!=null ? "\n" + Log.getStackTraceString(e) : ""));

        final String location;
        location = util.getLocation();
        if (eventName.equals(Const.EV_INSTALL) || eventName.equals(Const.EV_ERR_CATCHED)) {
            (new Thread(new Runnable() {
                @Override
                public void run() {
                    App app = (App) context.getApplicationContext();
                    try { myLogAcra(eventName, msg, e, location, "", "");
                    } catch (Exception e) { e.printStackTrace(); }
                }
            })).start();
        }
    }

    // отправляем события в ACRA TODO
    private void myLogAcra(String eventName, String msg, Exception e, String location, String history, String lastNetworkType) {
        /*ACRA.getErrorReporter().putCustomData("EVENT", eventName);
        ACRA.getErrorReporter().putCustomData("MSG", msg);
        ACRA.getErrorReporter().putCustomData("METHOD", eventName.equals(Const.EV_INSTALL) ? "" : location);
        ACRA.getErrorReporter().putCustomData("HISTORY", history);
        ACRA.getErrorReporter().putCustomData("INET", lastNetworkType);
        ACRA.getErrorReporter().putCustomData("LOCALE", (eventName.equals(Const.EV_INSTALL) && context!=null) ? context.getResources().getConfiguration().locale.toString() : "");

        ACRA.getErrorReporter().handleSilentException(e!=null ? e : null);

        ACRA.getErrorReporter().removeCustomData("EVENT");
        ACRA.getErrorReporter().removeCustomData("MSG");
        ACRA.getErrorReporter().removeCustomData("METHOD");
        ACRA.getErrorReporter().removeCustomData("HISTORY");
        ACRA.getErrorReporter().removeCustomData("INET");
        ACRA.getErrorReporter().removeCustomData("LOCALE");*/
    }


}