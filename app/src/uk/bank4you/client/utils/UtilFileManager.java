package uk.bank4you.client.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.*;
import android.net.Uri;
import android.os.Environment;
import uk.bank4you.client.BuildConfig;
import uk.bank4you.client.constants.Const;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilFileManager {
    private UtilLog utilLog;
    private Context context;
    private AssetManager assetManager;

    public UtilFileManager(Context context) {
        this.context = context;
        utilLog = new UtilLog(this.context);
    }

    // копируем файл из ASSETS на внутреннюю память
    public void copyFile(String what) {
        File fileOnDevice = new File(Const.PATH_LOCAL + what);
        if(fileOnDevice.exists())
            return;

        if (assetManager==null)
            assetManager = context.getAssets();

        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(what);
            File file1 = new File(Const.PATH_LOCAL);
            if (!file1.exists()) file1.mkdirs();

            File file = new File(Const.PATH_LOCAL + what);


            out = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = in.read(buffer)) != -1)
                out.write(buffer, 0, length);
        } catch (IOException e) { utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "Не смог скопировать (1): " + what + " AND " + Const.PATH_LOCAL + what, e);
        } catch (Exception e) { utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "Не смог скопировать (2): " + what + " AND " + Const.PATH_LOCAL + what, e);
        } finally {
            try {
                if (out!=null) out.flush();
                if (out!=null) out.close();
                if (in!=null) in.close();
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "Не смог скопировать (3): " + what + " AND " + Const.PATH_LOCAL + what, e);}
        }
    }

    // удалить файл
    public void deleteFile(String fileName) {
        File fileOnDevice = new File(BuildConfig.PATH_LOCAL + fileName);
        if(fileOnDevice.exists())
            fileOnDevice.delete();
    }

    /* УДАЛИТЬ ВСЕ ФАЙЛЫ ИЗ ПАПКИ */
    public void deleteFilesFromFolder(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                new File(dir, children[i]).delete();
            }
        }
    }

    // запись текста String в файл
    public void appendLog(String text, String fullPath) {
        File logFile = new File(fullPath);
        if (!logFile.exists()) {
            try { logFile.createNewFile();
            } catch (IOException e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, fullPath + " 1", e);}
        } else {logFile.delete();}
        try {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, fullPath + " 2", e);}
    }

    /* РАБОТА С КАРТИНКАМИ ********************************************************************************************* */
    public boolean createImageCashFolder() {
        try {
            String dirPath = BuildConfig.PATH_LOCAL + BuildConfig.IMAGES_CASH_FOLDER;
            File dir = new File(dirPath);
            dir.mkdirs();
            deleteFilesFromFolder(dir);
        } catch (Exception e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "папка не создалась для фоток", e);
            return false;
        }
        return true;
    }

    public String getDirPathImageCashFolder() {
        String dirPath = BuildConfig.PATH_LOCAL + BuildConfig.IMAGES_CASH_FOLDER;
        return dirPath;
    }

    public boolean createImagePhotoFolder() {
        try {
            String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + BuildConfig.IMAGES_PHOTO_FOLDER;
            File dir = new File(dirPath);
            dir.mkdirs();
        } catch (Exception e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "папка не создалась для фоток", e);
            return false;
        }
        return true;
    }

    public String getDirPathImagePhotoFolder() {
        String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + BuildConfig.IMAGES_PHOTO_FOLDER;
        return dirPath;
    }

    /* СГЕНЕРИРОВАТЬ СТРОКУ НАЗВАНИЯ ФАЙЛА */
    public String createFileName() {
        long dateUnix = System.currentTimeMillis();
        Date date = new Date(dateUnix);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
        return sdf.format(date);
    }

    /* ПЕРЕСОХРАНИМ КАРТИНКУ */
    public void resizeImage(String imageFilePathFrom, String imageFilePathTo, Uri uri) {
        Bitmap bitmap = getBitmap(uri);

        BufferedOutputStream out = null;
        try {
            File file = new File(imageFilePathTo);
            file.createNewFile();
            out = new BufferedOutputStream(new FileOutputStream(file));

            String extension = imageFilePathFrom.substring(imageFilePathFrom.lastIndexOf(".") + 1);
            Bitmap.CompressFormat compressFormat;
            if (extension.toLowerCase().equals("png")){
                compressFormat = Bitmap.CompressFormat.PNG;
            } else if (extension.toLowerCase().equals("jpeg")){
                compressFormat = Bitmap.CompressFormat.JPEG;
            } else if (extension.toLowerCase().equals("jpg")) {
                compressFormat = Bitmap.CompressFormat.JPEG;
            } else {
                compressFormat = Bitmap.CompressFormat.valueOf(extension);
            }

            bitmap.compress(compressFormat, 100, out);

        } catch (Exception e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "Не смог сохранить из: " + imageFilePathFrom + " в: " + imageFilePathTo, e);
            e.printStackTrace();
        } finally {
            try {
                if (out!=null) out.flush();
                if (out!=null) out.close();
            } catch (Exception e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "Не смог сохранить из: " + imageFilePathFrom + " в: " + imageFilePathTo, e);}
        }
    }

    /* УМЕНЬШИТЬ РАЗМЕР ФАЙЛА */
    public Bitmap getBitmap(Uri uri) {
        ContentResolver contentResolver = context.getContentResolver();

        InputStream in;
        try {
            final int IMAGE_MAX_SIZE = 500000; // 1.0MP
            in = contentResolver.openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();

            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >IMAGE_MAX_SIZE) {
                scale++;
            }
            utilLog.myLog("scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap bitmap = null;
            in = contentResolver.openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                bitmap = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = bitmap.getHeight();
                int width = bitmap.getWidth();
                utilLog.myLog("1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) x, (int) y, true);
                bitmap.recycle();
                bitmap = scaledBitmap;

                System.gc();
            } else {
                bitmap = BitmapFactory.decodeStream(in);
            }
            in.close();

            utilLog.myLog("bitmap size - width: " + bitmap.getWidth() + ", height: " + bitmap.getHeight());
            return bitmap;
        } catch (IOException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при уменьшении картинки",e);
            return null;
        }
    }

    public String bitmapToBase64(String fileName) {
        String encodedString = null;
        try {
            InputStream inputStream = new FileInputStream(fileName);

            byte[] bytes;
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }

            bytes = output.toByteArray();
            encodedString = android.util.Base64.encodeToString(bytes, android.util.Base64.DEFAULT);
        } catch (IOException e) {
            utilLog.myLogExeption(e);
        }
        return encodedString;
    }

    public Bitmap getBitmapLite(String path) {
        ContentResolver contentResolver = context.getContentResolver();
        File file = new File(path);
        Uri uri = Uri.fromFile(file);

        InputStream in;
        try {
            final int IMAGE_MAX_SIZE = 10000; // 1.0MP
            in = contentResolver.openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();

            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >IMAGE_MAX_SIZE) {
                scale++;
            }
            utilLog.myLog("scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap bitmap = null;
            in = contentResolver.openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                bitmap = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = bitmap.getHeight();
                int width = bitmap.getWidth();
                utilLog.myLog("1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) x, (int) y, true);
                bitmap.recycle();
                bitmap = scaledBitmap;

                System.gc();
            } else {
                bitmap = BitmapFactory.decodeStream(in);
            }
            in.close();

            utilLog.myLog("bitmap size - width: " + bitmap.getWidth() + ", height: " + bitmap.getHeight());
            return bitmap;
        } catch (IOException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при уменьшении картинки",e);
            return null;
        }
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = 12;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }
}