package uk.bank4you.client.utils;

import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import uk.bank4you.client.BuildConfig;
import uk.bank4you.client.constants.Const;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

public class Util {
    private UtilLog utilLog;
    private Context context;
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    /* ДЛЯ ПОЛУЧЕНИЯ УНИКАЛЬНОГО ИДЕНТИФФИКАТОРА УСТАНОВКИ */
    private static String sID;
    private static final String INSTALLATION    = "ACRA-INSTALLATION";
    public static final String  ALGORITHM       = "SHA-256";

    public Util(Context context) {
        this.context = context;
        utilLog = new UtilLog(this.context);
    }

    /* ПОЛУЧЕНИЕ ВЕРСИИ ПРИЛОЖЕНИЯ */
    public int getVersion() {
        int v = 0;

        try {
            v = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) { utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "", e); }

        return v;
    }

    public String getDeviceModel() {
        return Build.MODEL;
    }

    public String getDeviceName() {
        String name = "noName";
        try {
            BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
            name = myDevice.getName();
        } catch (Exception ignored) {}
        return name;
    }



    public String getDeviceMaker() {
        String manufacturer = Build.MANUFACTURER;
        return capitalize(manufacturer);
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    /* ПОЛУЧЕНИЕ И ЗАПИСЬ ID */
    public synchronized static String id(Context context) {
        if (sID == null) {
            final File installation = new File(context.getFilesDir(), INSTALLATION);
            try {
                if (!installation.exists()) writeInstallationFile(installation);
                sID = readInstallationFile(installation);
            } catch (IOException e) {
                new UtilLog().myLogExeption(e);
                return "Couldn't retrieve InstallationId";
            } catch (RuntimeException e) {
                new UtilLog().myLogExeption(e);
                return "Couldn't retrieve InstallationId";
            }
        }
        return sID;
    }

    private static String readInstallationFile(File installation) throws IOException {
        final RandomAccessFile  f       = new RandomAccessFile(installation, "r");
        final byte[]            bytes   = new byte[(int) f.length()];

        try {
            f.readFully(bytes);
        } finally {
            f.close();
        }

        return new String(bytes);
    }

    private static void writeInstallationFile(File installation) throws IOException {
        final FileOutputStream out = new FileOutputStream(installation);
        try {
            final String id = UUID.randomUUID().toString();
            out.write(id.getBytes());
        } finally { out.close(); }
    }

    /* ОПРЕДЕЛЕНИЕ МЕСТОПОЛОЖЕНИЯ МЕТОДА */
    public String getLocation() {
        final String className = Util.class.getName();
        final StackTraceElement[] traces = Thread.currentThread().getStackTrace();
        boolean found = false;

        for (StackTraceElement trace : traces) {
            try {
                if (found) {
                    if (!trace.getClassName().startsWith(className)) {
                        Class<?> clazz = Class.forName(trace.getClassName());
                        return "[" + getClassName(clazz) + ":" + trace.getMethodName() + ":" + traces[4].getMethodName() + ":" + trace.getLineNumber() + "]: ";
                    }
                } else if (trace.getClassName().startsWith(className))
                    found = true;
            } catch (ClassNotFoundException e) {
                Log.v(BuildConfig.TAG, "getLocation() не сработал", e);
                return "[]: ";
            }
        }
        return "[]: ";
    }

    public String getClassName(Class<?> clazz) {
        if (clazz != null) {
            if (!TextUtils.isEmpty(clazz.getSimpleName())) return clazz.getSimpleName();
            return getClassName(clazz.getEnclosingClass());
        }
        return "";
    }

    private boolean checkForFilter(String fieldValue, String filter) {
        String filterItem[] = filter.split(",");
        try {
            for (String aFilterItem : filterItem)
                if (fieldValue.substring(0, aFilterItem.length()).equals(aFilterItem))
                    return true;
        } catch (NumberFormatException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "", e);
            return false;
        }
        return false;
    }

    public boolean isFloat(String value) {
        boolean isFloat = true;
        try {
            Float.parseFloat(value);
        } catch (Exception e) {
            isFloat = false;
        }
        return isFloat;
    }

    public String getAccountInfoPrint(String accountInfoDb, String accountInfoFromCheckPayment) {
        String res = accountInfoDb;
        if (accountInfoFromCheckPayment.substring(0, 13).equals("<AccountInfo>")) {
            String accountInfoLight = accountInfoFromCheckPayment.substring(13, accountInfoFromCheckPayment.length()-14);
            String accountInfoLightNew = accountInfoLight.replaceAll("(\\\\r\\\\n|\\\\n)", "");
            Map<String, String> accountInfoMap = parseXmlToHashMap(accountInfoLightNew);
            for (Map.Entry<String, String> entry : accountInfoMap.entrySet()) {
                String tagWithBrackets = "{" + entry.getKey() + "}";
                if (res.contains(tagWithBrackets)) res = res.replace(tagWithBrackets, entry.getValue());
            }
        }
        res = res.replace("|", "<br/>");
        return res;
    }

    public HashMap<String, String> parseXmlToHashMap(String xml) {
        XmlPullParserFactory factory;
        String tagName;
        String text = "";
        HashMap<String, String> hm = new HashMap<String, String>();

        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            StringReader sr = new StringReader(xml);
            xpp.setInput(sr);
            int eventType = xpp.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.TEXT) text = xpp.getText(); //Pulling out node text
                else if (eventType == XmlPullParser.END_TAG) {
                    tagName = xpp.getName();
                    hm.put(tagName, text);
                    text = ""; //Reset text for the next node
                }
                eventType = xpp.next();
            }
        } catch (XmlPullParserException e) { utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "parseXmlToHashMap", e);
        } catch (IOException e) { utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "parseXmlToHashMap", e);
        } catch (Exception e) { utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "parseXmlToHashMap", e);}
        return hm;
    }

    public void getKeyboard(final EditText edt) {
        edt.setText("", TextView.BufferType.EDITABLE);
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edt, InputMethodManager.SHOW_IMPLICIT);

        edt.postDelayed(new Runnable() {
            @Override
            public void run() {
                edt.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                edt.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
            }
        }, 100);
    }

    private String formateHint(String hint) {
        String formatedHint=hint;
        formatedHint = formatedHint.replace("|", "<br/>");
        while (formatedHint.contains("^")){
            formatedHint = formatedHint.replaceFirst(Pattern.quote("^"), "<font color=\"red\">");
            formatedHint = formatedHint.replaceFirst(Pattern.quote("^"), "</font>");
        }
        while (formatedHint.contains("*")){
            formatedHint = formatedHint.replaceFirst(Pattern.quote("*"), "<b>");
            formatedHint = formatedHint.replaceFirst(Pattern.quote("*"), "</b>");
        }
        return formatedHint;
    }



    /* ПОДСТРОЙКА ВЫСОТЫ СПИСКА */
    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) return;

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);

        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        if (listAdapter.getCount()>=1)
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() + 1)) - (listView.getDividerHeight()*2) + 1;
        else params.height = totalHeight + 1;

        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    /* UNESCAPE ДЛЯ ВСТАВКИ В БД */
    public String unescape(String textForChanging) {
        if (textForChanging == null) return "";
        else return textForChanging.replaceAll("\\\\n", "\\\n");
    }

    public String stringNewLineToSpace(String textForChanging) {
        if (textForChanging == null) return "";
        else return textForChanging.replaceAll("\\\\n", " ");
    }

    public void myToast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public void myToastLong(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    /* КОНВЕРТЕРЫ */
    public int convertStringToInt(String string) {
        int resultInt = 0;

        if (string == null) {
            return resultInt;
        }

        try {
            resultInt = Integer.parseInt(string);
        } catch(NumberFormatException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "Не преобразовалась к числу строка: " + string, e);
        }
        return resultInt;
    }

    public double convertStringToDouble(String string) {
        double resultDouble = 0;
        if (string != null && !string.equals("") && !string.equals("0")) {
            try {
                resultDouble = Double.parseDouble(string); // Make use of autoboxing.  It's also easier to read.
            } catch (NumberFormatException e) {
                resultDouble = 0;
            }
        }
        return resultDouble;
    }


    /* ПРОВЕРКА НАЛИЧИЯ КАМЕРЫ */
    public boolean checkCameraExists() {
        PackageManager pm = context.getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    public boolean isMyServiceDownloadRunning() {
        ActivityManager manager = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ((BuildConfig.PACKAGE + ".services.ServiceDownload").equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    //SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(password))
    public byte[] getPassHash(String pass) {
        try {
            byte[] passwordBytes = pass.getBytes("UTF-8");
            byte[] res = getHashFromBytes(passwordBytes);
            return res;
        } catch (IOException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "", e);
        }
        return null;
    }

    // получение из строки String SHA-256
    private byte[] getHashFromString(String data) {
        MessageDigest digest = null;
        try { digest = MessageDigest.getInstance(ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            if (digest!=null) digest.reset();
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "", e);
            return null;
        }
        return digest.digest(data.getBytes());
    }

    // получение из массива байт SHA-256
    private byte[] getHashFromBytes(byte[] data) {
        MessageDigest digest = null;
        try { digest = MessageDigest.getInstance(ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            if (digest!=null) digest.reset();
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "", e);
            return null;
        }
        return digest.digest(data);
    }

    public static String encodeURL(String input) {
        StringBuilder resultStr = new StringBuilder();
        for (char ch : input.toCharArray()) {
            if (isUnsafe(ch)) {
                resultStr.append('%');
                resultStr.append(toHex(ch / 16));
                resultStr.append(toHex(ch % 16));
            } else {
                resultStr.append(ch);
            }
        }
        return resultStr.toString();
    }

    private static char toHex(int ch) {
        return (char) (ch < 10 ? '0' + ch : 'A' + ch - 10);
    }

    private static boolean isUnsafe(char ch) {
        if (ch > 128 || ch < 0)
            return true;
        return " %$&+,/:;=?@<>#%".indexOf(ch) >= 0;
    }

    public String createCurrentDateFormated() {
        long dateUnix = System.currentTimeMillis();
        Date date = new Date(dateUnix);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        return sdf.format(date);
    }

    public String getTimeFormated(long unixtime) {
        Date date = new Date(unixtime);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return format.format(date);
    }

    public String convertDateToString(String date) {
        if (date.equals(""))
            return "";

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date dates = null;
        try {
            dates = formatter.parse(date);
        } catch (ParseException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "Ошибка при преобразовании даты", e);
            return "";
        }

        SimpleDateFormat format = new SimpleDateFormat("HH:mm dd/MM/yyyy");
        return format.format(dates);

    }

    public String convertDateToStringSeccond(String date) {
        if (date.equals(""))
            return "";

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date dates = null;
        try {
            dates = formatter.parse(date);
        } catch (ParseException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "Ошибка при преобразовании даты", e);
            return "";
        }

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy, HH:mm");
        return format.format(dates);

    }

    public long convertDateToLong(String date) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date dates;
        long result = 0;
        try {
            dates = formatter.parse(date);
            result = dates.getTime();
        } catch (ParseException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "Ошибка при преобразовании даты", e);
        }
        return result;
    }

    public void hideKeyBoard(IBinder iBinder) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(iBinder, 0);
        } catch (Exception e) {}
    }

    public boolean call(String tel) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + tel));
            context.startActivity(intent);
            return true;
        } catch (Exception e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "Звонить не может", e);
            return false;
        }
    }

    public boolean sendSMS(String tel) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("sms:" + tel));
            context.startActivity(intent);
            return true;
        } catch (Exception e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "СМС отправлять не может", e);
            return false;
        }
    }

    public void removeItemFromArrayList(List<String> list, String removeItemValue) {
        for (int i=0; i<list.size(); i++) {
            String val = list.get(i);
            if (val.equals(removeItemValue)) {
                list.remove(i);
                break;
            }
        }
    }

    public String getStringWithoutStringAtTheEnd(String result) {
        if (result.length() > 0 && result.charAt(result.length() - 1)==',')
            result = result.substring(0, result.length() - 1);
        return result;
    }

    public double getLatFromString(String latLng) {
        double lat = 0;

        if (latLng.length()>0 && latLng.contains(",")) {
            String latString = latLng.substring(0, latLng.indexOf(","));
            lat = convertStringToDouble(latString);
        }

        return lat;
    }

    public double getLngFromString(String latLng) {
        double lng = 0;

        if (latLng.length()>0 && latLng.contains(",")) {
            String lngString = latLng.substring(latLng.indexOf(",")+1, latLng.length());
            lng = convertStringToDouble(lngString);
        }

        return lng;
    }

    public String getDateInput() {
        //"2014-07-31T12:52:24.1970365+03:00"
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        return simpleDateFormat.format(System.currentTimeMillis()) + "0000+03:00";
    }

    public String getSignTimeStamp() {
        //"2014-07-31T12:52:24.202"
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        return simpleDateFormat.format(System.currentTimeMillis());
    }

    public float convertStringToFloat(String str) {
        float res = 0;
        try {
            if (str != null) {
                str.replace(",", ".");
                res = Float.valueOf(str.trim()).floatValue();
            }
        } catch(Exception e) {}
        return res;
    }

    public boolean isWhole(BigDecimal bigDecimal) {
        return bigDecimal.toPlainString().matches("-?+\\d+(\\.0*)?");
    }

    public BigDecimal getBigDecimalFromString(String value) {
        BigDecimal bigDecimal = new BigDecimal("0");
        if (!isEmpty(value) && !value.equals("."))
            bigDecimal = bigDecimal.add(new BigDecimal(value));
        return bigDecimal;
    }

    public boolean isBigDecimalNegative(BigDecimal bd) {
        return bd.signum() == -1;
    }



    public boolean isEmpty(String text) {
        return text == null || text.equals("");
    }

    public int getDaysBetweenDates(Date dateOld, Date dateNew){
        return (int)((dateNew.getTime() - dateOld.getTime()) / (1000*60*60*24l));
    }



    public ArrayList<Integer> getAllOccurrencesInString(String word, String guess) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int index = word.indexOf(guess); index >= 0; index = word.indexOf(guess, index + 1))
            list.add(index);
        return list;
    }

    public String getFormatedDate(String dateIn) {
        String dateOut = "дата не визначена";
        SimpleDateFormat simpleDateFormatIn = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat simpleDateFormatOut = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = simpleDateFormatIn.parse(dateIn);
            dateOut = simpleDateFormatOut.format(date);
        } catch (ParseException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге даты", e);
        }

        return dateOut;
    }

    public Date getDateFromString(String dateIn, String format) {
        Date date = new Date();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(dateIn);
        } catch (ParseException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при парсинге даты", e);
        }

        return date;
    }

    public String getCharsetList() {
        SortedMap m = Charset.availableCharsets();
        Set k = m.keySet();
        Iterator i = k.iterator();
        StringBuffer sb = new StringBuffer();
        sb.append("Canonical name, Display name," + " Can encode, Aliases");
        while (i.hasNext()) {
            String n = (String) i.next();
            Charset e = (Charset) m.get(n);
            String d = e.displayName();
            boolean c = e.canEncode();
            sb.append(n + ", " + d + ", " + c);
            Set s = e.aliases();
            Iterator j = s.iterator();
            while (j.hasNext()) {
                String a = (String) j.next();
                sb.append(", " + a);
                System.out.print(", "+a);
            }
            sb.append("");
        }
        return sb.toString();
    }

    public static int generateViewId() {
        for (;;) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

    public boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}