package uk.bank4you.client.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import uk.bank4you.client.App;
import uk.bank4you.client.BuildConfig;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.Const;
import uk.bank4you.client.constants.ConstStatus;
import uk.bank4you.client.multyfields.AnswerConnection;
import uk.bank4you.client.multyfields.AnswerJSONObject;
import org.json.JSONException;
import org.json.JSONObject;
import uk.bank4you.client.response.ReqMain;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

public class UtilWeb {
    private UtilPrefCustom  utilPrefCustom;
    private UtilLog         utilLog;
    private Context         context;
    private final App       app;
    private Util            util;

    public UtilWeb(Context context) {
        this.context = context;
        app = (App) (this.context).getApplicationContext();
        utilLog = new UtilLog(this.context);
        utilPrefCustom = new UtilPrefCustom(this.context);
        util = new Util(this.context);
    }

    public AnswerJSONObject getJSONFromHttpUrlConnection(String url, String fileName, String req, boolean isPost) {

        AnswerConnection answerConnection = getResponseHttpUrlConnection(url, fileName, req, isPost);
        if (!answerConnection.status.equals(ConstStatus.ST_TRUE))
            return new AnswerJSONObject(answerConnection.status, null, answerConnection.log);

        InputStream in = null;
        try {
            if (answerConnection.connection.getResponseCode() == 200 )
                in = answerConnection.connection.getInputStream();
            else if (answerConnection.connection.getResponseCode() == 406 ||
                    answerConnection.connection.getResponseCode() == 401)
                in = answerConnection.connection.getErrorStream();
        } catch (IOException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "получение InputStream'a 1", e);
            try { if ( in != null ) in.close();
            } catch(IOException ex) { utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "получение InputStream'a 2", e);}
            return new AnswerJSONObject(ConstStatus.ST_FALSE, null, "Ошибка при получении readder'a из respons'a");
        }

        boolean isGzip = false;
        String contentEncoding = answerConnection.connection.getHeaderField("Content-Encoding");
        if (contentEncoding != null && contentEncoding.equalsIgnoreCase("gzip"))
            isGzip = true;

        AnswerJSONObject answerJSONObject = getJSONFromInputStream(in, BuildConfig.TEST_VER, isGzip);
        if (!answerJSONObject.status.equals(ConstStatus.ST_TRUE))
            return new AnswerJSONObject(answerJSONObject.status, null, answerJSONObject.log);

        return answerJSONObject;
    }

    private AnswerConnection getResponseHttpUrlConnection(String urlString, String fileName, String req, boolean isPost) {
        if (!fileName.equals(util.getClassName(ReqMain.ReqSignUpPhoto.class) + ".json"))
            utilLog.myLog("Запрос:" + "\n" + urlString + "\n" + req);

        if (!checkInternetConnection())
            return new AnswerConnection(ConstStatus.ST_NOINTERNET, null, "нет интернета");

        disableConnectionReuseIfNecessary();

        HttpURLConnection connection = null;
        int statusCode = 0;
        try {
            URL url = new URL(urlString);

            connection = ( url.getProtocol().toLowerCase().equals("https") ? (HttpsURLConnection) url.openConnection() : (HttpURLConnection) url.openConnection());
            connection.setConnectTimeout(25000);
            connection.setReadTimeout(35000);

            connection.setRequestMethod(isPost ? "POST" : "GET");
            connection.setUseCaches(false);
            connection.setAllowUserInteraction(false);
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("Accept-Encoding", "gzip");
            connection.setRequestProperty("Accept", "*/*");
            connection.setRequestProperty("Cookie", utilPrefCustom.getCookie());

            // perform POST operation
            if (isPost) {
                connection.setDoOutput(true);
                connection.setDoInput(true);
                DataOutputStream dataout = new DataOutputStream(connection.getOutputStream());
                dataout.write(req.getBytes("UTF-8"));
            }

            statusCode = connection.getResponseCode();

            // Getting the cookies from a URL connection
            String headerName;
            for (int i = 1; (headerName = connection.getHeaderFieldKey(i)) != null; i++) {
                if (headerName.toLowerCase().equals("asp.net_sessionid")) {
                    utilPrefCustom.setCookie(connection.getHeaderField(i));
                }
            }
        } catch (ProtocolException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка в протоколе запроса", e);
            disconnectConnection(connection);
            return new AnswerConnection(ConstStatus.ST_FALSE, null, "ошибка в протоколе запроса");
        } catch (SocketTimeoutException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка в SocketTimeout", e);
            disconnectConnection(connection);
            return new AnswerConnection(ConstStatus.ST_CONNECTION_ABSENT, null, "ошибка в SocketTimeout");
        } catch (IOException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "Страница недоступна - " + fileName, e);
            disconnectConnection(connection);
            return new AnswerConnection(ConstStatus.ST_CONNECTION_ABSENT, null, "Страница недоступна - " + fileName);
        } finally {  }

        if (statusCode != 200 && statusCode != 406 && statusCode != 401){
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "Status: " + Integer.toString(statusCode), null);
            disconnectConnection(connection);
            return new AnswerConnection(ConstStatus.ST_MAKE_LATER, null, ConstStatus.ST_MAKE_LATER);
        } else return new AnswerConnection(ConstStatus.ST_TRUE, connection, req + " Status: " + Integer.toString(statusCode));
    }

    private boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected()) return true;
        else utilLog.myLog(context.getString(R.string.STATUS_INTERNET_ABSENT_MSG));
        return false;
    }

    private static void disableConnectionReuseIfNecessary() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO)
            System.setProperty("http.keepAlive", "false");
    }

    private void disconnectConnection(HttpURLConnection connection) {
        if(connection != null) connection.disconnect();
    }

    private AnswerJSONObject getJSONFromInputStream(InputStream in, boolean log, boolean isGzip) {
        JSONObject jsonObject = null;
        String txtResponse = null;
        Reader reader = null;
        try {
            if (isGzip) {
                reader = new InputStreamReader(new GZIPInputStream(in), "UTF-8");
            } else reader = new InputStreamReader(in);
        } catch (Exception ex) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "получение Reader'a 1", ex);
            try {
                if (reader != null) reader.close();
            } catch (IOException e) {
                utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "получение Reader'a 2", e);
            }
            return new AnswerJSONObject(ConstStatus.ST_FALSE, null, "Ошибка при получении readder'a из respons'a");
        }

        try {
            BufferedReader bufReader = new BufferedReader(reader);
            StringBuilder str = new StringBuilder();
            String line;
            while ((line = bufReader.readLine()) != null)
                str.append(line);
            txtResponse = str.toString();

            if (log) utilLog.myLog("Ответ:" + "\n" + txtResponse);

            jsonObject = new JSONObject(txtResponse);
        } catch (IOException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "получение Reader'a 3", e);
        } catch (JSONException e) {
            utilLog.myLogFlurry(Const.EV_ERR_CATCHED, "ошибка при получении JSON объекта", e);
            return new AnswerJSONObject(ConstStatus.ST_FALSE, null, "Ошибка при получении JSON объекта");
        }

        return new AnswerJSONObject(ConstStatus.ST_TRUE, jsonObject, "response переведён в reader");
    }

}