package uk.bank4you.client.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import uk.bank4you.client.constants.ConstPref;

public class UtilPrefCustom {
    private SharedPreferences settingsCustom;

    private static final String userId      = "userId";
    private static final String clientId    = "phoneNumber";
    private static final String cookie      = "cookie";
    private static final String sessionToken = "sessionToken";
    private static final String authToken   = "authToken";
    private static final String phoneNumber = "phoneNumber";
    private static final String token       = "token";

    private String moneyPoloLink    = "moneyPoloLink";
    private String formData         = "formData";
    private String successLink      = "successLink";
    private String failureLink      = "failureLink";
    private String cardID           = "cardID";
    private String cardAvailable    = "cardID";
    private String cardSuccesLink   = "cardSuccesLink";
    private String stealContactTime = "stealContactTime";

    private static final String pref_is_first = "is_first"; // определяем когда происходит обновление приложения
    private static final String clientNeedsToSpecifyPassword        = "clientNeedsToSpecifyPassword";
    private static final String clientNeedsToSpecifyEmail           = "clientNeedsToSpecifyEmail";
    private static final String clientNeedsToPayForTheCard          = "clientNeedsToPayForTheCard";
    private static final String clientNeedsToProvidePassportPic     = "clientNeedsToProvidePassportPic";

    private static final String clientNeedsToProvideUtilityBillPic  = "clientNeedsToProvideUtilityBillPic";
    private static final String initiallySelectedCardId             = "initiallySelectedCardId";
    private static final String systemMessage                       = "systemMessage";

    public UtilPrefCustom(Context context) {
        settingsCustom = context.getSharedPreferences(ConstPref.PREF_FILE_NAME_CUSTOM, context.MODE_PRIVATE);
    }

    public String getUserId() {
        return settingsCustom.getString(userId, "");
    }

    public void setUserId(String userId) {
        settingsCustom.edit().putString(this.userId, userId).apply();
    }

    public long getIsFirst() {
        return settingsCustom.getLong(pref_is_first, 0);
    }

    public void setIsFirst(long verCode) {
        settingsCustom.edit().putLong(pref_is_first, verCode).apply();
    }


    public String getInitiallySelectedCardId() {
        return "";
    }

    public void setInitiallySelectedCardId(String initiallySelectedCardId) {
        settingsCustom.edit().putString(this.initiallySelectedCardId, initiallySelectedCardId).commit();
    }

    public String getToken() {
        return settingsCustom.getString(token, "");
    }

    public void setToken(String token) {
        settingsCustom.edit().putString(this.token, token).apply();
    }


    public String getClientId() {
        return settingsCustom.getString(clientId, "");
    }

    public void setClientId(String clientId) {
        settingsCustom.edit().putString(this.clientId, clientId).apply();
    }

    public void setCookie(String cookie) {
        settingsCustom.edit().putString(this.cookie, cookie).apply();
    }

    public String getCookie() {
        return settingsCustom.getString(cookie, "");
    }

    public void setSessionToken(String sessionToken) {
        settingsCustom.edit().putString(this.sessionToken, sessionToken).apply();
    }

    public String getSessionToken() {
        return settingsCustom.getString(sessionToken, "");
    }

    public void setAuthToken(String authToken) {
        settingsCustom.edit().putString(this.authToken, authToken).apply();
    }

    public String getAuthToken() {
        return settingsCustom.getString(authToken, "");
    }

    public void setPhoneNumber(String phoneNumber) {
        settingsCustom.edit().putString(this.phoneNumber, phoneNumber).apply();
    }

    public String getPhoneNumber() {
        return settingsCustom.getString(phoneNumber, "");
    }



    public void setClientNeedsToSpecifyPassword(boolean clientNeedsToSpecifyPassword) {
        settingsCustom.edit().putBoolean(this.clientNeedsToSpecifyPassword, clientNeedsToSpecifyPassword).apply();
    }

    public boolean getClientNeedsToSpecifyPassword() {
        return settingsCustom.getBoolean(clientNeedsToSpecifyPassword, true);
    }

    public void setclientNeedsToSpecifyEmail(boolean clientNeedsToSpecifyEmail) {
        settingsCustom.edit().putBoolean(this.clientNeedsToSpecifyEmail, clientNeedsToSpecifyEmail).apply();
    }

    public boolean getclientNeedsToSpecifyEmail() {
        return settingsCustom.getBoolean(clientNeedsToSpecifyEmail, true);
    }

    public void setClientNeedsToPayForTheCard(boolean clientNeedsToPayForTheCard) {
        settingsCustom.edit().putBoolean(this.clientNeedsToPayForTheCard, clientNeedsToPayForTheCard).apply();
    }

    public boolean getClientNeedsToPayForTheCard() {
        return settingsCustom.getBoolean(clientNeedsToPayForTheCard, true);
    }

    public void setclientNeedsToProvidePassportPic(boolean clientNeedsToProvidePassportPic) {
        settingsCustom.edit().putBoolean(this.clientNeedsToProvidePassportPic, clientNeedsToProvidePassportPic).apply();
    }

    public boolean getclientNeedsToProvidePassportPic() {
        return settingsCustom.getBoolean(clientNeedsToProvidePassportPic, true);
    }

    public void setclientNeedsToProvideUtilityBillPic(boolean clientNeedsToProvideUtilityBillPic) {
        settingsCustom.edit().putBoolean(this.clientNeedsToProvideUtilityBillPic, clientNeedsToProvideUtilityBillPic).apply();
    }

    public boolean getclientNeedsToProvideUtilityBillPic() {
        return settingsCustom.getBoolean(clientNeedsToProvideUtilityBillPic, true);
    }

    public void setMoneyPoloData(String moneyPoloLink, String formData, String successLink, String failureLink) {
        settingsCustom.edit().putString(this.moneyPoloLink, moneyPoloLink).apply();
        settingsCustom.edit().putString(this.formData, formData).apply();
        settingsCustom.edit().putString(this.successLink, successLink).apply();
        settingsCustom.edit().putString(this.failureLink, failureLink).apply();
    }

    public String getMoneyPoloLink() {
        return settingsCustom.getString(moneyPoloLink, "");
    }

    public String getFormData() {
        return settingsCustom.getString(formData, "");
    }

    public String getSuccessLink() {
        return settingsCustom.getString(successLink, "");
    }

    public String getFailureLink() {
        return settingsCustom.getString(failureLink, "");
    }

    public String getCardID() {
        return settingsCustom.getString(cardID, "05a29735-3766-4034-0001-000000000001");
    }

    public void setCardID(String cardID) {
        settingsCustom.edit().putString(this.cardID, cardID).apply();
    }

    public void setCardAvailable(String cardAvailable) {
        settingsCustom.edit().putString(this.cardAvailable, cardAvailable).apply();
    }

    public String getCardAvailable() {
        return settingsCustom.getString(cardAvailable, "");
    }

    public void setCardSuccesLink(String cardSuccesLink) {
        settingsCustom.edit().putString(this.cardSuccesLink, cardSuccesLink).apply();
    }

    public String getCardSuccesLink() {
        return settingsCustom.getString(cardSuccesLink, "");
    }


    public void setTimeStampContactSteal(long timeStampContactSteal) {
        settingsCustom.edit().putLong(this.stealContactTime, timeStampContactSteal).apply();
    }

    public Long getStealContactTime() {
        return settingsCustom.getLong(stealContactTime, 0);
    }

    public void saveSystemMessages(String text) {
        settingsCustom.edit().putString(systemMessage, text).apply();
    }

    public String getSystemMessage() {
        return settingsCustom.getString(systemMessage, "{\"systemMessages\":[],\"promoCode\":\"mqja02\",\"andriodAppUrl\":\"https:\\/\\/play.google.com\\/store\\/apps\\/details?id=uk.bank4you.client\",\"iosAppUrl\":\"https:\\/\\/itunes.apple.com\\/ua\\/app\\/bank4you\\/id1069036395\",\"error\":{\"code\":0,\"isTransient\":false,\"message\":null}}\n");
    }
}