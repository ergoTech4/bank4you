package uk.bank4you.client.response;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import java.io.Serializable;
import java.util.ArrayList;

import uk.bank4you.client.App;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstBC;
import uk.bank4you.client.multyfields.Contact;

public class ReqMain {

    public abstract static class ReqBasic implements Serializable {
        public int      event;
        public String   msg = "Downloading";

        protected ReqBasic() {
            setEvent();
            try {
                setMsg();
            } catch (Resources.NotFoundException e) {}
        }

        protected abstract void setEvent();
        protected abstract void setMsg();
        public abstract String getReq();
    }

    public static class ReqSignUpPhone extends ReqBasic {
        public String phoneNumber;
        public String installationIdentifier;
        public String urlPart = "api/register";

        public ReqSignUpPhone(String phoneNumber, String installationIdentifier) {
            this.phoneNumber            = phoneNumber;
            this.installationIdentifier = installationIdentifier;
        }


        @Override
        protected void setMsg() {
//            super.msg = "Registration. Phone sending";




            super.msg = App.getContext().getResources().getString(R.string.registration_phone);

        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_SIGNUP_PHONE;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"installationIdentifier\": \""+ installationIdentifier +"\",\n" +
                    "  \"phoneNumber\": \""+ phoneNumber +"\"\n" +
                    "}";
        }
    }

    public static class ReqStarter extends ReqBasic {
        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_GET_STARTED;
        }

        @Override
        protected void setMsg() {
//            super.msg = "Downloading data";


            super.msg = App.getContext().getResources().getString(R.string.download_data);
        }

        @Override
        public String getReq() {
            return null;
        }
    }

    public static class ReqSignInPhone extends ReqBasic {
        public String installationIdentifier;
        public String phoneNumber;
        public String password;
        public String urlPart = "api/getAuthToken";

        public ReqSignInPhone(String installationIdentifier, String phoneNumber, String password) {
            this.installationIdentifier = installationIdentifier;
            this.phoneNumber            = phoneNumber;
            this.password               = password;
        }

        @Override
        protected void setMsg() {
//            super.msg = "Authorization";

            super.msg = App.getContext().getResources().getString(R.string.authorization);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_SIGNIN_PHONE;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"phoneNumber\": \"" + phoneNumber + "\",\n" +
                    "  \"password\": \"" + password + "\",\n" +
                    "  \"installationIdentifier\": \""+ installationIdentifier +"\"\n" +
                    "}";
        }
    }

    public static class ReqGetAuthToken extends ReqBasic {
        public String installationIdentifier;
        public String phoneNumber;
        public String password;
        public String urlPart = "api/getAuthToken";

        public ReqGetAuthToken(String installationIdentifier, String phoneNumber,
                               String password) {
            this.installationIdentifier = installationIdentifier;
            this.phoneNumber            = phoneNumber;
            this.password               = password;
        }

        @Override
        protected void setMsg() {
//            super.msg = "Authorization";
            super.msg = App.getContext().getResources().getString(R.string.authorization);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_GET_AUTH_TOKEN;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"phoneNumber\": \"" + phoneNumber + "\",\n" +
                    "  \"password\": \"" + password + "\",\n" +
                    "  \"installationIdentifier\": \""+ installationIdentifier +"\"\n" +
                    "}";
        }
    }

    public static class ReqSignUpPin extends ReqBasic {
        public String pinCode;
        public String installationIdentifier;
        public String phoneNumber;
        public String urlPart = "api/register";

        public ReqSignUpPin(String pinCode, String installationIdentifier, String phoneNumber) {
            this.pinCode = pinCode;
            this.installationIdentifier = installationIdentifier;
            this.phoneNumber = phoneNumber;
        }

        @Override
        protected void setMsg() {
//            super.msg = "Registration. PIN code sending";

            super.msg = App.getContext().getResources().getString(R.string.registration_pin);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_SIGNUP_PIN;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"installationIdentifier\": \""+ installationIdentifier +"\",\n" +
                    "  \"phoneNumber\": \""+ phoneNumber +"\",\n" +
                    "  \"smsPin\": \""+ pinCode +"\"\n" +
                    "}";
        }
    }

    public static class ReqSignUpPhoto extends ReqBasic {
        public String installationIdentifier;
        public String sessionToken;
        public int    photoType;
        public String urlPart = "api/register";

        public ReqSignUpPhoto(String installationIdentifier, String sessionToken, int photoType) {
            this.installationIdentifier = installationIdentifier;
            this.sessionToken           = sessionToken;
            this.photoType              = photoType;
        }

        @Override
        protected void setMsg() {
//            super.msg = "Registration. Photo sending";

            super.msg = App.getContext().getResources().getString(R.string.registration_photo);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_SIGNUP_PHOTO;
        }

        @Override
        public String getReq() {
            return "";
        }
    }

    public static class ReqSignUpPass extends ReqBasic {
        public String installationIdentifier;
        public String sessionToken;
        public String pass;
        public String email;
        public String promoCode;
        public String urlPart = "api/register";

        public ReqSignUpPass(String installationIdentifier, String sessionToken,
                              String pass, String email, String promoCode) {

            this.installationIdentifier = installationIdentifier;
            this.sessionToken           = sessionToken;
            this.pass                   = pass;
            this.email                  = email;
            this.promoCode              = promoCode;
        }

        @Override
        protected void setMsg() {
//            super.msg = "Registration. Email and pass sending";

            super.msg = App.getContext().getResources().getString(R.string.registration_email);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_SIGNUP_PASS;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"installationIdentifier\": \""+ installationIdentifier +"\",\n" +
                    "  \"sessionToken\": \""+ sessionToken +"\",\n" +
                    "  \"password\": \"" + pass +"\",\n" +
                    "  \"promoCode\": \"" + promoCode +"\",\n" +
                    "  \"email\": \"" + email +"\"\n" +
                    "}";
        }
    }

    public static class ReqSignUpCardId extends ReqBasic {
        public String installationIdentifier;
        public String sessionToken;
        public String initialCardId;
        public String urlPart = "api/register";

        public ReqSignUpCardId(String installationIdentifier, String sessionToken,
                             String initialCardId) {
            this.installationIdentifier = installationIdentifier;
            this.sessionToken = sessionToken;
            this.initialCardId = initialCardId;
        }

        @Override
        protected void setMsg() {
//            super.msg = "Registration. Card type sending";

            super.msg = App.getContext().getResources().getString(R.string.registration_card_type);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_SIGNUP_CARD_ID;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"installationIdentifier\": \""+ installationIdentifier +"\",\n" +
                    "  \"sessionToken\": \""+ sessionToken +"\",\n" +
                    "  \"initialCardId\": \"" + initialCardId +"\"\n" +
                    "}";
        }
    }

    public static class ReqHello extends ReqBasic {
        public String installationIdentifier;
        public String protocolVersion;
        public String appBuildNumber;
        public String deviceString;
        public String language;

        public String maker;
        public String model;
        public String platformName;
        public String platformVersion;

        public String urlPart = "api/hello";

        public ReqHello(String installationIdentifier, String protocolVersion,
                                String appBuildNumber, String deviceString, String language,

                                String maker, String model,
                                String platformName, String platformVersion) {

            this.installationIdentifier = installationIdentifier;
            this.protocolVersion        = protocolVersion;
            this.appBuildNumber         = appBuildNumber;
            this.language               = language;

            this.deviceString           = deviceString;
            this.maker                  = maker;
            this.model                  = model;
            this.platformName           = platformName;
            this.platformVersion        = platformVersion;
        }

        @Override
        protected void setMsg() {
            super.msg = "Hello";
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_HELLO;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"protocolVersion\": " + protocolVersion + ",\n" +
                    "  \"appBuildNumber\": " + appBuildNumber + ",\n" +
                    "  \"installationIdentifier\": \""+ installationIdentifier +"\",\n" +
                    "  \"deviceString\": \""+ deviceString +"\",\n" +
                    "  \"maker\": \""+ maker +"\",\n" +
                    "  \"model\": \""+ model +"\",\n" +
                    "  \"platformName\": \""+ platformName +"\",\n" +
                    "  \"platformVersion\": \""+ platformVersion +"\",\n" +
                    "  \"language\": \"" + language +"\"\n" +
                    "}";
        }


//        "protocolVersion": 3,
//                "appBuildNumber": 1,
//                "installationIdentifier": "4EE7221A-3BC2-426A-AC47-4F40D256A78C",
//                "deviceString": "My Lenovo phone",
//                "maker": "Lenovo",
//                "model": "Lenovo S850",
//                "platformName": "Android",
//                "platformVersion": "4.4.2",
//                "language": "en-US"


    }

    public static class ReqGetCardList extends ReqBasic {
        public String authToken;
        public String urlPart = "api/getCardList";

        public ReqGetCardList(String authToken) {

            this.authToken = authToken;
        }

        @Override
        protected void setMsg() {
//            super.msg = "Getting of card list";
            super.msg = App.getContext().getResources().getString(R.string.getting_cards);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_GET_CARD_LIST;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"authToken\": \""+ authToken +"\"\n" +
                    "}";
        }
    }

    public static class ReqGetCardsAvailableAtRegistrationPhase extends ReqBasic {

        public String urlPart = "api/cardsAvailableAtRegistrationPhase";

        public ReqGetCardsAvailableAtRegistrationPhase() {

        }

        @Override
        protected void setMsg() {
//            super.msg = "Getting of available card list ";

            super.msg = App.getContext().getResources().getString(R.string.getting_cards);

        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_GET_CARD_LIST_AVAILABLE_IN_REG;
        }

        @Override
        public String getReq() {
            return "";
        }
    }

    public static class ReqGetTransactionList extends ReqBasic {
        public String authToken;
        public String cardId;
        public String laterThanUTC;
        public String beforeThanUTC;
        public String urlPart = "api/getTransactionList";

        public ReqGetTransactionList(String authToken, String cardId,
                               String laterThanUTC, String beforeThanUTC) {
            this.authToken = authToken;
            this.cardId = cardId;
            this.laterThanUTC = laterThanUTC;
            this.beforeThanUTC = beforeThanUTC;
        }

        @Override
        protected void setMsg() {
//            super.msg = "Getting list of transactions";

            super.msg = App.getContext().getResources().getString(R.string.getting_transactions);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_GET_TRANSACTION_LIST;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"authToken\": \"" + authToken + "\",\n" +
                    "  \"cardId\": " + cardId + ",\n" +
                    "  \"laterThanUTC\": \"" + laterThanUTC + "\",\n" +
                    "  \"beforeThanUTC\": \""+ beforeThanUTC +"\"\n" +
                    "}";
        }
    }

    public static class ReqGetTransactionListBefore extends ReqBasic {
        public String authToken;
        public String cardId;
        public String amount;
        public String beforeThanUTC;
        public String urlPart = "api/getNTransactionsBefore";

        public ReqGetTransactionListBefore(String authToken, String cardId,
                                     String amount, String beforeThanUTC) {
            this.authToken      = authToken;
            this.cardId         = cardId;
            this.amount         = amount;
            this.beforeThanUTC  = beforeThanUTC;
        }

        @Override
        protected void setMsg() {
            super.msg = App.getContext().getResources().getString(R.string.getting_transactions);

        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_GET_TRANSACTION_LIST_BEFORE;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"authToken\": \"" + authToken + "\",\n" +
                    "  \"cardId\": " + cardId + ",\n" +
                    "  \"amount\": \"" + amount + "\",\n" +
                    "  \"beforeThanUTC\": \""+ beforeThanUTC +"\"\n" +
                    "}";
        }
    }

    public static class ReqMoneyPoloLink extends ReqBasic {
        public String sessionToken;
        public String cardGuid;
        public String urlPart = "api/initiateCardPurchase";

        public ReqMoneyPoloLink(String sessionToken, String cardGuid) {
            this.sessionToken   = sessionToken;
            this.cardGuid       = cardGuid;
        }

        @Override
        protected void setMsg() {
//            super.msg = "prepare Money Polo page";
            super.msg = App.getContext().getResources().getString(R.string.prepare_money);

        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_GET_MONEYPOLOLINK;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"sessionToken\": \"" + sessionToken + "\",\n" +
                    "  \"cardGuid\": \"" + cardGuid + "\",\n" +
                    "}";
        }
    }

    public static class ReqValidateCard extends ReqBasic {
        public String cardNumber;
        public String batchNumber;
        public String urlPart = "api/validatecard";

        public ReqValidateCard(String batchNumber, String cardNumber) {
            this.batchNumber    = batchNumber;
            this.cardNumber     = cardNumber;
        }

        @Override
        protected void setMsg() {
            super.msg = App.getContext().getResources().getString(R.string.card_calidation);
//            super.msg = "Card validation";

        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_CARD_VALIDATION;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"batchNumber\": \"" + batchNumber + "\",\n" +
                    "  \"cardNumber\": \"" + cardNumber + "\"\n" +
                    "}";
        }
    }

    public static class ReqForgotPassword extends ReqBasic {
        public String phoneNumber;
        public String urlPart = "api/forgotPassword1";

        public ReqForgotPassword( String phoneNumber) {

            this.phoneNumber  = phoneNumber;

        }

        @Override
        protected void setMsg() {
//            super.msg = "Request sms with pin";
            super.msg = App.getContext().getResources().getString(R.string.request_sms);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_FORGOT_PASSWORD;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"phoneNumber\": \"" + phoneNumber + "\",\n" +
                    "}";
        }
    }

    public static class ReqForgotPassword1 extends ReqBasic {
        public String phoneNumber;
        public String smsPin;
        public String urlPart = "api/forgotPassword1";

        public ReqForgotPassword1(String phoneNumber, String smsPin) {
            this.smsPin            = smsPin;
            this.phoneNumber    = phoneNumber;

        }

        @Override
        protected void setMsg() {
//            super.msg = "Checking your pin";
            super.msg = App.getContext().getResources().getString(R.string.checking_sms);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_FORGOT_PASSWORD1;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"phoneNumber\": \"" + phoneNumber + "\",\n" +
                    "  \"smsPin\": \"" + smsPin + "\",\n" +
                    "}";
        }
    }

    public static class ReqForgotPassword2 extends ReqBasic {
        public String token;
        public String password;
        public String urlPart = "api/forgotPassword2";

        public ReqForgotPassword2(String token, String password) {
            this.token       = token;
            this.password    = password;

        }

        @Override
        protected void setMsg() {
//            super.msg = "Approving your new password";
            super.msg = App.getContext().getResources().getString(R.string.approving_password);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_FORGOT_PASSWORD2;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"token\": \"" + token + "\",\n" +
                    "  \"password\": \"" + password + "\",\n" +
                    "}";
        }
    }

    public static class ReqGetAccountDetails extends ReqBasic {
        public String authToken;

        public String urlPart = "api/getAccountDetails";

        public ReqGetAccountDetails(String authToken) {
            this.authToken       = authToken;
        }

        @Override
        protected void setMsg() {
//            super.msg = "Loading account info";
            super.msg = App.getContext().getResources().getString(R.string.loading_account);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_GET_ACCOUNT_DETAILS;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"authToken\": \"" + authToken + "\"\n" +
                    "}";
        }
    }

    public static class ReqChangePassword extends ReqBasic {
        public String authToken;
        public String newPassword;
        public String oldPassword;
        public String urlPart = "api/changePassword";

        public ReqChangePassword(String authToken, String newPassword, String oldPassword) {
            this.authToken   = authToken;
            this.oldPassword = oldPassword;
            this.newPassword = newPassword;

        }

        @Override
        protected void setMsg() {
//            super.msg = "Approving your new password";
            super.msg = App.getContext().getResources().getString(R.string.approving_password);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_CHANGE_PASSWORD;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"authToken\": \"" + authToken + "\",\n" +
                    "  \"oldPassword\": \"" + oldPassword + "\",\n" +
                    "  \"newPassword\": \"" + newPassword + "\",\n" +
                    "}";
        }
    }

    public static class ReqGetAuthToekn extends ReqBasic {
        public String installationIdentifier;
        public String phoneNumber;
        public String smsPin;
        public String urlPart = "api/OTPAuth";

        public ReqGetAuthToekn(String installationIdentifier, String phoneNumber, String smsPin) {
            this.installationIdentifier = installationIdentifier;
            this.phoneNumber            = phoneNumber;
            this.smsPin                 = smsPin;
        }

        @Override
        protected void setMsg() {
//            super.msg = "Authorization";

            super.msg = App.getContext().getResources().getString(R.string.checking_sms);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_SIGNIN_PIN;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"phoneNumber\": \"" + phoneNumber + "\",\n" +
                    "  \"SmsPin\": \"" + smsPin + "\",\n" +
                    "  \"installationIdentifier\": \""+ installationIdentifier +"\"\n" +
                    "}";
        }
    }

    public static class ReqResendSms extends ReqBasic {
        public String installationIdentifier;
        public String phoneNumber;

        public String urlPart = "api/ResendPIN";

        public ReqResendSms(String installationIdentifier, String phoneNumber) {
            this.installationIdentifier = installationIdentifier;
            this.phoneNumber            = phoneNumber;
        }

        @Override
        protected void setMsg() {

            super.msg = App.getContext().getResources().getString(R.string.resend_sms);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_RESEND_SMS;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"phoneNumber\": \"" + phoneNumber + "\",\n" +
                    "  \"installationIdentifier\": \""+ installationIdentifier +"\"\n" +
                    "}";
        }
    }

    public static class ReqSaveClientPhoneContacts extends ReqBasic {
        public String contactArrayList;
        public String authToken;


        public String urlPart = "api/SaveClientPhoneContacts";

        public ReqSaveClientPhoneContacts(String authToken, String contactArrayList) {
            this.authToken          = authToken;
            this.contactArrayList   = contactArrayList;
        }

        @Override
        protected void setMsg() {
            super.msg = App.getContext().getResources().getString(R.string.authorization);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_SAVE_CLIENT_PHONE_CONTACTS;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"authToken\": \""+ authToken +"\",\n" +
                    "  \"contacts\": [" + contactArrayList + "\n]\n" +
                    "}";
        }

//        private String prepareContactString(){
//            String answer = "";
//
//            for ( int i = 0; i < contactArrayList.size(); i++ ) {
//                answer += "  \"firstName\": \"" + contactArrayList.get(i).firstName + "\",\n";
//                answer += "  \"lastName\": \"" + contactArrayList.get(i).lastName + "\",\n";
//                answer += "  \"phoneNumber\": \"" + contactArrayList.get(i).phoneNumber + "\",\n";
//                answer += "  \"email\": \"" + contactArrayList.get(i).email + "\",\n";
//
//            }


//            return answer;
//        }

    }

    public static class ReqGetSystemMessages extends ReqBasic {
        public String authToken;

        public String urlPart = "api/GetSystemMessages";

        public ReqGetSystemMessages(String authToken) {
            this.authToken = authToken;

        }

        @Override
        protected void setMsg() {

            super.msg = App.getContext().getResources().getString(R.string.prepare_message);
        }

        @Override
        protected void setEvent() {
            super.event = ConstBC.BC_EVENT_SYSTEM_MESSAGE;
        }

        @Override
        public String getReq() {
            return "{\n" +
                    "  \"authToken\": \"" + authToken + "\",\n" +
                    "  \"messageType\": " + 1 + "\n" +
                    "}";
        }
    }



}
