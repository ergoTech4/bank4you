package uk.bank4you.client.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import uk.bank4you.client.App;
import uk.bank4you.client.utils.Util;
import uk.bank4you.client.utils.UtilCreditCard;
import uk.bank4you.client.utils.UtilDbGet;
import uk.bank4you.client.utils.UtilFileManager;
import uk.bank4you.client.utils.UtilLog;
import uk.bank4you.client.utils.UtilPrefCustom;

public abstract class FrBasic extends Fragment {
    protected UtilLog           utilLog;
    protected UtilPrefCustom    utilPrefCustom;
    protected Util              util;
    protected UtilDbGet         utilDbGet;
    protected UtilFileManager   utilFileManager;
    protected App               app;
    protected UtilCreditCard    utilCreditCard;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app             = ((App) getActivity().getApplication());
        utilPrefCustom  = new UtilPrefCustom(getActivity());
        util            = new Util(getActivity());
        utilDbGet       = new UtilDbGet(getActivity());
        utilLog         = new UtilLog(getActivity());
        utilFileManager = new UtilFileManager(getActivity());
        utilCreditCard  = new UtilCreditCard();
    }
}