package uk.bank4you.client.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.bank4you.client.App;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstEX;
import uk.bank4you.client.multyfields.Card;
import uk.bank4you.client.multyfields.Transaction;

public class FrFavorites extends FrBasic {
    private App app;
    private int orderPosition;

    private TextView    txtAmmount;
    private TextView    txtCurrency;
    private TextView    txtPan;
    private TextView    txtDate;
    private TextView    txtName;

    private CircleImageView     imgLogo;
    private ImageLoader         imageLoader;
    private DisplayImageOptions options;


    public static final FrFavorites newInstance(int orderPosition) {
        FrFavorites fragment = new FrFavorites();

        final Bundle args = new Bundle();

        args.putInt(ConstEX.EX_CARD_ORDER_POSITION, orderPosition);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orderPosition = getArguments().getInt(ConstEX.EX_CARD_ORDER_POSITION);

        imageLoader = ImageLoader.getInstance();

        options     = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.shape_empty)
                .showImageForEmptyUri(R.drawable.shape_empty)
                .showImageOnFail(R.drawable.shape_empty)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fr_favorite, container, false);

        //app = ((App) getActivity().getApplication());

        initialize(v);

        Transaction transaction = utilDbGet.getFavoritesTransaction(orderPosition);

        txtName.setText(transaction.info);
        txtPan.setText(transaction.amount);

        String imgUrl = transaction.imgUrl;
        imageLoader.displayImage(imgUrl, imgLogo, options);

        if (imgLogo.getTag() == null || !imgLogo.getTag().equals(imgUrl)) {
            ImageAware imageAware = new ImageViewAware(imgLogo, false);
            imageLoader.displayImage(imgUrl, imageAware, options);
            imgLogo.setTag(imgUrl);
        }

        return v;
    }

    private void initialize(View v) {
        txtAmmount  = (TextView) v.findViewById(R.id.txtAmmount);
        txtCurrency = (TextView) v.findViewById(R.id.txtCurrency);
        txtPan = (TextView) v.findViewById(R.id.txtPan);
        txtDate = (TextView) v.findViewById(R.id.txtDate);
        txtName = (TextView) v.findViewById(R.id.txtName);
        imgLogo      = (CircleImageView) v.findViewById(R.id.img_logo);

    }
}