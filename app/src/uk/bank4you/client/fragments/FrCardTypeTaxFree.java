package uk.bank4you.client.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ViewAnimator;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import uk.bank4you.client.R;
import uk.bank4you.client.multyfields.AvailableCard;
import uk.bank4you.client.utils.AnimationFactory;

public class FrCardTypeTaxFree extends Fragment {

    private ImageView cardFace;
    private ImageView cardBack;
    private ViewAnimator viewAnimator;
    private RelativeLayout rl1, rl2;

    private DisplayImageOptions options;

    private ArrayList<AvailableCard> availableCards;

    private int counter = 1;
    private int currencyID = 0;
    private int position = 0;

    public FrCardTypeTaxFree() {}

    @SuppressLint("ValidFragment")
    public FrCardTypeTaxFree(ArrayList<AvailableCard> availableCards, int currencyID, int position) {
        this.availableCards = availableCards;
        this.currencyID     = currencyID;
        this.position       = position;

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fr_cardtype_student, container, false);

        initialize(v);

        ImageLoader.getInstance().displayImage(availableCards.get(currencyID).cardFrontImageUrl, cardFace, options);
        ImageLoader.getInstance().displayImage(availableCards.get(currencyID).cardBackImageUrl, cardBack, options);

        cardFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AnimationFactory.flipTransition(viewAnimator, AnimationFactory.FlipDirection.LEFT_RIGHT);
                counter++;
            }
        });

        cardBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AnimationFactory.flipTransition(viewAnimator, AnimationFactory.FlipDirection.LEFT_RIGHT);
                counter++;
            }
        });

        viewAnimator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationFactory.flipTransition(viewAnimator, AnimationFactory.FlipDirection.LEFT_RIGHT);
                counter++;
            }

        });

        return v;
    }

    private void initialize(View v) {
        cardFace     = (ImageView) v.findViewById(R.id.imageView1);
        cardBack     = (ImageView) v.findViewById(R.id.imageView2);

        rl1         = (RelativeLayout) v.findViewById(R.id.rl1);
        rl2         = (RelativeLayout) v.findViewById(R.id.rl2);

        viewAnimator = (ViewAnimator) v.findViewById(R.id.viewFlipper);
    }

    public void showAdv() {
        if ( (counter % 2) != 0 ) {
            cardFace.performClick();
        }
    }
}