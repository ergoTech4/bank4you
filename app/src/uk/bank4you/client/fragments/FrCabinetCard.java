package uk.bank4you.client.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import uk.bank4you.client.App;
import uk.bank4you.client.R;
import uk.bank4you.client.constants.ConstEX;
import uk.bank4you.client.multyfields.Card;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Locale;

public class FrCabinetCard extends FrBasic {
    private static final String TAG = "FrCabinetCard";
    private App app;
    private int orderPosition;
    private TextView txtAmmount;
    private TextView txtCurrency;
    private TextView txtPan;
    private TextView txtDate;
    private TextView txtName;
    private ImageView imgLogo;
    private Card        card;

    public static final FrCabinetCard newInstance(int orderPosition) {
        FrCabinetCard fragment = new FrCabinetCard();

        final Bundle args = new Bundle();
        args.putInt(ConstEX.EX_CARD_ORDER_POSITION, orderPosition);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orderPosition = getArguments().getInt(ConstEX.EX_CARD_ORDER_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fr_cabinet_card, container, false);
        app = ((App) getActivity().getApplication());

        initialize(v);


        return v;
    }

    private void initData(){


        card = utilDbGet.getCardDataByOrderNumber(orderPosition);
        Log.d(TAG, "initData : " + card.availableBalance);

        if (card.cardCurrency.equals("USD")) {
            txtAmmount.setText(String.format("%s $", card.availableBalance));
        } else if (card.cardCurrency.equals("GBP")) {
            txtAmmount.setText(String.format("£ %s", card.availableBalance));
        } else if (card.cardCurrency.equals("EUR")) {
            txtAmmount.setText(String.format("%s €", card.availableBalance));
        }

        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Calendar c = Calendar.getInstance();
            c.setTime(formatter.parse(card.expiryDate));
            c.add(Calendar.DATE, 1);

            DateFormat dateFormat = new SimpleDateFormat("MM");
            String month = dateFormat.format(c.getTime());

            DateFormat dateFormat1 = new SimpleDateFormat("yy");
            String year = dateFormat1.format(c.getTime());

            txtDate.setText(String.format("%s/%s", month, year));
        } catch (ParseException e) {
            utilLog.myLogExeption(e);
        }

        txtName.setText(card.cardEmbossLine.toUpperCase());

        String cardNo = card.cardNo;
        String cardNo2 = cardNo.substring(0, 4) + " ****" + " **** " + cardNo.substring(12, 16);

        txtPan.setText(cardNo2);
        int cardSystem = utilCreditCard.getCardNameId(cardNo);

        if (cardSystem == 0) {
            imgLogo.setImageResource(R.drawable.ic_visa);
        } else if (cardSystem == 1) {
            imgLogo.setImageResource(R.drawable.ic_master);
        }

    }

    private void initialize(View v) {
        txtAmmount = (TextView) v.findViewById(R.id.txtAmmount);
        txtCurrency = (TextView) v.findViewById(R.id.txtCurrency);
        txtPan = (TextView) v.findViewById(R.id.txtPan);
        txtDate = (TextView) v.findViewById(R.id.txtDate);
        txtName = (TextView) v.findViewById(R.id.txtName);
        imgLogo = (ImageView) v.findViewById(R.id.imgLogo);
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }
}