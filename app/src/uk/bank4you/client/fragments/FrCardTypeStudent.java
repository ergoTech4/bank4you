package uk.bank4you.client.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewAnimator;
import uk.bank4you.client.R;
import uk.bank4you.client.utils.AnimationFactory;
import uk.bank4you.client.utils.UtilLog;

public class FrCardTypeStudent extends Fragment {
    private View cardFace;
    private View cardBack;
    private ViewAnimator viewAnimator;

    private int counter = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fr_cardtype_student, container, false);

        initialize(v);



        cardFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AnimationFactory.flipTransition(viewAnimator, AnimationFactory.FlipDirection.LEFT_RIGHT);
                counter++;
            }
        });

        cardBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AnimationFactory.flipTransition(viewAnimator, AnimationFactory.FlipDirection.LEFT_RIGHT);
                counter++;
            }
        });

        viewAnimator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationFactory.flipTransition(viewAnimator, AnimationFactory.FlipDirection.LEFT_RIGHT);
                counter++;
            }

        });

        return v;
    }

    private void initialize(View v) {
        cardFace = (View) v.findViewById(R.id.imageView1);
        cardBack = (View) v.findViewById(R.id.imageView2);
        viewAnimator = (ViewAnimator) v.findViewById(R.id.viewFlipper);
    }

    public void showAdv() {
        if ( (counter % 2) !=0 ) {
            cardFace.performClick();
        }
    }



}