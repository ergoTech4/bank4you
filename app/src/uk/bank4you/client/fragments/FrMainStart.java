package uk.bank4you.client.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import uk.bank4you.client.App;
import uk.bank4you.client.R;

public class FrMainStart extends Fragment {
    private App app;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fr_main_start, container, false);
        app = ((App) getActivity().getApplication());

        initialoze();

        return v;
    }

    private void initialoze() {

    }
}