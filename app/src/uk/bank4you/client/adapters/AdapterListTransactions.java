package uk.bank4you.client.adapters;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;
import de.hdodenhof.circleimageview.CircleImageView;
import uk.bank4you.client.R;
import uk.bank4you.client.utils.UtilDbGet;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

public class AdapterListTransactions extends CursorAdapter {
    private static final String TAG = "AdapterListTransactions";

    private UtilDbGet           utilDbGet;
    private ImageLoader         imageLoader;
    private DisplayImageOptions options;
    private LayoutInflater      inflater;
    DateFormat formatter        = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    DateFormat dateFormat2      = new SimpleDateFormat("EEEE, dd MMMM", Locale.getDefault());

    DateFormat dateFormat       = new SimpleDateFormat("HH:mm");


	public AdapterListTransactions(Context context) {
		super(context, null, true);

//        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        utilDbGet   = new UtilDbGet(context);
        imageLoader = ImageLoader.getInstance();

        options     = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.shape_empty)
                .showImageForEmptyUri(R.drawable.shape_empty)
                .showImageOnFail(R.drawable.shape_empty)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
        inflater = LayoutInflater.from(context);

    }

	private class ViewHolder {
        CircleImageView imgLogo;
        TextView        txtName, txtComment, txtTime, txtSumm, txtCategory, txtTitle;
        LinearLayout    llTitle, llDiv;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View convertView    = inflater.inflate(R.layout.list_item_transactions, parent, false);
        ViewHolder holder   = new ViewHolder();

        holder.txtName      = (TextView) convertView.findViewById(R.id.txt_name);
        holder.txtComment   = (TextView) convertView.findViewById(R.id.txt_comment);
        holder.txtTime      = (TextView) convertView.findViewById(R.id.txt_time);
        holder.txtSumm      = (TextView) convertView.findViewById(R.id.txt_summ);
     //   holder.txtCategory  = (TextView) convertView.findViewById(R.id.txt_category);
        holder.imgLogo      = (CircleImageView) convertView.findViewById(R.id.img_logo);
        holder.txtTitle     = (TextView) convertView.findViewById(R.id.txtTitle);
        holder.llTitle      = (LinearLayout) convertView.findViewById(R.id.llTitle);
        holder.llDiv        = (LinearLayout) convertView.findViewById(R.id.llDiv);

        convertView.setTag(holder);

        return convertView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cur) {
        ViewHolder holder = (ViewHolder) view.getTag();

        if ( cur != null ) {
            holder.txtName.setText(cur.getString(cur.getColumnIndex("info")));

            String comment = cur.getString(cur.getColumnIndex("comment"));
            holder.txtComment.setText(comment.equals("null") ? "" : comment);

            String dateTimeUtc = cur.getString(cur.getColumnIndex("dateTimeUtc"));

            try {

                Date date = formatter.parse(dateTimeUtc);

                holder.txtTime.setText(dateFormat.format(date));

                holder.txtTitle.setText(dateFormat2.format(date));

                String prevDate = utilDbGet.getPrevDate(cur.getPosition(), cur.getString(cur.getColumnIndex("cardId")));

                if (prevDate != null && prevDate.substring(0, 10).equals(dateTimeUtc.substring(0, 10))) {
                    holder.llTitle.setVisibility(View.GONE);
                } else {
                    holder.llTitle.setVisibility(View.VISIBLE);
                }

                String nextDate = utilDbGet.getNextDate(cur.getPosition(), cur.getString(cur.getColumnIndex("cardId")));
                if (nextDate != null) {
                    holder.llDiv.setVisibility(nextDate.substring(0, 10).equals(dateTimeUtc.substring(0, 10)) ?
                            View.VISIBLE : View.GONE);
                } else {
                    holder.llDiv.setVisibility(View.VISIBLE);
                }
            } catch (ParseException e) {
                holder.txtTime.setText("error");
                holder.txtTitle.setText("error");
                holder.llTitle.setVisibility(View.VISIBLE);
                holder.llDiv.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }

            String amount = cur.getString(cur.getColumnIndex("amount"));

            if (amount.startsWith("-")) {
                holder.txtSumm.setTextColor(context.getResources().getColor(R.color.black));
            } else {
                holder.txtSumm.setTextColor(context.getResources().getColor(R.color.green));
            }

            String currencyCode = cur.getString(cur.getColumnIndex("currencyCode"));

            if ( !currencyCode.equals("null") ) {

                Log.d(TAG, "bindView() currencyCode = [" + currencyCode + "]");

                Currency currency = Currency.getInstance(currencyCode);

//            Locale finalLocal = Locale.getDefault();
//
//            try {
//                if (localeList.containsKey(currencyCode)) {
//                    finalLocal = localeList.get(currencyCode);
//                } else {
//                    for (Locale locale : NumberFormat.getAvailableLocales()) {
//                        String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
//                        if (currencyCode.contains(code)) {
//                            finalLocal = locale;
//                            localeList.put(currencyCode, finalLocal);
//                        }
//                    }
//                }
//            } catch (NullPointerException ignore) {}
//
//            NumberFormat numberFormat = DecimalFormat.getCurrencyInstance(finalLocal);
//
//            numberFormat.setCurrency(currency);
//
//            String strNum = numberFormat.format(Double.valueOf(amount));
                if (currencyCode.equals("RUB")) {
                    holder.txtSumm.setText(String.format("₽ %s", amount));
//        } else if (currencyCode.equals("GBP")){
//            holder.txtSumm.setText(String.format("%s %s", currency.getSymbol(), amount));
                } else if (currencyCode.equals("CZK")) {
                    holder.txtSumm.setText(String.format("Kč %s", amount));
                } else if (currencyCode.equals("EUR") || currencyCode.equals("USD")) {
                    holder.txtSumm.setText(String.format("%s %s", amount, currency.getSymbol()));
                } else {
                    holder.txtSumm.setText(String.format("%s %s", currency.getSymbol(), amount));
                }

//            holder.txtSumm.setText(String.format("%s %s", symbol, amount));

//            holder.txtCategory.setText(cur.getString(cur.getColumnIndex("category")));
                String imgUrl = cur.getString(cur.getColumnIndex("imgUrl"));
                imageLoader.displayImage(imgUrl, holder.imgLogo, options);

                if (holder.imgLogo.getTag() == null || !holder.imgLogo.getTag().equals(imgUrl)) {
                    ImageAware imageAware = new ImageViewAware(holder.imgLogo, false);
                    imageLoader.displayImage(imgUrl, imageAware, options);
                    holder.imgLogo.setTag(imgUrl);
                }
            } else {
                holder.txtName.setText("Fin: CARD TRANSACTION");
            }
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);


        return view;
    }
}