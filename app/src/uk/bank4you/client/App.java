package uk.bank4you.client;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.helpshift.Core;
import com.helpshift.support.Support;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import io.fabric.sdk.android.Fabric;
import uk.bank4you.client.constants.Const;
import uk.bank4you.client.multyfields.MyProgressBar;
import uk.bank4you.client.services.DatabaseHelper;
import uk.bank4you.client.services.gcm.RegistrationIntentService;
import uk.bank4you.client.utils.UtilLog;

public class App extends Application {
    private static final String TAG = "App";
    private SQLiteDatabase db;
    private MyProgressBar progressBar;
    public String imageInBase64;
    private static Context mContext;


    @Override
    public void onCreate() {
        MultiDex.install(this);
        super.onCreate();
        mContext = this;


        Fabric.with(this, new Crashlytics());
        Core.init(Support.getInstance());
        Core.install(this, // "this" should be the application object
                "e2180988ee8584c5bfee85fcf237e98b",
                "bank4you.helpshift.com",
                "bank4you_platform_20150703085450739-ffe2ad6dd2f9ef4");

        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .threadPoolSize(5)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new UsingFreqLimitedMemoryCache(20 * 1024 * 1024)) // 20 Mb
                .discCacheFileNameGenerator(new HashCodeFileNameGenerator())
                .imageDownloader(new BaseImageDownloader(this, 5 * 1000, 30 * 1000)) // connectTimeout (5 s), readTimeout (30 s)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .diskCacheFileCount(250)
                .build();

        ImageLoader.getInstance().init(config);

        setMyProgressBar("", false);
    }

    public static Context getContext(){
        return mContext;
    }


    /* MY PROGRESSBAR */
    public void setMyProgressBar(String text, boolean status) {
        progressBar = new MyProgressBar(text, status);
    }

    public MyProgressBar getProgressBar() {
        return progressBar;
    }

    /* DB SETTINGS */
    public void dbClose() {
        if ( db != null )
            db.close();
    }

    public SQLiteDatabase getDb() {
        if ( db == null || !db.isOpen() ) {
            try {
                DatabaseHelper helper = new DatabaseHelper(this);
                db = helper.getWritableDatabase();

                Log.d(TAG, "BD open success");
            } catch (SQLException e) {
                new UtilLog(this).myLogFlurry(Const.EV_ERR_CATCHED, "при создании подключения к БД", e);
                e.printStackTrace();
            }
        }
        return db;
    }
}