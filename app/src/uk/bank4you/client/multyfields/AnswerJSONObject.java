package uk.bank4you.client.multyfields;

import org.json.JSONObject;

public class AnswerJSONObject {
    public String       status;
    public String       log;
    public JSONObject   jsonObject;

    public AnswerJSONObject(String status, JSONObject jsonObject, String log) {
        this.status         = status;
        this.log            = log;
        this.jsonObject     = jsonObject;
    }
}