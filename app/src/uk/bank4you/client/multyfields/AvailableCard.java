package uk.bank4you.client.multyfields;

public class AvailableCard {
    public String name;
    public String id;
    public String currency;
    public String orderPriceInChosenCurrency;
    public String cardFrontImageUrl;
    public String cardBackImageUrl;
    public String rotatedViewUrl;
    public String tariffUrl;

}


//{"name":"Tax Free","options":[{
//[{"id":"05a29735-3766-4034-0001-000000000001",
// "currency":"GBP",
// "orderPriceInChosenCurrency":25,
// "cardFrontImageUrl":"http:\/\/dev.bank4you.uk\/Static\/cards\/TaxFree_front.png",
// "cardBackImageUrl":"http:\/\/dev.bank4you.uk\/Static\/cards\/TaxFree__back.png",
// "rotatedViewUrl":"http:\/\/dev.bank4you.uk\/Static\/cards\/TaxFree_rotated.png",
// "