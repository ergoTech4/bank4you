package uk.bank4you.client.multyfields;

import java.io.Reader;

public class AnswerReader {
    public String status;
    public String log;

    public Reader reader;

    public AnswerReader(String status, Reader reader, String log) {
        this.status = status;
        this.log = log;

        this.reader = reader;
    }
}
