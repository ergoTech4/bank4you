package uk.bank4you.client.multyfields;

/**
 * Created by grench on 08/01/2016.
 */
public class Transaction {
    public String category;
    public String id;
    public String dateTimeUtc;
    public String amount;
    public String currencyCode;
    public String amountInCardCurrency;
    public String additionalCommissionInCardCurrency;
    public String info;
    public String transactionType;
    public String mcc;
    public String comment;
    public String cardId;
    public String imgUrl;


    public Transaction(String additionalCommissionInCardCurrency, String amount,
                       String amountInCardCurrency, String cardId, String category,
                       String comment, String currencyCode, String dateTimeUtc, String id,
                       String imgUrl, String info, String mcc, String transactionType) {

        this.additionalCommissionInCardCurrency = additionalCommissionInCardCurrency;
        this.amount = amount;
        this.amountInCardCurrency = amountInCardCurrency;
        this.cardId = cardId;
        this.category = category;
        this.comment = comment;
        this.currencyCode = currencyCode;
        this.dateTimeUtc = dateTimeUtc;
        this.id = id;
        this.imgUrl = imgUrl;
        this.info = info;
        this.mcc = mcc;
        this.transactionType = transactionType;
    }
}
