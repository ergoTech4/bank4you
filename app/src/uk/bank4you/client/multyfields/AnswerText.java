package uk.bank4you.client.multyfields;

public class AnswerText {
    public String status;
    public String text;
    public String log;

    public AnswerText(String status, String text, String log) {
        this.status = status;
        this.text = text;
        this.log = log;
    }
}
