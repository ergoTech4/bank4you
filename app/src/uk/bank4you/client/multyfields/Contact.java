package uk.bank4you.client.multyfields;

/**
 * Created by ergo on 7/19/16.
 */
public class Contact {

    public String firstName;
    public String lastName;
    public String phoneNumber;
    public String email;
}
