package uk.bank4you.client.multyfields;

public class Card {
    public String cardID;
    public String cardNo;
    public String cardCurrency;
    public String availableBalance;
    public String holdBalance;
    public String effectiveDate;
    public String expiryDate;
    public String cardEmbossLine;


    public Card(String availableBalance, String cardCurrency, String cardEmbossLine, String cardID, String cardNo, String effectiveDate, String expiryDate, String holdBalance) {
        this.availableBalance = availableBalance;
        this.cardCurrency = cardCurrency;
        this.cardEmbossLine = cardEmbossLine;
        this.cardID = cardID;
        this.cardNo = cardNo;
        this.effectiveDate = effectiveDate;
        this.expiryDate = expiryDate;
        this.holdBalance = holdBalance;
    }
}
