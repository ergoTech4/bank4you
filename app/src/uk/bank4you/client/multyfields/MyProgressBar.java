package uk.bank4you.client.multyfields;

public class MyProgressBar {
    private String text;
    private boolean status;

    public MyProgressBar(String text, boolean status) {
        this.text = text;
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public boolean getStatus() {
        return status;
    }
}
