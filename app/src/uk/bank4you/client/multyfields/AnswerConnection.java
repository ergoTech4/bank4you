package uk.bank4you.client.multyfields;

import java.net.HttpURLConnection;

public class AnswerConnection {
    public String status;
    public String log = null;

    public HttpURLConnection connection = null;

    public AnswerConnection(String status, HttpURLConnection connection, String log) {
        this.status = status;
        this.log    = log;

        this.connection = connection;
    }
}