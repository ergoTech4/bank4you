package uk.bank4you.client.multyfields;

import android.content.res.Resources;
import android.provider.ContactsContract;
import android.text.SpannableStringBuilder;
import android.util.LongSparseArray;

public class AddressBookContact {
    private long id;
    private Resources res;
    public String name;
    public String family;
    private LongSparseArray<String> emails;
    private LongSparseArray<String> phones;

    public AddressBookContact(long id, String name, Resources res) {
        this.id = id;
        this.name = name;
        this.res = res;
    }

    @Override
    public String toString() {
        return toString(false);
    }

    public String toString(boolean rich) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        if (rich) {
            builder.append("id: ").append(Long.toString(id))
                    .append(", name: ").append("\u001b[1m").append(name).append("\u001b[0m");
        } else {
            builder.append(name);
        }

        if (phones != null) {
            builder.append("\n\tphones: ");
            for (int i = 0; i < phones.size(); i++) {
                int type = (int) phones.keyAt(i);
                builder.append(ContactsContract.CommonDataKinds.Phone.getTypeLabel(res, type, ""))
                        .append(": ")
                        .append(phones.valueAt(i));
                if (i + 1 < phones.size()) {
                    builder.append(", ");
                }
            }
        }

        if (emails != null) {
            builder.append("\n\temails: ");
            for (int i = 0; i < emails.size(); i++) {
                int type = (int) emails.keyAt(i);
                builder.append(ContactsContract.CommonDataKinds.Email.getTypeLabel(res, type, ""))
                        .append(": ")
                        .append(emails.valueAt(i));
                if (i + 1 < emails.size()) {
                    builder.append(", ");
                }
            }
        }
        return builder.toString();
    }

    public void addEmail(int type, String address) {
        if (emails == null) {
            emails = new LongSparseArray<String>();
        }
        emails.put(type, address);
    }

    public void addPhone(int type, String number) {
        if (phones == null) {
            phones = new LongSparseArray<String>();
        }
        phones.put(type, number);
    }

    public String getEmail(){
        SpannableStringBuilder builder = new SpannableStringBuilder();
        if (emails != null) {
            for (int i = 0; i < emails.size(); i++) {
//                int type = (int) emails.keyAt(i);
                builder
//                        .append(ContactsContract.CommonDataKinds.Email.getTypeLabel(res, type, ""))
//                        .append(": ")
                        .append(emails.valueAt(i));
                if ( i + 1 < emails.size() ) {
                    builder.append(", ");
                }
            }
        }
        return builder.toString();
    }

    public  String getPhoneNumber() {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        if ( phones != null ) {
            int size = phones.size();
            for ( int i = 0; i < size; i++ ) {
                int type = (int) phones.keyAt(i);
                builder
//                        .append(ContactsContract.CommonDataKinds.Phone.getTypeLabel(res, type, ""))
//                        .append(": ")
                        .append(phones.valueAt(i));
                if ( i + 1 < phones.size() ) {
                    builder.append(", ");
                }
            }
        }
        return builder.toString();
    }

    public String getName() {
        try {
            family = name.substring(name.indexOf(" ") + 1, name.length());

            family = family.replace("\"", "");

            name = name.substring(0, name.indexOf(" "));
        } catch (Exception s){}

        return name.replace("\"", "");
    }

}
