package uk.bank4you.client.multyfields;

public class AnswerParse<T> {
    public String status;
    public String log;

    public T resultedClass;

    public AnswerParse(String status, T resultedClass, String log) {
        this.status = status;
        this.log = log;

        this.resultedClass = resultedClass;
    }
}